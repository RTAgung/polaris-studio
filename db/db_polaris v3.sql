-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 23, 2021 at 05:50 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_polaris`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id_admin` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(255) NOT NULL,
  `no_hp` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `tgl_lahir` date NOT NULL,
  `tgl_masuk` date NOT NULL,
  `jabatan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id_admin`, `nama`, `email`, `password`, `no_hp`, `alamat`, `tgl_lahir`, `tgl_masuk`, `jabatan`) VALUES
(1, 'Indraa', 'indra@gmail.com', '$2y$10$2YmNqIFsoTclH5cTyHUpq.nzifO7M4MlCrGnglD/CS0w2dCgc2HWK', '081377879966', 'Bantull', '2000-01-01', '2021-03-16', 'manajer');

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id_article` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `teks` text NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `tgl_buat` date NOT NULL,
  `tgl_edit` date NOT NULL,
  `id_admin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id_article`, `judul`, `teks`, `gambar`, `tgl_buat`, `tgl_edit`, `id_admin`) VALUES
(2, 'Artikel Test 2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc in ultrices leo. Integer porttitor posuere porta. Aenean eget hendrerit erat. Quisque vehicula velit id justo aliquet, eu volutpat est tempor. Aenean tristique libero nec nisi malesuada volutpat. Phasellus dignissim suscipit turpis, id maximus velit molestie ullamcorper. Proin nec risus at sem semper gravida.\r\n\r\nInteger egestas, augue finibus feugiat tempor, ipsum urna volutpat neque, id feugiat elit turpis vitae sem. Praesent at rhoncus nibh, quis bibendum velit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi sagittis nec ante nec dignissim. Vestibulum egestas venenatis urna. Quisque ut ornare purus, id blandit odio.', 'article_example2.png', '0000-00-00', '0000-00-00', 1),
(11, 'Artikel Test 3', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc in ultrices leo. Integer porttitor posuere porta. Aenean eget hendrerit erat. Quisque vehicula velit id justo aliquet, eu volutpat est tempor. Aenean tristique libero nec nisi malesuada volutpat. Phasellus dignissim suscipit turpis, id maximus velit molestie ullamcorper. Proin nec risus at sem semper gravida.\r\n\r\nInteger egestas, augue finibus feugiat tempor, ipsum urna volutpat neque, id feugiat elit turpis vitae sem. Praesent at rhoncus nibh, quis bibendum velit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi sagittis nec ante nec dignissim. Vestibulum egestas venenatis urna. Quisque ut ornare purus, id blandit odio.', 'article_example3.png', '0000-00-00', '0000-00-00', 1),
(12, 'Artikel Test 4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc in ultrices leo. Integer porttitor posuere porta. Aenean eget hendrerit erat. Quisque vehicula velit id justo aliquet, eu volutpat est tempor. Aenean tristique libero nec nisi malesuada volutpat. Phasellus dignissim suscipit turpis, id maximus velit molestie ullamcorper. Proin nec risus at sem semper gravida.\r\n\r\nInteger egestas, augue finibus feugiat tempor, ipsum urna volutpat neque, id feugiat elit turpis vitae sem. Praesent at rhoncus nibh, quis bibendum velit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi sagittis nec ante nec dignissim. Vestibulum egestas venenatis urna. Quisque ut ornare purus, id blandit odio.', 'article_example4.png', '0000-00-00', '0000-00-00', 1),
(13, 'Artikel Test 5', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc in ultrices leo. Integer porttitor posuere porta. Aenean eget hendrerit erat. Quisque vehicula velit id justo aliquet, eu volutpat est tempor. Aenean tristique libero nec nisi malesuada volutpat. Phasellus dignissim suscipit turpis, id maximus velit molestie ullamcorper. Proin nec risus at sem semper gravida.\r\n\r\nInteger egestas, augue finibus feugiat tempor, ipsum urna volutpat neque, id feugiat elit turpis vitae sem. Praesent at rhoncus nibh, quis bibendum velit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi sagittis nec ante nec dignissim. Vestibulum egestas venenatis urna. Quisque ut ornare purus, id blandit odio.', 'article_example5.png', '0000-00-00', '0000-00-00', 1),
(14, 'Artikel Test 6', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc in ultrices leo. Integer porttitor posuere porta. Aenean eget hendrerit erat. Quisque vehicula velit id justo aliquet, eu volutpat est tempor. Aenean tristique libero nec nisi malesuada volutpat. Phasellus dignissim suscipit turpis, id maximus velit molestie ullamcorper. Proin nec risus at sem semper gravida.\r\n\r\nInteger egestas, augue finibus feugiat tempor, ipsum urna volutpat neque, id feugiat elit turpis vitae sem. Praesent at rhoncus nibh, quis bibendum velit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi sagittis nec ante nec dignissim. Vestibulum egestas venenatis urna. Quisque ut ornare purus, id blandit odio.', 'article_example1.png', '0000-00-00', '0000-00-00', 1),
(15, 'Artikel Test 7', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc in ultrices leo. Integer porttitor posuere porta. Aenean eget hendrerit erat. Quisque vehicula velit id justo aliquet, eu volutpat est tempor. Aenean tristique libero nec nisi malesuada volutpat. Phasellus dignissim suscipit turpis, id maximus velit molestie ullamcorper. Proin nec risus at sem semper gravida.\r\n\r\nInteger egestas, augue finibus feugiat tempor, ipsum urna volutpat neque, id feugiat elit turpis vitae sem. Praesent at rhoncus nibh, quis bibendum velit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi sagittis nec ante nec dignissim. Vestibulum egestas venenatis urna. Quisque ut ornare purus, id blandit odio.', 'article_example2.png', '0000-00-00', '0000-00-00', 1),
(16, 'Artikel Test 8', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc in ultrices leo. Integer porttitor posuere porta. Aenean eget hendrerit erat. Quisque vehicula velit id justo aliquet, eu volutpat est tempor. Aenean tristique libero nec nisi malesuada volutpat. Phasellus dignissim suscipit turpis, id maximus velit molestie ullamcorper. Proin nec risus at sem semper gravida.\r\n\r\nInteger egestas, augue finibus feugiat tempor, ipsum urna volutpat neque, id feugiat elit turpis vitae sem. Praesent at rhoncus nibh, quis bibendum velit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi sagittis nec ante nec dignissim. Vestibulum egestas venenatis urna. Quisque ut ornare purus, id blandit odio.', 'article_example3.png', '0000-00-00', '0000-00-00', 1),
(17, 'Artikel Test 9', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc in ultrices leo. Integer porttitor posuere porta. Aenean eget hendrerit erat. Quisque vehicula velit id justo aliquet, eu volutpat est tempor. Aenean tristique libero nec nisi malesuada volutpat. Phasellus dignissim suscipit turpis, id maximus velit molestie ullamcorper. Proin nec risus at sem semper gravida.\r\n\r\nInteger egestas, augue finibus feugiat tempor, ipsum urna volutpat neque, id feugiat elit turpis vitae sem. Praesent at rhoncus nibh, quis bibendum velit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi sagittis nec ante nec dignissim. Vestibulum egestas venenatis urna. Quisque ut ornare purus, id blandit odio.', 'article_example4.png', '0000-00-00', '0000-00-00', 1),
(18, 'Artikel Test 10', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc in ultrices leo. Integer porttitor posuere porta. Aenean eget hendrerit erat. Quisque vehicula velit id justo aliquet, eu volutpat est tempor. Aenean tristique libero nec nisi malesuada volutpat. Phasellus dignissim suscipit turpis, id maximus velit molestie ullamcorper. Proin nec risus at sem semper gravida.\r\n\r\nInteger egestas, augue finibus feugiat tempor, ipsum urna volutpat neque, id feugiat elit turpis vitae sem. Praesent at rhoncus nibh, quis bibendum velit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi sagittis nec ante nec dignissim. Vestibulum egestas venenatis urna. Quisque ut ornare purus, id blandit odio.', 'article_example5.png', '0000-00-00', '0000-00-00', 1),
(19, 'Artikel Test 1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc in ultrices leo. Integer porttitor posuere porta. Aenean eget hendrerit erat. Quisque vehicula velit id justo aliquet, eu volutpat est tempor. Aenean tristique libero nec nisi malesuada volutpat. Phasellus dignissim suscipit turpis, id maximus velit molestie ullamcorper. Proin nec risus at sem semper gravida.\r\n\r\nInteger egestas, augue finibus feugiat tempor, ipsum urna volutpat neque, id feugiat elit turpis vitae sem. Praesent at rhoncus nibh, quis bibendum velit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi sagittis nec ante nec dignissim. Vestibulum egestas venenatis urna. Quisque ut ornare purus, id blandit odio.', 'article_example1.png', '0000-00-00', '0000-00-00', 1),
(20, 'Artikel Test 2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc in ultrices leo. Integer porttitor posuere porta. Aenean eget hendrerit erat. Quisque vehicula velit id justo aliquet, eu volutpat est tempor. Aenean tristique libero nec nisi malesuada volutpat. Phasellus dignissim suscipit turpis, id maximus velit molestie ullamcorper. Proin nec risus at sem semper gravida.\r\n\r\nInteger egestas, augue finibus feugiat tempor, ipsum urna volutpat neque, id feugiat elit turpis vitae sem. Praesent at rhoncus nibh, quis bibendum velit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi sagittis nec ante nec dignissim. Vestibulum egestas venenatis urna. Quisque ut ornare purus, id blandit odio.', 'article_example2.png', '0000-00-00', '0000-00-00', 1),
(21, 'Artikel Test 3', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc in ultrices leo. Integer porttitor posuere porta. Aenean eget hendrerit erat. Quisque vehicula velit id justo aliquet, eu volutpat est tempor. Aenean tristique libero nec nisi malesuada volutpat. Phasellus dignissim suscipit turpis, id maximus velit molestie ullamcorper. Proin nec risus at sem semper gravida.\r\n\r\nInteger egestas, augue finibus feugiat tempor, ipsum urna volutpat neque, id feugiat elit turpis vitae sem. Praesent at rhoncus nibh, quis bibendum velit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi sagittis nec ante nec dignissim. Vestibulum egestas venenatis urna. Quisque ut ornare purus, id blandit odio.', 'article_example3.png', '0000-00-00', '0000-00-00', 1),
(22, 'Artikel Test 4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc in ultrices leo. Integer porttitor posuere porta. Aenean eget hendrerit erat. Quisque vehicula velit id justo aliquet, eu volutpat est tempor. Aenean tristique libero nec nisi malesuada volutpat. Phasellus dignissim suscipit turpis, id maximus velit molestie ullamcorper. Proin nec risus at sem semper gravida.\r\n\r\nInteger egestas, augue finibus feugiat tempor, ipsum urna volutpat neque, id feugiat elit turpis vitae sem. Praesent at rhoncus nibh, quis bibendum velit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi sagittis nec ante nec dignissim. Vestibulum egestas venenatis urna. Quisque ut ornare purus, id blandit odio.', 'article_example4.png', '0000-00-00', '0000-00-00', 1),
(23, 'Artikel Test 5', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc in ultrices leo. Integer porttitor posuere porta. Aenean eget hendrerit erat. Quisque vehicula velit id justo aliquet, eu volutpat est tempor. Aenean tristique libero nec nisi malesuada volutpat. Phasellus dignissim suscipit turpis, id maximus velit molestie ullamcorper. Proin nec risus at sem semper gravida.\r\n\r\nInteger egestas, augue finibus feugiat tempor, ipsum urna volutpat neque, id feugiat elit turpis vitae sem. Praesent at rhoncus nibh, quis bibendum velit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi sagittis nec ante nec dignissim. Vestibulum egestas venenatis urna. Quisque ut ornare purus, id blandit odio.', 'article_example5.png', '0000-00-00', '0000-00-00', 1),
(24, 'Artikel Test 6', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc in ultrices leo. Integer porttitor posuere porta. Aenean eget hendrerit erat. Quisque vehicula velit id justo aliquet, eu volutpat est tempor. Aenean tristique libero nec nisi malesuada volutpat. Phasellus dignissim suscipit turpis, id maximus velit molestie ullamcorper. Proin nec risus at sem semper gravida.\r\n\r\nInteger egestas, augue finibus feugiat tempor, ipsum urna volutpat neque, id feugiat elit turpis vitae sem. Praesent at rhoncus nibh, quis bibendum velit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi sagittis nec ante nec dignissim. Vestibulum egestas venenatis urna. Quisque ut ornare purus, id blandit odio.', 'article_example1.png', '0000-00-00', '0000-00-00', 1),
(25, 'Artikel Test 7', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc in ultrices leo. Integer porttitor posuere porta. Aenean eget hendrerit erat. Quisque vehicula velit id justo aliquet, eu volutpat est tempor. Aenean tristique libero nec nisi malesuada volutpat. Phasellus dignissim suscipit turpis, id maximus velit molestie ullamcorper. Proin nec risus at sem semper gravida.\r\n\r\nInteger egestas, augue finibus feugiat tempor, ipsum urna volutpat neque, id feugiat elit turpis vitae sem. Praesent at rhoncus nibh, quis bibendum velit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi sagittis nec ante nec dignissim. Vestibulum egestas venenatis urna. Quisque ut ornare purus, id blandit odio.', 'article_example2.png', '0000-00-00', '0000-00-00', 1),
(26, 'Artikel Test 8', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc in ultrices leo. Integer porttitor posuere porta. Aenean eget hendrerit erat. Quisque vehicula velit id justo aliquet, eu volutpat est tempor. Aenean tristique libero nec nisi malesuada volutpat. Phasellus dignissim suscipit turpis, id maximus velit molestie ullamcorper. Proin nec risus at sem semper gravida.\r\n\r\nInteger egestas, augue finibus feugiat tempor, ipsum urna volutpat neque, id feugiat elit turpis vitae sem. Praesent at rhoncus nibh, quis bibendum velit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi sagittis nec ante nec dignissim. Vestibulum egestas venenatis urna. Quisque ut ornare purus, id blandit odio.', 'article_example3.png', '0000-00-00', '0000-00-00', 1),
(27, 'Artikel Test 9', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc in ultrices leo. Integer porttitor posuere porta. Aenean eget hendrerit erat. Quisque vehicula velit id justo aliquet, eu volutpat est tempor. Aenean tristique libero nec nisi malesuada volutpat. Phasellus dignissim suscipit turpis, id maximus velit molestie ullamcorper. Proin nec risus at sem semper gravida.\r\n\r\nInteger egestas, augue finibus feugiat tempor, ipsum urna volutpat neque, id feugiat elit turpis vitae sem. Praesent at rhoncus nibh, quis bibendum velit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi sagittis nec ante nec dignissim. Vestibulum egestas venenatis urna. Quisque ut ornare purus, id blandit odio.', 'article_example4.png', '0000-00-00', '0000-00-00', 1),
(28, 'Artikel Test 10', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc in ultrices leo. Integer porttitor posuere porta. Aenean eget hendrerit erat. Quisque vehicula velit id justo aliquet, eu volutpat est tempor. Aenean tristique libero nec nisi malesuada volutpat. Phasellus dignissim suscipit turpis, id maximus velit molestie ullamcorper. Proin nec risus at sem semper gravida.\r\n\r\nInteger egestas, augue finibus feugiat tempor, ipsum urna volutpat neque, id feugiat elit turpis vitae sem. Praesent at rhoncus nibh, quis bibendum velit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi sagittis nec ante nec dignissim. Vestibulum egestas venenatis urna. Quisque ut ornare purus, id blandit odio.', 'article_example5.png', '2021-04-01', '0000-00-00', 1),
(50, 'Test Artikel', 'ini test artikel\r\nke 1', 'article_1619144055.jpg', '2021-04-23', '2021-04-23', 1);

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `id_chat` int(11) NOT NULL,
  `pengirim` varchar(255) NOT NULL,
  `teks` text NOT NULL,
  `tgl_kirim` date NOT NULL,
  `label` varchar(255) NOT NULL,
  `id_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `chat`
--

INSERT INTO `chat` (`id_chat`, `pengirim`, `teks`, `tgl_kirim`, `label`, `id_order`) VALUES
(8, 'client', 'haloo', '2021-03-17', '', 1),
(9, 'client', 'mantap', '2021-03-17', '', 1),
(10, 'admin', 'ini ya hasil nya', '2021-03-17', 'revisi 1', 1),
(11, 'admin', 'ohiya lupa wkwkwk', '2021-03-17', '', 1),
(12, 'client', 'tes', '2021-03-17', '', 1),
(13, 'client', 'satu', '2021-03-17', '', 1),
(14, 'client', 'dua', '2021-03-17', '', 1),
(15, 'client', 'hai', '2021-03-18', '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id_client` int(11) NOT NULL,
  `nama_instansi` varchar(255) NOT NULL,
  `email` varchar(128) NOT NULL,
  `instagram` varchar(255) NOT NULL,
  `whatsapp` varchar(255) NOT NULL,
  `is_registered` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id_client`, `nama_instansi`, `email`, `instagram`, `whatsapp`, `is_registered`) VALUES
(1, 'UPN Veteran Yogyakarta', 'upn@gmail.com', 'upnvy', '081377879966', 1),
(2, 'UPN Veteran Yogyakarta1', 'upn1@gmail.com', 'upnvy', '081377879966', 0),
(3, 'UPN Veteran Yogyakarta2', 'upn2@gmail.com', 'upnvy', '081377879966', 0),
(10, 'test', 'test@gmail.com', 'test ig', '123456', 0),
(11, 'Test2', 'test2@gmail.com', 'test2.id', '456879', 0),
(12, 'test3', 'test3@gmail.com', 'test3', '456123', 0),
(13, 'test4', 'test4@gmail.com', 'test4', '123', 0),
(14, 'abc', 'abc@gmail.com', 'abc.id', '082136564484', 0),
(16, 'abc', 'ab1c@gmail.com', 'abc', '123123', 0),
(17, 'qwe', 'qwe@gmail.com', 'qwe', '123', 0),
(18, 'qqq', 'qqq@gmail.com', 'qqq', '111', 0),
(19, 'ucup', 'ucupp@gmail.com', 'igucup', '082136564484', 0),
(20, 'akua', 'akua@gmail.com', 'akuaa', '123456789', 0);

-- --------------------------------------------------------

--
-- Table structure for table `detail_orders`
--

CREATE TABLE `detail_orders` (
  `id_detail_order` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `asset` varchar(255) NOT NULL,
  `id_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `detail_orders`
--

INSERT INTO `detail_orders` (`id_detail_order`, `judul`, `asset`, `id_order`) VALUES
(1, 'Website Profile UPN', 'link asset baru', 1),
(3, 'Video Pembelajaran', 'aset ini', 2);

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id_invoice` int(11) NOT NULL,
  `kode_invoice` varchar(128) NOT NULL,
  `tgl_keluar` date NOT NULL,
  `biaya` double NOT NULL,
  `term` int(11) NOT NULL,
  `status` varchar(255) NOT NULL,
  `lampiran` varchar(255) NOT NULL,
  `id_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id_invoice`, `kode_invoice`, `tgl_keluar`, `biaya`, `term`, `status`, `lampiran`, `id_order`) VALUES
(1, 'P1/17/03/2021', '2021-03-17', 6000000, 1, 'paid', '/asset/file/invoice/test.pdf', 1),
(2, 'P2/17/03/2021', '2021-03-17', 6000000, 2, 'unpaid', '/asset/file/invoice/test.pdf', 1),
(3, 'P3/17/03/2021', '2021-03-17', 1500000, 1, 'unpaid', '/asset/file/invoice/test.pdf', 2);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id_order` int(11) NOT NULL,
  `deskripsi` text NOT NULL,
  `status` varchar(255) NOT NULL,
  `tgl_mulai` date NOT NULL,
  `id_client` int(11) NOT NULL,
  `id_packet` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id_order`, `deskripsi`, `status`, `tgl_mulai`, `id_client`, `id_packet`) VALUES
(1, '', 'on progress', '2021-03-15', 1, 2),
(2, '', 'on progress', '2021-03-17', 1, 5),
(4, 'testing desc', 'pending', '0000-00-00', 10, 2),
(5, 'testing 2', 'pending', '0000-00-00', 11, 1),
(7, 'testing4', 'pending', '0000-00-00', 13, 5),
(8, 'abc v', 'pending', '0000-00-00', 16, 3),
(9, 'abc pedas', 'pending', '0000-00-00', 14, 2),
(10, 'qweeee', 'pending', '0000-00-00', 17, 1),
(11, 'qqq', 'pending', '0000-00-00', 18, 2),
(12, 'rtas*', 'pending', '0000-00-00', 1, 1),
(13, 'ucup kerupuk', 'pending', '0000-00-00', 19, 2),
(14, 'akua segar', 'pending', '0000-00-00', 20, 7),
(15, 'akua segar', 'pending', '0000-00-00', 20, 4);

-- --------------------------------------------------------

--
-- Table structure for table `packets`
--

CREATE TABLE `packets` (
  `id_packet` int(11) NOT NULL,
  `kode` varchar(128) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `jenis` varchar(255) NOT NULL,
  `harga` double NOT NULL DEFAULT 0,
  `harga_awal` double NOT NULL,
  `deskripsi` text NOT NULL,
  `detail` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `packets`
--

INSERT INTO `packets` (`id_packet`, `kode`, `nama`, `jenis`, `harga`, `harga_awal`, `deskripsi`, `detail`) VALUES
(1, 'W1', 'Web Profile', 'Web', 6900000, 10000000, 'Cocok untuk web dengan profil, layanan, produk dan sosial media update.', 'Desain web\r\nProgramming'),
(2, 'W2', 'Web Shop', 'Web', 12000000, 18000000, 'Cocok untuk web dengan katalog dan sistem transaksi jual-beli, profil, layanan, dan sosial media update.', 'Desain web\r\nEcommerce\r\nProgramming\r\nGratis Domain'),
(3, 'V1', 'Iklan Produk Fokus', 'video', 900000, 2500000, 'Menonjolkan produk, cocok untuk launch produk baru atau memperlihatkan detail.', '1 video 30 – 45 detik\r\n5 foto\r\nindoor/ studio shoot'),
(4, 'V2', 'Iklan Testimoni', 'video', 1500000, 3500000, 'Testimoni produk, bisa meningkatkan kepercayaan konsumen dari kisah nyata pelanggan.', '1 video 30 – 60 detik\r\nmodel/ talent\r\n5 foto'),
(5, 'V3', 'Iklan Komersil', 'video', 3500000, 9500000, 'Iklan menggunakan model, cocok untuk branding/ soft selling.', '1 video 30 – 60 detik\r\n1 model\r\n2 video story\r\n10 foto'),
(7, 'V4', 'Video Profil', 'video', 8000000, 20500000, 'Testimoni produk, bisa meningkatkan kepercayaan konsumen dari kisah nyata pelanggan.', '1 video 30 – 45 detik\r\n5 foto\r\nindoor/ studio shoot');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id_payment` int(11) NOT NULL,
  `bukti` varchar(255) NOT NULL,
  `konfirmasi` tinyint(1) NOT NULL,
  `id_invoice` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id_payment`, `bukti`, `konfirmasi`, `id_invoice`) VALUES
(1, '/asset/file/payment/test.pdf', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `registered_clients`
--

CREATE TABLE `registered_clients` (
  `id_registered_client` int(11) NOT NULL,
  `nama_pemilik` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `id_admin` int(11) NOT NULL,
  `id_client` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `registered_clients`
--

INSERT INTO `registered_clients` (`id_registered_client`, `nama_pemilik`, `password`, `alamat`, `id_admin`, `id_client`) VALUES
(1, 'Rama Tri Agung', '$2y$10$SRsQ4lhODttqHftHKsXVW.Qi9Pn9eB.0bbqXeUlTZsvbfkHe0.pfu', 'jalan babarsari nomor 5', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id_tag` int(11) NOT NULL,
  `nama` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id_tag`, `nama`) VALUES
(14, 'new'),
(17, 'tag'),
(11, 'tag1'),
(12, 'tag2'),
(3, 'tag3'),
(4, 'tag4'),
(5, 'tag5');

-- --------------------------------------------------------

--
-- Table structure for table `tag_article`
--

CREATE TABLE `tag_article` (
  `id_tag_article` int(11) NOT NULL,
  `id_article` int(11) NOT NULL,
  `id_tag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tag_article`
--

INSERT INTO `tag_article` (`id_tag_article`, `id_article`, `id_tag`) VALUES
(3, 28, 3),
(4, 28, 5),
(5, 27, 3),
(57, 50, 11),
(58, 50, 14);

-- --------------------------------------------------------

--
-- Table structure for table `video_orders`
--

CREATE TABLE `video_orders` (
  `id_video` int(11) NOT NULL,
  `jenis_produk` varchar(255) NOT NULL,
  `brand` varchar(255) NOT NULL,
  `target_usia` varchar(255) NOT NULL,
  `target_lokasi_geografis` varchar(255) NOT NULL,
  `target_jenis_kelamin` varchar(255) NOT NULL,
  `tone_warna` varchar(255) NOT NULL,
  `pesan_video` text NOT NULL,
  `tambahan_model` varchar(255) NOT NULL,
  `catatan` text NOT NULL,
  `id_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `video_orders`
--

INSERT INTO `video_orders` (`id_video`, `jenis_produk`, `brand`, `target_usia`, `target_lokasi_geografis`, `target_jenis_kelamin`, `tone_warna`, `pesan_video`, `tambahan_model`, `catatan`, `id_order`) VALUES
(2, 'Pendidikan Perguruan Tinggi', 'UPNVY', '17-25 tahun', 'Gedung', 'laki-laki dan perempuan', 'Cerah', 'Tidak Ada', 'model sendiri', '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `web_orders`
--

CREATE TABLE `web_orders` (
  `id_web` int(11) NOT NULL,
  `jenis_usaha` varchar(255) NOT NULL,
  `tujuan` varchar(255) NOT NULL,
  `menu` varchar(255) NOT NULL,
  `punya_domain` tinyint(1) NOT NULL,
  `punya_hosting` tinyint(1) NOT NULL,
  `catatan` text NOT NULL,
  `id_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `web_orders`
--

INSERT INTO `web_orders` (`id_web`, `jenis_usaha`, `tujuan`, `menu`, `punya_domain`, `punya_hosting`, `catatan`, `id_order`) VALUES
(1, 'pendidikan perguruan tinggi', 'Profile, Update Info, sistem informasi akademis, sistem keuangan', 'About us/Profile, Product / Services, Contact us, akademi, pembayaran', 0, 0, '', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id_admin`),
  ADD UNIQUE KEY `admins_email` (`email`);

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id_article`),
  ADD KEY `fk_article_admin` (`id_admin`);

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id_chat`),
  ADD KEY `fk_chat_order` (`id_order`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id_client`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `detail_orders`
--
ALTER TABLE `detail_orders`
  ADD PRIMARY KEY (`id_detail_order`),
  ADD KEY `fk_detail_order` (`id_order`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id_invoice`),
  ADD UNIQUE KEY `id_invoice` (`kode_invoice`),
  ADD KEY `fk_invoice_order` (`id_order`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id_order`),
  ADD KEY `fk_order_client` (`id_client`),
  ADD KEY `fk_order_packet` (`id_packet`);

--
-- Indexes for table `packets`
--
ALTER TABLE `packets`
  ADD PRIMARY KEY (`id_packet`),
  ADD UNIQUE KEY `kode` (`kode`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id_payment`),
  ADD KEY `fk_payment_invoice` (`id_invoice`);

--
-- Indexes for table `registered_clients`
--
ALTER TABLE `registered_clients`
  ADD PRIMARY KEY (`id_registered_client`),
  ADD KEY `fk_registered_admin` (`id_admin`),
  ADD KEY `fk_registered_client` (`id_client`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id_tag`),
  ADD UNIQUE KEY `nama` (`nama`);

--
-- Indexes for table `tag_article`
--
ALTER TABLE `tag_article`
  ADD PRIMARY KEY (`id_tag_article`),
  ADD KEY `fk_tag_article` (`id_article`),
  ADD KEY `fk_tag_tag` (`id_tag`);

--
-- Indexes for table `video_orders`
--
ALTER TABLE `video_orders`
  ADD PRIMARY KEY (`id_video`),
  ADD KEY `fk_video_order` (`id_order`);

--
-- Indexes for table `web_orders`
--
ALTER TABLE `web_orders`
  ADD PRIMARY KEY (`id_web`),
  ADD KEY `fk_web_order` (`id_order`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id_article` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `id_chat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id_client` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `detail_orders`
--
ALTER TABLE `detail_orders`
  MODIFY `id_detail_order` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id_invoice` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id_order` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `packets`
--
ALTER TABLE `packets`
  MODIFY `id_packet` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id_payment` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `registered_clients`
--
ALTER TABLE `registered_clients`
  MODIFY `id_registered_client` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id_tag` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tag_article`
--
ALTER TABLE `tag_article`
  MODIFY `id_tag_article` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `video_orders`
--
ALTER TABLE `video_orders`
  MODIFY `id_video` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `web_orders`
--
ALTER TABLE `web_orders`
  MODIFY `id_web` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `articles`
--
ALTER TABLE `articles`
  ADD CONSTRAINT `fk_article_admin` FOREIGN KEY (`id_admin`) REFERENCES `admins` (`id_admin`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `chat`
--
ALTER TABLE `chat`
  ADD CONSTRAINT `fk_chat_order` FOREIGN KEY (`id_order`) REFERENCES `orders` (`id_order`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `detail_orders`
--
ALTER TABLE `detail_orders`
  ADD CONSTRAINT `fk_detail_order` FOREIGN KEY (`id_order`) REFERENCES `orders` (`id_order`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `invoices`
--
ALTER TABLE `invoices`
  ADD CONSTRAINT `fk_invoice_order` FOREIGN KEY (`id_order`) REFERENCES `orders` (`id_order`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `fk_order_client` FOREIGN KEY (`id_client`) REFERENCES `clients` (`id_client`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_order_packet` FOREIGN KEY (`id_packet`) REFERENCES `packets` (`id_packet`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `fk_payment_invoice` FOREIGN KEY (`id_invoice`) REFERENCES `invoices` (`id_invoice`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `registered_clients`
--
ALTER TABLE `registered_clients`
  ADD CONSTRAINT `fk_registered_admin` FOREIGN KEY (`id_admin`) REFERENCES `admins` (`id_admin`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_registered_client` FOREIGN KEY (`id_client`) REFERENCES `clients` (`id_client`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tag_article`
--
ALTER TABLE `tag_article`
  ADD CONSTRAINT `fk_tag_article` FOREIGN KEY (`id_article`) REFERENCES `articles` (`id_article`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_tag_tag` FOREIGN KEY (`id_tag`) REFERENCES `tags` (`id_tag`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `video_orders`
--
ALTER TABLE `video_orders`
  ADD CONSTRAINT `fk_video_order` FOREIGN KEY (`id_order`) REFERENCES `orders` (`id_order`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `web_orders`
--
ALTER TABLE `web_orders`
  ADD CONSTRAINT `fk_web_order` FOREIGN KEY (`id_order`) REFERENCES `orders` (`id_order`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
