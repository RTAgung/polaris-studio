function detailPayment() {
    const button = document.querySelectorAll("#buttonArrow");

    for (let i = 0; i < button.length; i++) {
        const target = button[i].getAttribute('data-bs-target');
        button[i].addEventListener('click', () => {
            actionDetailPayment(button[i], target)
        });
    }

    function actionDetailPayment(buttonId, target) {
        const textButton = buttonId.innerHTML;
        const detailPayment = document.getElementById(target);
        if (textButton == "keyboard_arrow_down") {
            detailPayment.setAttribute("class", "js-showDetailPayment");
            buttonId.innerHTML = "keyboard_arrow_up";
        } else if (textButton == "keyboard_arrow_up") {
            detailPayment.setAttribute("class", "js-hideDetailPayment");
            buttonId.innerHTML = "keyboard_arrow_down";
        }
    }
}

function autoScrollChat() {
    let chatBox = document.getElementById("chat-box");
    if (chatBox)
        chatBox.scrollTop = chatBox.scrollHeight;
}

function generatePassword() {
    const button = document.querySelectorAll("#buttonGenerate");

    for (let i = 0; i < button.length; i++) {
        const target = button[i].getAttribute('data-bs-target');
        button[i].addEventListener('click', () => {
            actionGeneratePassword(target)
        });
    }

    function actionGeneratePassword(target) {
        const password = randomString(16);
        const formPassword = document.getElementById(target);
        formPassword.value = password;
    }
}

function randomString(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

detailPayment();
autoScrollChat();
generatePassword();