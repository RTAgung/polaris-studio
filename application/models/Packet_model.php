<?php

class Packet_model extends CI_Model
{
    public function getAllPacket(){
        return $this->db->select('*')
            ->from('packets')
            ->get()
            ->result_array();
    }

    public function getPacketByJenis($jenis){
        return $this->db->select('*')
            ->from('packets')
            ->where('jenis', $jenis)
            ->get()
            ->result_array();
    }

    public function getPacketById($id){
        return $this->db->select('*')
            ->from('packets')
            ->where('id_packet', $id)
            ->get()
            ->row_array();
    }

    public function addPacket($kode, $nama, $jenis, $harga_awal, $harga, $deskripsi, $detail){
        $data = [
            'kode' => $kode,
            'nama' => $nama,
            'jenis' => $jenis,
            'harga_awal' => $harga_awal,
            'harga' => $harga,
            'deskripsi' => $deskripsi,
            'detail' => $detail
        ];
        
        $this->db->insert('packets', $data);
    }

    public function updatePacket($id, $kode, $nama, $jenis, $harga_awal, $harga, $deskripsi, $detail){
        $data = [
            'kode' => $kode,
            'nama' => $nama,
            'jenis' => $jenis,
            'harga_awal' => $harga_awal,
            'harga' => $harga,
            'deskripsi' => $deskripsi,
            'detail' => $detail
        ];

        $this->db->where('id_packet', $id);
        $this->db->update('packets', $data);
    }
    
    public function deletePacket($id)
    {
        $this->db->delete('packets', ['id_packet' => $id]);
    }
}