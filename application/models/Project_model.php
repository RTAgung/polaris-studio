<?php

class Project_model extends CI_Model
{
    public function getAllProject($jenis, $state, $limit = 10)
    {
        $status1 = "";
        $status2 = "";
        if ($state == 'current') {
            $status1 = "on progress";
            $status2 = "pending";
        } else if ($state == 'history') {
            $status1 = "canceled";
            $status2 = "finished";
        }

        return $this->db->query("SELECT jo.*, p.nama, p.jenis 
            FROM (SELECT o.*, d.judul, d.asset 
                FROM orders o 
                LEFT JOIN detail_orders d ON o.id_order = d.id_order) jo 
            LEFT JOIN packets p ON jo.id_packet = p.id_packet
            WHERE p.jenis = '$jenis' AND status in('$status1', '$status2')
            ORDER BY status DESC, id_order DESC
            LIMIT $limit")
            ->result_array();
    }

    public function countAllProject($jenis, $state = "")
    {
        $status = [""];
        if ($state == 'current')
            $status = ['on progress', 'pending'];
        else if ($state == 'history')
            $status = ['canceled', 'finished'];

        return $this->db->select('*')
            ->from('packets')
            ->join('orders', 'orders.id_packet = packets.id_packet', 'left')
            ->where('jenis', $jenis)
            ->where_in('status', $status)
            ->count_all_results();
    }

    public function getProjectByClientId($clientId, $limit = 10)
    {
        return $this->db->query(
            "SELECT jo.*, p.jenis, p.harga, p.detail, p.nama, p.deskripsi as deskripsi_packet 
                FROM (SELECT o.*, d.judul, d.asset 
                    FROM orders o 
                    LEFT JOIN detail_orders d ON o.id_order = d.id_order) jo 
                LEFT JOIN packets p ON jo.id_packet = p.id_packet 
                WHERE id_client = $clientId
                ORDER BY status DESC, id_order DESC
                LIMIT $limit"
        )
            ->result_array();
    }

    public function countAllProjectByClientId($clientId)
    {
        return $this->db->where('id_client', $clientId)
            ->count_all_results('orders');
    }

    public function getProjectAndClient($projectId)
    {
        return $this->db->select('*')
            ->from('orders')
            ->join('clients', 'orders.id_client = clients.id_client', 'left')
            ->where('id_order', $projectId)
            ->get()
            ->row_array();
    }

    public function getPacketByProjectId($projectId)
    {
        return $this->db->select('*')
            ->from('packets')
            ->join('orders', 'orders.id_packet = packets.id_packet', 'left')
            ->where('id_order', $projectId)
            ->get()
            ->row_array();
    }

    public function getFullProject($projectId)
    {
        return $this->db->query("SELECT jo.*, p.nama 
            FROM (SELECT o.*, d.judul, d.asset 
                FROM orders o 
                LEFT JOIN detail_orders d ON o.id_order = d.id_order) jo 
            LEFT JOIN packets p ON jo.id_packet = p.id_packet 
            WHERE id_order = $projectId")
            ->row_array();
    }

    public function getDetailProject($projectId)
    {
        return $this->db->get_where('detail_orders', ["id_order" => $projectId])->row_array();
    }

    public function getChat($projectId)
    {
        return $this->db->get_where('chat', ["id_order" => $projectId])->result_array();
    }

    public function insertChat($text, $projectId, $pengirim, $label = "")
    {
        date_default_timezone_set("Asia/Jakarta");
        $date = date('Y-m-d');
        $data = array(
            "pengirim" => $pengirim,
            "teks" => $text,
            "tgl_kirim" => $date,
            "label" => $label,
            "id_order" => $projectId
        );

        $this->db->insert('chat', $data);
    }

    public function updateAsset($asset, $projectId)
    {
        $this->db->where('id_order', $projectId)
            ->update('detail_orders', ["asset" => $asset]);
    }

    public function checkProjectExist($projectId, $jenis = null)
    {
        if ($jenis == null)
            return $this->db->where('id_order', $projectId)
                ->count_all_results('orders');
        else
            return $this->db->select('o.*, p.jenis')
                ->from('orders o')
                ->join('packets p', 'o.id_packet = p.id_packet', 'left')
                ->where('o.id_order', $projectId)
                ->where('p.jenis', $jenis)
                ->count_all_results('orders');
    }

    public function checkFormRequired($projectId)
    {
        return $this->db->where('id_order', $projectId)
            ->count_all_results('detail_orders');
    }

    public function insertDataFormWeb($data, $idOrder)
    {
        $detail = array(
            'judul' => $data['judul'],
            'asset' => $data['asset'],
            'id_order' => $idOrder
        );

        $web = array(
            'jenis_usaha' => $data['jenisUsaha'],
            'tujuan' => $data['tujuan'],
            'menu' => $data['menu'],
            'punya_domain' => $data['domain'],
            'punya_hosting' => $data['hosting'],
            'catatan' => $data['catatan'],
            'id_order' => $idOrder
        );

        $this->db->insert('detail_orders', $detail);
        $this->db->insert('web_orders', $web);
        $this->setStatusProject("on progress", $idOrder);
    }

    public function updateDataFormWeb($data, $idOrder)
    {
        $detail = array(
            'judul' => $data['judul'],
            'asset' => $data['asset']
        );

        $web = array(
            'jenis_usaha' => $data['jenisUsaha'],
            'tujuan' => $data['tujuan'],
            'menu' => $data['menu'],
            'punya_domain' => $data['domain'],
            'punya_hosting' => $data['hosting'],
            'catatan' => $data['catatan']
        );

        $this->db->where('id_order', $idOrder);
        $this->db->update('detail_orders', $detail);
        $this->db->where('id_order', $idOrder);
        $this->db->update('web_orders', $web);
    }

    public function getDataWeb($idProject)
    {
        return $this->db->get_where('web_orders', ['id_order' => $idProject])->row_array();
    }

    public function insertDataFormVideo($data, $idOrder)
    {
        $detail = array(
            'judul' => $data['judul'],
            'asset' => $data['asset'],
            'id_order' => $idOrder
        );

        $video = array(
            'jenis_produk' => $data['jenisProduk'],
            'brand' => $data['merk'],
            'target_usia' => $data['usia'],
            'target_lokasi_geografis' => $data['lokasi'],
            'target_jenis_kelamin' => $data['jenisKelamin'],
            'tone_warna' => $data['tone'],
            'pesan_video' => $data['pesan'],
            'tambahan_model' => $data['model'],
            'catatan' => $data['catatan'],
            'id_order' => $idOrder
        );

        $this->db->insert('detail_orders', $detail);
        $this->db->insert('video_orders', $video);
        $this->setStatusProject("on progress", $idOrder);
    }

    public function updateDataFormVideo($data, $idOrder)
    {
        $detail = array(
            'judul' => $data['judul'],
            'asset' => $data['asset']
        );

        $video = array(
            'jenis_produk' => $data['jenisProduk'],
            'brand' => $data['merk'],
            'target_usia' => $data['usia'],
            'target_lokasi_geografis' => $data['lokasi'],
            'target_jenis_kelamin' => $data['jenisKelamin'],
            'tone_warna' => $data['tone'],
            'pesan_video' => $data['pesan'],
            'tambahan_model' => $data['model'],
            'catatan' => $data['catatan']
        );

        $this->db->where('id_order', $idOrder);
        $this->db->update('detail_orders', $detail);
        $this->db->where('id_order', $idOrder);
        $this->db->update('video_orders', $video);
    }

    public function getDataVideo($idProject)
    {
        return $this->db->get_where('video_orders', ['id_order' => $idProject])->row_array();
    }

    public function insertOrder($deskripsi, $id_client, $paket)
    {
        $order = array(
            'deskripsi' => $deskripsi,
            'status' => 'pending',
            'id_client' => $id_client,
            'id_packet' => $paket
        );

        $this->db->insert('orders', $order);
    }

    public function setStatusProject($state, $idProject)
    {
        $this->db->where('id_order', $idProject)->update('orders', ['status' => $state]);
    }

    public function deleteProject($idProject)
    {
        $this->db->delete('orders', ['id_order' => $idProject]);
    }
}
