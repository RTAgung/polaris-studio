<?php

class Client_model extends CI_Model
{
    public function getAllClient($limit = 10)
    {
        return $this->db
            ->query("SELECT c.*, COUNT(o.id_order) as project 
                FROM clients c 
                LEFT JOIN orders o ON c.id_client = o.id_client 
                GROUP BY c.id_client 
                ORDER BY c.id_client DESC 
                LIMIT $limit")
            ->result_array();
    }

    public function countAllClient()
    {
        return $this->db->count_all_results('clients');
    }

    public function insertPasswordClient($password, $admin, $client)
    {
        $data = [
            'password' => $password,
            'id_admin' => $admin,
            'id_client' => $client
        ];
        $this->db->insert('registered_clients', $data);
        $this->updateRegisteredState($client);
    }

    public function updateRegisteredState($id_client, $state = 1)
    {
        $this->db->where('id_client', $id_client)
            ->update('clients', ['is_registered' => $state]);
    }

    public function checkClientIsRegistered($id_client)
    {
        return $this->db->where('id_client', $id_client)
            ->count_all_results('registered_clients');
    }

    public function getClientById($id)
    {
        // SELECT * FROM `clients` LEFT JOIN `registered_clients` ON clients.id_client = registered_clients.id_client WHERE clients.email = 'upn@gmail.com';
        return $this->db->select('*')
            ->from('clients')
            ->join('registered_clients', 'clients.id_client = registered_clients.id_client', 'left')
            ->where('clients.id_client', $id)
            ->get()
            ->row_array();
    }

    public function getClientByEmail($email)
    {
        // SELECT * FROM `clients` LEFT JOIN `registered_clients` ON clients.id_client = registered_clients.id_client WHERE clients.email = 'upn@gmail.com';
        return $this->db->select('*')
            ->from('clients')
            ->join('registered_clients', 'clients.id_client = registered_clients.id_client', 'left')
            ->where('email', $email)
            ->get()
            ->row_array();
    }

    public function getClientByEmailNoJoin($email)
    {
        return $this->db->get_where('clients', ["email" => $email])->row_array();
    }

    public function checkEmailExist($email){
        return $this->db->where("email" , $email)->count_all_results('clients');
    }

    public function updateClientById($id, $nama_pemilik, $alamat, $instagram, $whatsapp)
    {
        $registeredClient = array(
            'nama_pemilik' => $nama_pemilik,
            'alamat' => $alamat
        );

        $client = array(
            'instagram' => $instagram,
            'whatsapp' => $whatsapp
        );

        $this->db->where('id_client', $id);
        $this->db->update('clients', $client);
        $this->db->where('id_client', $id);
        $this->db->update('registered_clients', $registeredClient);
    }

    public function getIdClientByEmail($email)
    {
        return $this->db->select('id_client')
            ->from('clients')
            ->where('email', $email)
            ->get()
            ->row_array();
    }

    public function insertClient($nama, $email, $instagram, $whatsapp)
    {
        $client = array(
            'nama_instansi' => $nama,
            'email' => $email,
            'instagram' => $instagram,
            'whatsapp' => $whatsapp
        );

        $this->db->insert('clients', $client);
        return $this->getClientByEmailNoJoin($email);
    }

    public function updatePassword($id, $password)
    {
        $this->db->set('password', $password);
        $this->db->where('id_client', $id);
        $this->db->update('registered_clients');
    }

    public function deleteClient($id)
    {
        $this->db->delete('clients', ['id_client' => $id]);
    }
}
