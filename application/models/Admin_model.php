<?php
class Admin_model extends CI_Model
{
    public function getAdminByEmail($email)
    {
        return $this->db->get_where('admins', ['email' => $email])
            ->row_array();
    }

    public function getAllAdmin()
    {
        return $this->db->select('*')
            ->from('admins')
            //            ->where('jabatan !=', 'manajer')
            ->get()
            ->result_array();
    }

    public function insertAdmin($data)
    {
        $this->db->insert('admins', $data);
    }

    public function updateAdmin($id, $data)
    {
        $this->db
            ->where('id_admin', $id)
            ->update('admins', $data);
    }

    public function updatePassword($id, $password)
    {
        $this->db->set('password', $password);
        $this->db->where('id_admin', $id);
        $this->db->update('admins');
    }

    public function deleteAdmin($id)
    {
        $this->db->delete('admins', ['id_admin' => $id]);
    }
}
