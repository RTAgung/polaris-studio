<?php

class Main_model extends CI_Model
{
    public function getVideoDummy($index){
        $index = $index-1;

        $dummy = array(
            array(
                "title" => "Dari Bajigur Sampai Kumis Melintir / Erwan Sudiwjaya",
                "link" => "https://www.youtube.com/embed/3jonfiItaPQ", 
                "deskripsi" => "Cast 2 Cast, sebuah obrolan singkat tentang perkembangan dunia digital, dunia kreatif dan fenomena belakangan ini."
            ),
            array(
                "title" => "Iklan Seblak Baraka – Bikin Tobat",
                "link" => "https://www.youtube.com/embed/q1v6q-9As20", 
                "deskripsi" => "Client: Baraka Sundanese<br>
                Produksi : Polaris Studio<br>
                Director: Heru Prasetyo<br>
                DOP: Heru Prasetyo<br>
                Editor: Heru Prasetyo & Indra Triwahyudy<br>
                <br>
                Agustus 2020<br>"
            ),
            array(
                "title" => "Warung Layar Sentuh",
                "link" => "https://www.youtube.com/embed/WSnRrfOH3gE", 
                "deskripsi" => "Suatu hari kami berkunjung ke sebuah warung yang nikmat suasananya. Eh tak hanya suasanya yang chill.. tapi menunya juga humble banget dengan Indonesia. Uniknya, pesan menunya cukup di sentuh aja..
                wah jadi kangen kesana lagi..<br>
                <br>
                DOP: Heru Prasetyo<br>
                Editor: Heru Prasetyo & Indra Triwahyudy<br>
                <br>
                Oktober 2020<br>"
            ),
            array(
                "title" => "Iklan JAEGER – Jahe merah bubuk",
                "link" => "https://www.youtube.com/embed/kpBqKXXVkZo", 
                "deskripsi" => "Client: Niteni, Minuman Herbal<br>
                Produksi : Polaris Studio<br>
                Director: Indra Triwahyudy<br>
                DOP: Kiko<br>
                Editor: Indra Triwahyudy<br>
                <br>
                Agustus 2020<br>"
            ),
            array(
                "title" => "TIPS produk diminati Konsumen / Video Untuk Yayasan PLAN",
                "link" => "https://www.youtube.com/embed/mAISqpPHWwE", 
                "deskripsi" => "Video edukasi untuk usaha yang dibuat untuk Yayasan PLAN Internasional Indonesia."
            ),
            array(
                "title" => "Iklan Niteni Wedang Uwuh – Run",
                "link" => "https://www.youtube.com/embed/eAqb2OUWFBs", 
                "deskripsi" => "Client: Niteni, Minuman Herbal<br>
                Produksi: Polaris Studio<br>
                <br>
                September 2020<br>"
            ),
        );

        return $dummy[$index];
    }
}
