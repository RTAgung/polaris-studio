<?php
class Article_model extends CI_Model
{
    public function getArticle()
    {
        // SELECT * FROM articles desc
        return $this->db->select('*')
            ->from('articles')
            ->order_by('id_article', 'DESC')
            ->get()
            ->result_array();
    }

    public function getLastArticle(){
        return $this->db->select('*')
            ->from('articles')
            ->order_by('id_article', 'DESC')
            ->limit(1)
            ->get()
            ->row_array();
    }

    public function getDetailArticle($id)
    {
        return $this->db->select('*')
            ->from('articles')
            ->where('id_article', $id)
            ->get()
            ->row_array();
    }

    public function getAllArticleByTag($id_tag){
        return $this->db->select('*')
            ->from('tag_article')
            ->join('articles', 'tag_article.id_article = articles.id_article', 'left')
            ->where('id_tag', $id_tag)
            ->get()
            ->result_array();
    }

    public function uploadArticle($data){
        $this->db->insert('articles ', $data);
    }

    public function updateArticle($id, $data){
        $this->db
            ->where('id_article', $id)
            ->update('articles', $data);
    }

    public function deleteArticle($id){
        $this->db->delete('articles', ['id_article' => $id]);
    }

    public function checkDetailExsist($id)
    {
        return $this->db->where('id_article', $id)
            ->count_all_results('articles');
    }

    public function getAllTags()
    {
        return $this->db->select('*')
            ->from('tags')
            ->get()
            ->result_array();
    }

    public function getTags($id)
    {
        return $this->db->select('*')
            ->from('tag_article')
            ->join('tags', 'tag_article.id_tag = tags.id_tag', 'left')
            ->where('id_article', $id)
            ->get()
            ->result_array();
    }

    public function getTagById($id_tag){
        return $this->db->select('*')
            ->from('tags')
            ->where('id_tag', $id_tag)
            ->get()
            ->row_array();
    }

    public function checkTagExist($tag)
    {
        return $this->db->where('nama', $tag)
            ->count_all_results('tags');
    }

    public function insertTag($tag)
    {
        $data = array(
            'nama' => $tag
        );

        $this->db->insert('tags', $data);
    }

    public function insertArticleTag($data){
        $this->db->insert('tag_article', $data);
    }

    public function deleteArticleTags($id){
        $this->db->delete('tag_article', ['id_article' => $id]);
    }

    public function deleteTag($id)
    {
        $this->db->delete('tags', ['id_tag' => $id]);
    }

    public function updateTag($id, $tag)
    {
        $this->db
            ->where('id_tag', $id)
            ->update('tags', ["nama" => $tag]);
    }

    public function getIdArticleByTag($id_tag, $id)
    {
        return $this->db->select('*')
            ->from('tag_article')
            ->where('id_tag', $id_tag)
            ->where('id_article !=', $id)
            ->limit(3)
            ->get()
            ->result_array();
    }

    public function getArticleByTag($id_article)
    {
        return $this->db->select('*')
            ->from('articles')
            ->where_in('id_article', $id_article)
            ->limit(3)
            ->get()
            ->result_array();
    }
}
