<?php

class Payment_model extends CI_Model
{
    public function getAllInvoice($state, $limit = 10)
    {
        $status1 = "";
        $status2 = "";
        if ($state == 'current') {
            $status1 = "unpaid";
            $status2 = "waiting";
        } else if ($state == 'history') {
            $status1 = "paid";
            $status2 = "canceled";
        }

        return $this->db->select('i.* , p.bukti, p.konfirmasi')
            ->from('invoices i')
            ->join('payments p', 'i.id_invoice = p.id_invoice', 'left')
            ->order_by('i.status DESC, i.id_invoice DESC')
            ->where_in('i.status', [$status1, $status2])
            ->limit($limit)
            ->get()
            ->result_array();
    }

    public function countAllInvoice($state = "")
    {
        $status = [""];
        if ($state == 'current')
            $status = ['unpaid', 'waiting'];
        else if ($state == 'history')
            $status = ['paid', 'canceled'];

        return $this->db->where_in('status', $status)
            ->count_all_results('invoices');
    }

    public function checkExistInvoice($invoiceId)
    {
        return $this->db->where('id_invoice', $invoiceId)
            ->count_all_results('invoices');
    }

    public function getInvoiceByClientId($clientId, $limit = 10)
    {
        return $this->db->select('o.*, i.* , p.bukti, p.konfirmasi')
            ->from('invoices i')
            ->join('orders o', 'o.id_order = i.id_order', 'left')
            ->join('payments p', 'i.id_invoice = p.id_invoice', 'left')
            ->order_by('i.status DESC, i.id_invoice DESC')
            ->where('id_client', $clientId)
            ->limit($limit)
            ->get()
            ->result_array();
    }

    public function countInvoiceByClient($clientId)
    {
        return $this->db->select('*')
            ->from('invoices')
            ->join('orders', 'orders.id_order = invoices.id_order', 'left')
            ->where('id_client', $clientId)
            ->count_all_results();
    }

    public function getInvoiceByProjectId($projectId, $term = null)
    {
        if ($term == null)
            return $this->db->order_by('id_invoice', 'DESC')
                ->get_where('invoices', ['id_order' => $projectId])
                ->result_array();
        else
            return $this->db->where('id_order', $projectId)
                ->where('term <=', $term)
                ->get_where('invoices', ['id_order' => $projectId])
                ->result_array();
    }

    public function getInvoiceByInvoiceId($invoiceId)
    {
        return $this->db->get_where('invoices', ['id_invoice' => $invoiceId])->row_array();
    }

    public function getPaymentByInvoiceId($invoiceId)
    {
        return $this->db->get_where('payments', ['id_invoice' => $invoiceId])->row_array();
    }

    public function uploadPayment($invoiceId, $bukti)
    {
        $data = [
            'id_invoice' => $invoiceId,
            'bukti' => $bukti,
            'konfirmasi' => 0
        ];
        $this->db->insert('payments', $data);
    }

    public function uploadInvoice($data)
    {
        $this->db->insert('invoices', $data);
    }

    public function updateInvoice($data, $invoiceId)
    {
        $this->db->where('id_invoice', $invoiceId);
        $this->db->update('invoices', $data);
    }

    public function updateStatusInvoice($invoiceId, $status)
    {
        $this->db->where('id_invoice', $invoiceId)
            ->update('invoices', ['status' => $status]);
    }

    public function confirmPayment($invoiceId, $state = 1)
    {
        $this->db->where('id_invoice', $invoiceId)
            ->update('payments', ['konfirmasi' => $state]);
        $this->updateStatusInvoice($invoiceId, 'paid');
    }

    public function cancelInvoice($invoiceId)
    {
        $this->updateStatusInvoice($invoiceId, 'canceled');
    }
}
