<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'main/redirect';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// client side
$route['client'] = 'client_auth/redirect';
$route['client/login'] = 'client_auth';
$route['client/logout'] = 'client_auth/logout';
$route['client/dashboard'] = 'client_dashboard';
$route['client/dashboard/edit-profile'] = 'client_dashboard/edit_profile';
$route['client/dashboard/edit-password'] = 'client_dashboard/edit_password';
$route['client/project'] = 'client_project';
$route['client/project/(:any)/form-order'] = 'client_project/form_order/$1';
$route['client/project/(:any)'] = 'client_project/detail/$1';
$route['client/payment'] = 'client_payment';
$route['client/payment/upload/(:any)'] = 'client_payment/upload_payment/$1';


// main side
$route['main'] = 'main';
$route['main/index'] = 'main';
$route['main/web'] = 'main/web';
$route['main/video'] = 'main/video';
$route['main/kontak'] = 'main/kontak';
$route['main/form-web'] = 'main/form_web';
$route['main/form-video'] = 'main/form_video';
$route['main/artikel'] = 'main/artikel';
$route['main/artikel/detail/(:any)'] = 'main/artikel_detail/$1';
$route['main/artikel/tag/(:any)'] = 'main/artikel_tag/$1';
$route['main/video/detail/(:any)'] = 'main/video_detail/$1';


// admin side
// RTA
$route['po-admin'] = 'admin_auth/redirect';
$route['po-admin/login'] = 'admin_auth';
$route['po-admin/logout'] = 'admin_auth/logout';
$route['po-admin/dashboard'] = 'admin_dashboard';
$route['po-admin/client'] = 'admin_client';
$route['po-admin/client/(:any)'] = 'admin_client/detail/$1';
$route['po-admin/client/(:any)/edit'] = 'admin_client/edit/$1';
$route['po-admin/client/(:any)/delete'] = 'admin_client/delete/$1';
$route['po-admin/project/web'] = 'admin_project/web';
$route['po-admin/project/web/(:any)'] = 'admin_project/web_detail/$1';
$route['po-admin/project/web/(:any)/edit'] = 'admin_project/web_edit/$1';
$route['po-admin/project/video'] = 'admin_project/video';
$route['po-admin/project/video/(:any)'] = 'admin_project/video_detail/$1';
$route['po-admin/project/video/(:any)/edit'] = 'admin_project/video_edit/$1';
$route['po-admin/project/status/(:any)/(:any)'] = 'admin_project/set_status/$1/$2';
$route['po-admin/project/delete/(:any)'] = 'admin_project/delete/$1';
$route['po-admin/payment'] = 'admin_payment';
$route['po-admin/payment/(:any)'] = 'admin_payment/detail/$1';
$route['po-admin/payment/(:any)/confirm'] = 'admin_payment/confirm/$1';
$route['po-admin/payment/(:any)/cancel'] = 'admin_payment/cancel/$1';
$route['po-admin/payment/upload/(:any)'] = 'admin_payment/upload/$1';
// LA
$route['po-admin/packet'] = 'admin_packet'; // done
$route['po-admin/packet/new'] = 'admin_packet/new'; // done
$route['po-admin/packet/edit/(:any)'] = 'admin_packet/edit/$1'; // done
$route['po-admin/packet/delete/(:any)'] = 'admin_packet/delete/$1'; // done
$route['po-admin/management'] = 'admin_management'; // done
$route['po-admin/management/new'] = 'admin_management/new'; // done
$route['po-admin/management/edit/(:any)'] = 'admin_management/edit/$1'; // done
$route['po-admin/management/delete/(:any)'] = 'admin_management/delete/$1'; // done
$route['po-admin/management/edit_password'] = 'Admin_management_password/edit_password'; // done
$route['po-admin/article'] = 'admin_article'; // done
$route['po-admin/article/new'] = 'admin_article/new'; // done
$route['po-admin/article/detail/(:any)'] = 'admin_article/detail/$1'; // done
$route['po-admin/article/edit/(:any)'] = 'admin_article/edit/$1'; // done
$route['po-admin/article/delete/(:any)'] = 'admin_article/delete/$1'; // done
$route['po-admin/article/tag'] = 'admin_article/tag'; // done
$route['po-admin/article/tag/new'] = 'admin_article/tag_new'; // done
$route['po-admin/article/tag/delete/(:any)'] = 'admin_article/tag_delete/$1'; // done
$route['po-admin/article/tag/edit/(:any)'] = 'admin_article/tag_edit/$1'; // done
