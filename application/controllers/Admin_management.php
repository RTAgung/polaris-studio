<?php

class Admin_management extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!logged_in_admin())
            redirect('/po-admin/login');
        $this->load->model('Admin_model');
        $this->load->helper('text');

        $email = getAdminEmailSession();
        $admin = $this->Admin_model->getAdminByEmail($email);
        if ($admin['jabatan'] != "manajer") {
            $this->session->set_flashdata('access_denied', '<span class="text-smaller text-warning ml-2">*access denied</span>');
            redirect('/po-admin');
        }
    }

    public function index()
    {
        $dataHeader = $this->setHeaderData("Management");

        $admin_data = $this->Admin_model->getAllAdmin();

        for ($i = 0; $i < count($admin_data); $i++) {
            if ($admin_data[$i]['no_hp'][0] == '0') {
                $admin_data[$i]['no_hp'] = $this->konversiNomorHP($admin_data[$i]['no_hp']);
            }
        }

        $data['admin'] = $admin_data;

        $this->load->view('admin/template/header', $dataHeader);
        $this->load->view('admin/management/index', $data);
        $this->load->view('admin/template/footer');
    }

    public function new()
    {
        $this->form_validation->set_rules('nama', 'Nama', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required|trim');
        $this->form_validation->set_rules('no_hp', 'Nomor Handphone', 'required|trim');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required|trim');
        $this->form_validation->set_rules('lahir', 'Lahir', 'required|trim');
        $this->form_validation->set_rules('masuk', 'Masuk', 'required|trim');

        if ($this->form_validation->run()) {
            if (isset($_POST['submit'])) {
                $nama = $this->input->post('nama');
                $email = $this->input->post('email');
                $password = $this->input->post('password');
                $no_hp = $this->input->post('no_hp');
                $alamat = $this->input->post('alamat');
                $lahir = $this->input->post('lahir');
                $masuk = $this->input->post('masuk');

                $password = password_hash($password, PASSWORD_DEFAULT);
                $lahir = date("Y-m-d", strtotime($lahir));
                $masuk = date("Y-m-d", strtotime($masuk));

                $data = [
                    "nama" => $nama,
                    "email" => $email,
                    "password" => $password,
                    "no_hp" => $no_hp,
                    "alamat" => $alamat,
                    "tgl_lahir" => $lahir,
                    "tgl_masuk" => $masuk,
                    "jabatan" => "admin"
                ];

                $this->Admin_model->insertAdmin($data);
            }
        }
        redirect('/po-admin/management');
    }

    public function edit($id)
    {
        $email = getAdminEmailSession();
        $admin = $this->Admin_model->getAdminByEmail($email);

        $this->form_validation->set_rules('nama', 'Nama', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
        $this->form_validation->set_rules('no_hp', 'Nomor Handphone', 'required|trim');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required|trim');
        $this->form_validation->set_rules('lahir', 'Lahir', 'required|trim');
        $this->form_validation->set_rules('masuk', 'Masuk', 'required|trim');

        if ($this->form_validation->run()) {
            if (isset($_POST['submit'])) {
                $nama = $this->input->post('nama');
                $email = $this->input->post('email');
                $no_hp = $this->input->post('no_hp');
                $alamat = $this->input->post('alamat');
                $lahir = $this->input->post('lahir');
                $masuk = $this->input->post('masuk');

                $password = $this->input->post('password');

                $lahir = date("Y-m-d", strtotime($lahir));
                $masuk = date("Y-m-d", strtotime($masuk));
                $data = '';

                if ($password != "") {
                    $password = password_hash($password, PASSWORD_DEFAULT);
                    $data = [
                        "nama" => $nama,
                        "email" => $email,
                        "password" => $password,
                        "no_hp" => $no_hp,
                        "alamat" => $alamat,
                        "tgl_lahir" => $lahir,
                        "tgl_masuk" => $masuk
                    ];
                } else {
                    $data = [
                        "nama" => $nama,
                        "email" => $email,
                        "no_hp" => $no_hp,
                        "alamat" => $alamat,
                        "tgl_lahir" => $lahir,
                        "tgl_masuk" => $masuk
                    ];
                }

                var_dump($data);

                $this->Admin_model->updateAdmin($id, $data);
            }
        }
        redirect('/po-admin/management');
    }

    public function delete($id)
    {
        $this->Admin_model->deleteAdmin($id);
        redirect('/po-admin/management');
    }

    private function setHeaderData($title)
    {
        $email = getAdminEmailSession();
        $admin = $this->Admin_model->getAdminByEmail($email);

        $data['header_title'] = $title;
        $data['header_admin'] = $admin['nama'];
        // fill '' or 'menu-open'
        $data['nav_menu_open'] = '';
        // fill '' or 'my-bg-black'
        $data['nav_dashboard'] = '';
        $data['nav_client'] = '';
        $data['nav_video'] = '';
        $data['nav_web'] = '';
        $data['nav_payment'] = '';
        $data['nav_packet'] = '';
        $data['nav_article'] = '';
        $data['nav_admin'] = 'my-bg-black';
        $data['admin'] = $admin;
        return $data;
    }

    private function konversiNomorHP($nomor)
    {
        $nomor = str_replace(" ", "", $nomor);
        $nomor = str_replace("(", "", $nomor);
        $nomor = str_replace(")", "", $nomor);
        $nomor = str_replace(".", "", $nomor);
        $hasil = '';

        if (!preg_match('/[^+0-9]/', trim($nomor))) {
            if (substr(trim($nomor), 0, 3) == '+62') {
                $hasil = trim($nomor);
            } elseif (substr(trim($nomor), 0, 1) == '0') {
                $hasil = '+62' . substr(trim($nomor), 1);
            }
        }
        $hasil = str_replace("+", "", $hasil);
        return $hasil;
    }
}
