<?php

class Admin_management_password extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!logged_in_admin())
            redirect('/po-admin/login');
        $this->load->model('Admin_model');
        $this->load->helper('text');
    }

    public function edit_password()
    {
        $email = getAdminEmailSession();
        $admin = $this->Admin_model->getAdminByEmail($email);

        $dataHeader = $this->setHeaderData("Edit Password");

        $this->form_validation->set_rules('pwlama', 'Password Lama', 'required|trim|min_length[8]|max_length[32]');
        $this->form_validation->set_rules('pwbaru', 'Password Baru', 'required|trim|min_length[8]|max_length[32]');
        $this->form_validation->set_rules('konfirmasipw', 'Konfirmasi Password Baru', 'required|trim|min_length[8]|max_length[32]');

        if ($this->form_validation->run()) {
            if (isset($_POST['submit'])) {
                $pw_lama = $this->input->post('pwlama');
                $pw_baru = $this->input->post('pwbaru');
                $konfirmasi_pw = $this->input->post('konfirmasipw');

                if (!password_verify($pw_lama, $admin['password'])) {
                    $this->session->set_flashdata('pwlama', '<small class="text-danger">Password Lama Salah</small>');
                    redirect('/po-admin/management/edit_password');
                } else if ($pw_baru != $konfirmasi_pw) {
                    $this->session->set_flashdata('konfirmasipw', '<small class="text-danger">Konfirmasi Password salah</small>');
                    redirect('/po-admin/management/edit_password');
                }

                $pw_lama = password_hash($pw_lama, PASSWORD_DEFAULT);
                $pw_baru = password_hash($pw_baru, PASSWORD_DEFAULT);
                $konfirmasi_pw = password_hash($konfirmasi_pw, PASSWORD_DEFAULT);

                $this->Admin_model->updatePassword($admin['id_admin'], $pw_baru);
                $this->session->set_flashdata('update_pw_berhasil', '<script type="text/javascript">alert("Password berhasil diubah");</script>');
                redirect('/po-admin/management/edit_password');
            }
        } else {
            $this->load->view('admin/template/header', $dataHeader);
            $this->load->view('admin/management/edit');
            $this->load->view('admin/template/footer');
        }
    }

    private function setHeaderData($title)
    {
        $email = getAdminEmailSession();
        $admin = $this->Admin_model->getAdminByEmail($email);

        $data['header_title'] = $title;
        $data['header_admin'] = $admin['nama'];
        // fill '' or ' menu-open'
        $data['nav_menu_open'] = '';
        // fill '' or 'my-bg-black'
        $data['nav_dashboard'] = '';
        $data['nav_client'] = '';
        $data['nav_video'] = '';
        $data['nav_web'] = '';
        $data['nav_payment'] = '';
        $data['nav_packet'] = '';
        $data['nav_article'] = '';
        $data['nav_admin'] = '';
        $data['admin'] = $admin;
        return $data;
    }
}
