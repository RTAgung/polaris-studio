<?php

class Client_payment extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!logged_in())
            redirect('/client/login');
        $this->load->model('Client_model');
        $this->load->model('Project_model');
        $this->load->model('Payment_model');
    }

    public function index()
    {
        $email = getClientEmailSession();
        $client = $this->Client_model->getIdClientByEmail($email);
        $sumOfInvoice = $this->Payment_model->countInvoiceByClient($client['id_client']);
        if (isset($_GET['more'])) {
            $dataInvoice = $this->Payment_model->getInvoiceByClientId($client['id_client'], $sumOfInvoice);
        } else {
            $dataInvoice = $this->Payment_model->getInvoiceByClientId($client['id_client']);
        }
        for ($i = 0; $i < count($dataInvoice); $i++) {
            $projectId = $dataInvoice[$i]['id_order'];

            $packet = $this->Project_model->getPacketByProjectId($projectId);
            $dataInvoice[$i]['harga_paket'] = $packet['harga'];
            $dataInvoice[$i]['nama_paket'] = $packet['nama'];

            $invoiceByProject = $this->Payment_model->getInvoiceByProjectId($projectId, $dataInvoice[$i]['term']);
            $dataInvoice[$i]['detail_invoice'] = $invoiceByProject;

            $detailProject = $this->Project_model->getDetailProject($projectId);
            $dataInvoice[$i]['judul'] = $detailProject['judul'];

            $dataInvoice[$i]['tgl_keluar'] = dateFormat($dataInvoice[$i]['tgl_keluar'], "d/m/Y");
        }
        $data['invoices'] = $dataInvoice;
        $data['countInvoice'] = $sumOfInvoice;
        $dataHeader = $this->setHeaderData('Pembayaran');

        $this->load->view('client/template/header', $dataHeader);
        $this->load->view('client/payment/index', $data);
        $this->load->view('client/template/footer');
        $this->session->unset_userdata('upload_payment');
    }

    public function upload_payment($id_invoice)
    {
        if (isset($_POST['submit'])) {
            $invoice = $this->Payment_model->getInvoiceByInvoiceId($id_invoice);

            $config['upload_path'] = './asset/file/payment/';
            $config['file_name'] = "bukti_" . $invoice['kode_invoice'] . "_term" . $invoice['term'];
            $config['file_ext_tolower'] = TRUE;
            $config['overwrite'] = TRUE;
            $config['allowed_types'] = 'jpeg|jpg|png|pdf';
            $config['max_size'] = 500;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('buktiPembayaran')) {
                $error = array('error' => $this->upload->display_errors());
                $this->session->set_flashdata('upload_payment', "<p class='text-red text-center m-0'>error :</p><small class='text-red text-center'>" . $error['error'] . "</small>");
            } else {
                $data = $this->upload->data();
                $filename = $data['file_name'];

                $this->Payment_model->uploadPayment($id_invoice, $filename);
                $this->Payment_model->updateStatusInvoice($id_invoice, "waiting");

                $this->session->set_flashdata('upload_payment', '<p class="text-yellow text-center m-0">Bukti Pembayaran berhasil dikirim</p>');
            }
        } else {
            show_404();
        }
        
        if (isset($_GET['d'])) {
            redirect('/client/dashboard');
        } else {
            redirect('/client/payment');
        }
    }

    private function setHeaderData($title)
    {
        $konsultasi = "Halo polaris, \n\nSaya ingin konsultasi";
        $data['konsultasi'] = rawurlencode($konsultasi);
        $data['header_title'] = $title;
        $data['header_dashboard'] = 'navbar-hover';
        $data['header_project'] = 'navbar-hover';
        $data['header_payment'] = 'navbar-hover-active';
        return $data;
    }
}
