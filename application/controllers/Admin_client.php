<?php

class Admin_client extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!logged_in_admin())
            redirect('/po-admin/login');
        $this->load->model('Admin_model');
        $this->load->model('Client_model');
        $this->load->model('Project_model');
        $this->load->model('Payment_model');
    }

    public function index()
    {
        $this->form_validation->set_rules(
            'password',
            'Password',
            'required|trim|min_length[8]|max_length[32]'
        );

        if ($this->form_validation->run()) {
            $this->_insertPassword();
        } else {
            $dataHeader = $this->setHeaderData("Client");

            $countClient = $this->Client_model->countAllClient();

            if (isset($_GET['more']))
                $clients = $this->Client_model->getAllClient($countClient);
            else
                $clients = $this->Client_model->getAllClient();

            for ($i = 0; $i < count($clients); $i++) {
                $clients[$i]['whatsapp'] = konversiNomorHP($clients[$i]['whatsapp']);
            }

            $data['clients'] = $clients;
            $data['countClient'] = $countClient;

            $this->load->view('admin/template/header', $dataHeader);
            $this->load->view('admin/client/index', $data);
            $this->load->view('admin/template/footer');
        }
    }

    private function _insertPassword()
    {
        $id = $this->input->post('id');
        $password = $this->input->post('password');
        $password = password_hash($password, PASSWORD_DEFAULT);

        $email = getAdminEmailSession();
        $admin = $this->Admin_model->getAdminByEmail($email);

        $this->Client_model->insertPasswordClient($password, $admin['id_admin'], $id);
        redirect('/po-admin/client');
    }

    public function detail($id_client)
    {
        if ($this->Client_model->checkClientIsRegistered($id_client)) {
            $this->form_validation->set_rules(
                'password',
                'Password',
                'required|trim|min_length[8]|max_length[32]'
            );

            if ($this->form_validation->run()) {
                $this->_updatePassword();
            } else {
                $client = $this->Client_model->getClientById($id_client);
                $client['whatsapp'] = konversiNomorHP($client['whatsapp']);

                // get data project
                $countProjects = $this->Project_model->countAllProjectByClientId($id_client);
                if (isset($_GET['pr_m']))
                    $projects = $this->Project_model->getProjectByClientId($id_client, $countProjects);
                else
                    $projects = $this->Project_model->getProjectByClientId($id_client, 5);

                // get data invoice
                $countInvoices = $this->Payment_model->countInvoiceByClient($id_client);
                if (isset($_GET['py_m']))
                    $invoices = $this->Payment_model->getInvoiceByClientId($id_client, $countInvoices);
                else
                    $invoices = $this->Payment_model->getInvoiceByClientId($id_client, 5);

                for ($i = 0; $i < $countInvoices; $i++) {
                    $projectId = $invoices[$i]['id_order'];

                    $packet = $this->Project_model->getPacketByProjectId($projectId);
                    $invoices[$i]['harga_paket'] = $packet['harga'];
                    $invoices[$i]['nama_paket'] = $packet['nama'];

                    $detailProject = $this->Project_model->getDetailProject($projectId);
                    $invoices[$i]['judul'] = $detailProject['judul'];
                }

                // set all data 
                $data['client'] = $client;
                $data['projects'] = $projects;
                $data['countProjects'] = $countProjects;
                $data['invoices'] = $invoices;
                $data['countInvoices'] = $countInvoices;
                $dataHeader = $this->setHeaderData("Detail Client");

                $this->load->view('admin/template/header', $dataHeader);
                $this->load->view('admin/client/detail', $data);
                $this->load->view('admin/template/footer');
            }
        } else {
            show_404();
        }
    }

    private function _updatePassword()
    {
        $id = $this->input->post('id');
        $password = $this->input->post('password');
        $password = password_hash($password, PASSWORD_DEFAULT);

        $this->Client_model->updatePassword($id, $password);
        redirect('/po-admin/client/' . $id);
    }

    public function edit($id_client)
    {
        if ($this->Client_model->checkClientIsRegistered($id_client)) {
            $this->form_validation->set_rules('namaPemilikInstansi', 'Nama Pemilik', 'required|trim');
            $this->form_validation->set_rules('alamat', 'Alamat', 'required|trim|max_length[250]');
            $this->form_validation->set_rules('instagram', 'Instagram', 'required|trim');
            $this->form_validation->set_rules('whatsapp', 'Whatsapp', 'required|trim');

            if ($this->form_validation->run()) {
                $this->_updateClient($id_client);
            } else {
                $client = $this->Client_model->getClientById($id_client);

                $data['client'] = $client;
                $dataHeader = $this->setHeaderData("Edit Client");

                $this->load->view('admin/template/header', $dataHeader);
                $this->load->view('admin/client/edit', $data);
                $this->load->view('admin/template/footer');
            }
        } else {
            show_404();
        }
    }

    private function _updateClient($id_client)
    {
        $nama_pemilik = $this->input->post('namaPemilikInstansi');
        $alamat = $this->input->post('alamat');
        $instagram = $this->input->post('instagram');
        $whatsapp = $this->input->post('whatsapp');

        $this->Client_model->updateClientById($id_client, $nama_pemilik, $alamat, $instagram, $whatsapp);
        redirect('/po-admin/client/' . $id_client);
    }

    public function delete($id_client)
    {
        if (isset($_POST['submitDelete'])) {
            $this->Client_model->deleteClient($id_client);
            redirect('/po-admin/client');
        } else {
            show_404();
        }
    }

    private function setHeaderData($title)
    {
        $email = getAdminEmailSession();
        $admin = $this->Admin_model->getAdminByEmail($email);

        $data['header_title'] = $title;
        $data['header_admin'] = $admin['nama'];
        // fill '' or 'menu-open'
        $data['nav_menu_open'] = '';
        // fill '' or 'my-bg-black'
        $data['nav_dashboard'] = '';
        $data['nav_client'] = 'my-bg-black';
        $data['nav_video'] = '';
        $data['nav_web'] = '';
        $data['nav_payment'] = '';
        $data['nav_packet'] = '';
        $data['nav_article'] = '';
        $data['nav_admin'] = '';
        return $data;
    }
}
