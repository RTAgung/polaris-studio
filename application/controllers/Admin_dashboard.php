<?php

class Admin_dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!logged_in_admin())
            redirect('/po-admin/login');
        $this->load->model('Admin_model');
        $this->load->model('Client_model');
        $this->load->model('Project_model');
        $this->load->model('Payment_model');
    }

    public function index()
    {
        $this->form_validation->set_rules(
            'password',
            'Password',
            'required|trim|min_length[8]|max_length[32]'
        );

        if ($this->form_validation->run()) {
            $this->_insertPassword();
        } else {
            $data = [
                'clients' => $this->getClientData(),
                'web' => $this->getProjectData('web'),
                'video' => $this->getProjectData('video'),
                'payments' => $this->getPaymentData()
            ];
            $dataHeader = $this->setHeaderData("Dashboard");

            $this->load->view('admin/template/header', $dataHeader);
            $this->load->view('admin/dashboard/index', $data);
            $this->load->view('admin/template/footer');
            $this->session->unset_userdata('access_denied');
        }
    }

    private function _insertPassword()
    {
        $id = $this->input->post('id');
        $password = $this->input->post('password');
        $password = password_hash($password, PASSWORD_DEFAULT);

        $email = getAdminEmailSession();
        $admin = $this->Admin_model->getAdminByEmail($email);

        $this->Client_model->insertPasswordClient($password, $admin['id_admin'], $id);
        redirect('/po-admin/dashboard');
    }

    private function getPaymentData()
    {
        $payments['count'] = $this->Payment_model->countAllInvoice('current');
        $payments['data'] = $this->Payment_model->getAllInvoice('current', 5);

        for ($i = 0; $i < count($payments['data']); $i++) {
            $projectId = $payments['data'][$i]['id_order'];

            $packet = $this->Project_model->getPacketByProjectId($projectId);
            $payments['data'][$i]['harga_paket'] = $packet['harga'];
            $payments['data'][$i]['nama_paket'] = $packet['nama'];

            $detailProject = $this->Project_model->getDetailProject($projectId);
            $payments['data'][$i]['judul'] = $detailProject['judul'];
        }

        return $payments;
    }

    private function getProjectData($type)
    {
        $data['count'] = $this->Project_model->countAllProject($type, 'current');
        $data['data'] = $this->Project_model->getAllProject($type, 'current', 4);

        return $data;
    }

    private function getClientData()
    {
        $clients['count'] = $this->Client_model->countAllClient();
        $clients['data'] = $this->Client_model->getAllClient(5);

        for ($i = 0; $i < count($clients['data']); $i++) {
            $clients['data'][$i]['whatsapp'] = konversiNomorHP($clients['data'][$i]['whatsapp']);
        }

        return $clients;
    }

    private function setHeaderData($title)
    {
        $email = getAdminEmailSession();
        $admin = $this->Admin_model->getAdminByEmail($email);

        $data['header_title'] = $title;
        $data['header_admin'] = $admin['nama'];
        // fill '' or 'menu-open'
        $data['nav_menu_open'] = '';
        // fill '' or 'my-bg-black'
        $data['nav_dashboard'] = 'my-bg-black';
        $data['nav_client'] = '';
        $data['nav_video'] = '';
        $data['nav_web'] = '';
        $data['nav_payment'] = '';
        $data['nav_packet'] = '';
        $data['nav_article'] = '';
        $data['nav_admin'] = '';
        return $data;
    }
}
