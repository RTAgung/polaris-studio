<?php

class Client_dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!logged_in())
            redirect('/client/login');
        $this->load->model('Client_model');
        $this->load->model('Project_model');
        $this->load->model('Payment_model');
    }

    public function index()
    {
        $dataHeader = $this->setHeaderData("Menu Utama");

        $email = getClientEmailSession();
        $client = $this->Client_model->getClientByEmail($email);
        $data['client'] = $client;

        $sumOfProject = $this->Project_model->countAllProjectByClientId($client['id_client']);
        $data['project'] = $this->Project_model->getProjectByClientId($client['id_client'], 5);
        $data['countProject'] = $sumOfProject;

        $sumOfInvoice = $this->Payment_model->countInvoiceByClient($client['id_client']);
        $dataInvoice = $this->Payment_model->getInvoiceByClientId($client['id_client'], 3);
        for ($i = 0; $i < count($dataInvoice); $i++) {
            $projectId = $dataInvoice[$i]['id_order'];

            $packet = $this->Project_model->getPacketByProjectId($projectId);
            $dataInvoice[$i]['harga_paket'] = $packet['harga'];
            $dataInvoice[$i]['nama_paket'] = $packet['nama'];

            $invoiceByProject = $this->Payment_model->getInvoiceByProjectId($projectId, $dataInvoice[$i]['term']);
            $dataInvoice[$i]['detail_invoice'] = $invoiceByProject;

            $detailProject = $this->Project_model->getDetailProject($projectId);
            $dataInvoice[$i]['judul'] = $detailProject['judul'];

            $dataInvoice[$i]['tgl_keluar'] = dateFormat($dataInvoice[$i]['tgl_keluar'], "d/m/Y");
        }
        $data['invoices'] = $dataInvoice;
        $data['countInvoice'] = $sumOfInvoice;

        $this->load->view('client/template/header', $dataHeader);
        $this->load->view('client/dashboard/index', $data);
        $this->load->view('client/template/footer');
        $this->session->unset_userdata('upload_payment');
    }

    public function edit_profile()
    {
        $this->form_validation->set_rules('namaPemilikInstansi', 'Nama Pemilik', 'required|trim');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required|trim|max_length[250]');
        $this->form_validation->set_rules('instagram', 'Instagram', 'required|trim');
        $this->form_validation->set_rules('whatsapp', 'Whatsapp', 'required|trim');

        $email = getClientEmailSession();

        if ($this->form_validation->run()) {
            $this->_updateProfile($email);
        } else {
            $dataHeader = $this->setHeaderData("Edit Profil");

            $data['client'] = $this->Client_model->getClientByEmail($email);

            $this->load->view('client/template/header', $dataHeader);
            $this->load->view('client/dashboard/edit_profile', $data);
            $this->load->view('client/template/footer');
            $this->session->unset_userdata('lengkapi_profile');
        }
    }

    private function _updateProfile($email)
    {
        $nama_pemilik = $this->input->post('namaPemilikInstansi');
        $alamat = $this->input->post('alamat');
        $instagram = $this->input->post('instagram');
        $whatsapp = $this->input->post('whatsapp');

        $clientId = $this->Client_model->getIdClientByEmail($email);
        $this->Client_model->updateClientById($clientId['id_client'], $nama_pemilik, $alamat, $instagram, $whatsapp);

        redirect('/client/dashboard');
    }

    public function edit_password()
    {
        $this->form_validation->set_rules(
            'passwordLama',
            'Kata Sandi Lama',
            'required|trim|min_length[8]|max_length[32]|differs[passwordBaru]'
        );
        $this->form_validation->set_rules(
            'passwordBaru',
            'Kata Sandi Baru',
            'required|trim|min_length[8]|max_length[32]'
        );
        $this->form_validation->set_rules(
            'konfirmasiPasswordBaru',
            'Konfirmasi Kata Sandi Baru',
            'required|trim|min_length[8]|max_length[32]|matches[passwordBaru]'
        );

        $email = getClientEmailSession();

        if ($this->form_validation->run()) {
            $this->_updatePassword($email);
        } else {
            $dataHeader = $this->setHeaderData("Edit Kata Sandi");

            $this->load->view('client/template/header', $dataHeader);
            $this->load->view('client/dashboard/edit_password');
            $this->load->view('client/template/footer');
            $this->session->unset_userdata('message1');
        }
    }

    private function _updatePassword($email)
    {
        $passwordLama = $this->input->post('passwordLama');
        $passwordBaru = $this->input->post('passwordBaru');

        $client = $this->Client_model->getClientByEmail($email);

        if (password_verify($passwordLama, $client['password'])) {
            $clientId = $this->Client_model->getIdClientByEmail($email);

            $passwordBaru = password_hash($passwordBaru, PASSWORD_DEFAULT);

            $this->Client_model->updatePassword($clientId['id_client'], $passwordBaru);
            redirect('/client/dashboard');
        } else {
            $this->session->set_flashdata('message1', '<small class="text-danger">Kata Sandi Lama Salah</small>');
            redirect('/client/dashboard/edit-password');
        }
    }

    private function setHeaderData($title)
    {
        $konsultasi = "Halo polaris, \n\nSaya ingin konsultasi";
        $data['konsultasi'] = rawurlencode($konsultasi);
        $data['header_title'] = $title;
        $data['header_dashboard'] = 'navbar-hover-active';
        $data['header_project'] = 'navbar-hover';
        $data['header_payment'] = 'navbar-hover';
        return $data;
    }
}
