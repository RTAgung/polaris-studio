<?php

class Admin_auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Admin_model');
    }

    public function redirect()
    {
        redirect('/po-admin/login');
    }

    public function index()
    {
        if (logged_in_admin()) {
            redirect('/po-admin/dashboard');
        } else {
            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
            $this->form_validation->set_rules(
                'password',
                'Password',
                'required|trim|min_length[8]|max_length[32]'
            );

            if ($this->form_validation->run()) {
                $this->_login();
            } else {
                $this->load->view('admin/auth/login');
                $this->session->unset_userdata('msg_email');
                $this->session->unset_userdata('msg_password');
            }
        }
    }

    private function _login()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        $admin = $this->Admin_model->getAdminByEmail($email);

        if ($admin) {
            if (password_verify($password, $admin['password'])) {
                $key = $this->encryption->create_key(16);

                $dataSession = [
                    'auth_adme' => encryptEmail($email, $key),
                    'auth_admk' => $key,
                    'auth_adms' => 1
                ];

                $this->session->set_userdata($dataSession);
                redirect('/po-admin/dashboard');
            } else {
                $this->session->set_flashdata('msg_password', '<small class="text-danger">Password Salah</small>');
                redirect('/po-admin/login');
            }
        } else {
            $this->session->set_flashdata('msg_email', '<small class="text-danger">Email Belum Terdaftar</small>');
            redirect('/po-admin/login');
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('auth_adme');
        $this->session->unset_userdata('auth_admk');
        $this->session->unset_userdata('auth_adms');
        redirect('/po-admin/login');
    }
}
