<?php
class Main extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Main_model');
		$this->load->model('Project_model');
		$this->load->model('Client_model');
		$this->load->model('Article_model');
		$this->load->model('Packet_model');
		$this->load->helper('text');
	}

	public function index()
	{
		$data = $this->setHeaderData("Home");
		$this->load->view('main/template/header', $data);
		$this->load->view('main/home');
		$this->load->view('main/template/footer');
	}

	public function redirect()
	{
		redirect('/main');
	}

	public function web()
	{
		$data = $this->setHeaderData("Buat Web");

		$data['header_web'] = "navbar-hover-active";

		$jenis = "web";
		$data['packet'] = $this->Packet_model->getPacketByJenis($jenis);

		$this->load->view('main/template/header', $data);
		$this->load->view('main/web');
		$this->load->view('main/template/footer');
	}

	public function video()
	{
		$data = $this->setHeaderData("Buat Video");
		$data['header_video'] = "navbar-hover-active";

		$jenis = "video";
		$data['packet'] = $this->Packet_model->getPacketByJenis($jenis);

		$this->load->view('main/template/header', $data);
		$this->load->view('main/video');
		$this->load->view('main/template/footer');
	}

	public function kontak()
	{
		$data = $this->setHeaderData("Kontak Kami");
		$this->load->view('main/template/header', $data);
		$this->load->view('main/kontak');
		$this->load->view('main/template/footer');
	}

	public function tentang(){
		$data = $this->setHeaderData("Tentang Kami");
		$data['header_tentang'] = "navbar-hover-active";
		$this->load->view('main/template/header', $data);
		$this->load->view('main/tentang');
		$this->load->view('main/template/footer');
	}

	public function form_web()
	{
		$data = $this->setHeaderData("Form Web");
		$data['header_web'] = "navbar-hover-active";
		$jenis = "web";
		$data['packet'] = $this->Packet_model->getPacketByJenis($jenis);

		$this->load->view('main/template/header', $data);

		$this->form_validation->set_rules('nama', 'Nama Usaha', 'required|trim');
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
		$this->form_validation->set_rules('whatsapp', 'Whatsapp', 'required|trim');
		$this->form_validation->set_rules('instagram', 'Instagram', 'required|trim');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi Singkat', 'required|trim');
		$this->form_validation->set_rules('privacyPolicy', 'Privacy Policy', 'required|trim');

		if ($this->form_validation->run()) {
			$nama = $this->input->post('nama');
			$email = $this->input->post('email');
			$whatsapp = $this->input->post('whatsapp');
			$instagram = $this->input->post('instagram');
			$deskripsi = $this->input->post('deskripsi');
			$paket = $this->input->post('paket');
			$client_data = 0;

			if($this->Client_model->checkEmailExist($email)){
				$client_data = $this->Client_model->getClientByEmailNoJoin($email);
			} else {
				$client_data = $this->Client_model->insertClient($nama, $email, $instagram, $whatsapp);
			}

			$this->Project_model->insertOrder($deskripsi, $client_data['id_client'], $paket);

			$kode_paket = $this->Packet_model->getPacketById($paket);
			$nama_paket = $kode_paket['nama'];
			$kode = $kode_paket['kode'];

			$data['form_filled'] = 1;
			$konfirmasi = "Halo polaris, \nSaya ingin konfirmasi pesanan a.n $nama dengan paket $nama_paket, kode $kode\nmohon untuk segera diproses\n\nTerima kasih";
			$data['konfirmasi'] = rawurlencode($konfirmasi);
			$this->load->view('main/form_web', $data);
		} else {
			$this->load->view('main/form_web');
		}

		$this->load->view('main/template/footer');
	}

	public function form_video()
	{
		$data = $this->setHeaderData("Form Video");
		$data['header_video'] = "navbar-hover-active";
		$jenis = "video";
		$data['packet'] = $this->Packet_model->getPacketByJenis($jenis);

		$this->load->view('main/template/header', $data);

		$this->form_validation->set_rules('nama', 'Nama Usaha', 'required|trim');
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
		$this->form_validation->set_rules('whatsapp', 'Whatsapp', 'required|trim');
		$this->form_validation->set_rules('instagram', 'Instagram', 'required|trim');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi Singkat', 'required|trim');
		$this->form_validation->set_rules('privacyPolicy', 'Privacy Policy', 'required|trim');

		if ($this->form_validation->run()) {
			$nama = $this->input->post('nama');
			$email = $this->input->post('email');
			$whatsapp = $this->input->post('whatsapp');
			$instagram = $this->input->post('instagram');
			$deskripsi = $this->input->post('deskripsi');
			$paket = $this->input->post('paket');
			$client_data = 0;

			if($this->Client_model->checkEmailExist($email)){
				$client_data = $this->Client_model->getClientByEmailNoJoin($email);
			} else {
				$client_data = $this->Client_model->insertClient($nama, $email, $instagram, $whatsapp);
			}

			$this->Project_model->insertOrder($deskripsi, $client_data['id_client'], $paket);

			$kode_paket = $this->Packet_model->getPacketById($paket);
			$nama_paket = $kode_paket['nama'];
			$kode = $kode_paket['kode'];

			$data['form_filled'] = 1;
			$konfirmasi = "Halo polaris, \nSaya ingin konfirmasi pesanan a.n $nama dengan paket $nama_paket, kode $kode\nmohon untuk segera diproses\n\nTerima kasih";
			$data['konfirmasi'] = rawurlencode($konfirmasi);
			$this->load->view('main/form_web', $data);
		} else {
			$this->load->view('main/form_video');
		}

		$this->load->view('main/template/footer');
	}

	public function artikel()
	{
		$data = $this->setHeaderData("Artikel");
		$data['header_artikel'] = "navbar-hover-active";
		$this->load->view('main/template/header', $data);

		$article = $this->Article_model->getArticle();

		foreach ($article as $key => $value) {
			if($key == 0){
				$article[$key]['teks'] = word_limiter($value['teks'], 75);
			} else {
				$article[$key]['teks'] = word_limiter($value['teks'], 20);
			}
		}

		$data['article'] = $article;
		$this->load->view('main/artikel', $data);



		$this->load->view('main/template/footer');
	}

	public function artikel_detail($id)
	{
		$data = $this->setHeaderData("Artikel");
		$data['header_artikel'] = "navbar-hover-active";
		if(!$this->Article_model->checkDetailExsist($id)){
			redirect('main/artikel');
		}
		$data['detail_artikel'] = $this->Article_model->getDetailArticle($id);
		$data['tags'] = $this->Article_model->getTags($id);
		$data['articleByTag'] = 0;
		
		if($data['tags'] != null){
			$tags = $data['tags'];
			$random_keys=array_rand($tags,1);
			$artikel_tag = $this->Article_model->getIdArticleByTag($tags[$random_keys]['id_tag'], $id);

			if($artikel_tag !=null){
				$id_article = array_column($artikel_tag, 'id_article'); 
				$data['articleByTag'] = $this->Article_model->getArticleByTag($id_article);
			}
		}

		$this->load->view('main/template/header', $data);
		$this->load->view('main/artikel_detail', $data);
		$this->load->view('main/template/footer');
	}

	public function video_detail($id)
	{
		$data = $this->setHeaderData("Buat Video");
		$data['header_artikel'] = "navbar-hover-active";
		$data['id'] = $this->Main_model->getVideoDummy($id);

		$this->load->view('main/template/header', $data);
		$this->load->view('main/video_detail', $data);
		$this->load->view('main/template/footer');
	}

	public function artikel_tag($id_tag){
		$article = $this->Article_model->getAllArticleByTag($id_tag);
		$data = $this->setHeaderData("Artikel #" . $id_tag);
		$data['header_artikel'] = "navbar-hover-active";

		$tag = $this->Article_model->getTagById($id_tag);

		foreach ($article as $key => $value) {
            $article[$key]['teks'] = word_limiter($value['teks'], 40);
        }
		$data['article'] = $article;
		$data['tag'] = $tag;

		$this->load->view('main/template/header', $data);
		$this->load->view('main/artikel_tag', $data);
		$this->load->view('main/template/footer');
	}

	private function setHeaderData($title)
	{
		$konsultasi = "Halo polaris, \n\nSaya ingin konsultasi";
		$data['konsultasi'] = rawurlencode($konsultasi);
		$data['header_title'] = $title;
		$data['header_tentang'] = "navbar-hover";
		$data['header_web'] = "navbar-hover";
		$data['header_video'] = "navbar-hover";
		$data['header_artikel'] = "navbar-hover";
		$data['header_login'] = "navbar-hover";
		return $data;
	}

}
