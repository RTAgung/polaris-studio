<?php

class Client_auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Client_model');
    }

    public function redirect()
    {
        redirect('/client/login');
    }

    public function index()
    {
        if (logged_in()) {
            redirect('/client/dashboard');
        } else {
            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
            $this->form_validation->set_rules(
                'password',
                'Kata Sandi',
                'required|trim|min_length[8]|max_length[32]'
            );

            if ($this->form_validation->run()) {
                $this->_login();
            } else {
                $hubungi_lupa_password = "Halo polaris, \n\nSaya melupakan akun password saya \nnama: \nemail: \n\nmohon bantuannya terima kasih";
                $konsultasi = "Halo polaris, \n\nSaya ingin konsultasi";

                $dataHeader['konsultasi'] = rawurlencode($konsultasi);
                $dataHeader = $this->setHeaderData("Masuk");
                $dataContent['hubungi_lupa_password'] = rawurlencode($hubungi_lupa_password);
                $dataHeader['header_login'] = "navbar-hover-active";

                $this->load->view('main/template/header', $dataHeader);
                $this->load->view('client/auth/login', $dataContent);
                $this->load->view('main/template/footer');
                $this->session->unset_userdata('message1');
                $this->session->unset_userdata('message2');
            }
        }
    }

    private function _login()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        $client = $this->Client_model->getClientByEmail($email);

        if ($client) {
            if ($client['is_registered']) {
                if (password_verify($password, $client['password'])) {
                    $key = $this->encryption->create_key(16);

                    $dataSession = [
                        'auth_e' => encryptEmail($email, $key),
                        'auth_k' => $key
                    ];
                    $this->session->set_userdata($dataSession);
                    redirect('/client/dashboard');
                } else {
                    $this->session->set_flashdata('message2', '<small class="text-danger">Kata Sandi Salah</small>');
                    redirect('/client/login');
                }
            } else {
                $this->session->set_flashdata('message1', '<small class="text-danger">Email Belum Diaktivasi</small>');
                redirect('/client/login');
            }
        } else {
            $this->session->set_flashdata('message1', '<small class="text-danger">Email Belum Terdaftar</small>');
            redirect('/client/login');
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('auth_e');
        $this->session->unset_userdata('auth_k');
        redirect('/client/login');
    }

    private function setHeaderData($title)
    {
        $konsultasi = "Halo polaris, \n\nSaya ingin konsultasi";
        $data['konsultasi'] = rawurlencode($konsultasi);
        $data['header_title'] = $title;
        $data['header_tentang'] = "navbar-hover";
        $data['header_web'] = "navbar-hover";
        $data['header_video'] = "navbar-hover";
        $data['header_artikel'] = "navbar-hover";
        $data['header_login'] = "navbar-hover";
        return $data;
    }
}
