<?php

class Admin_article extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!logged_in_admin())
            redirect('/po-admin/login');
        $this->load->model('Article_model');
        $this->load->model('Admin_model');
        $this->load->helper('text');
    }

    public function index()
    {
        $dataHeader = $this->setHeaderData("Article");

        $article = $this->Article_model->getArticle();

        foreach ($article as $key => $value) {
            $article[$key]['teks'] = word_limiter($value['teks'], 40);
        }
        $data['article'] = $article;

        $this->load->view('admin/template/header', $dataHeader);
        $this->load->view('admin/article/index', $data);
        $this->load->view('admin/template/footer');
    }

    public function detail($id)
    {
        $dataHeader = $this->setHeaderData("Article Detail");

        $data['detail_artikel'] = $this->Article_model->getDetailArticle($id);
        $data['tags'] = $this->Article_model->getTags($id);
        $data['articleByTag'] = 0;

        if ($data['tags'] != null) {
            $tags = $data['tags'];
            $random_keys = array_rand($tags, 1);
            $artikel_tag = $this->Article_model->getIdArticleByTag($tags[$random_keys]['id_tag'], $id);
        }

        $this->load->view('admin/template/header', $dataHeader);
        $this->load->view('admin/article/detail', $data);
        $this->load->view('admin/template/footer');
    }

    public function new()
    {
        $dataHeader = $this->setHeaderData("New Article");

        $data['tags'] = $this->Article_model->getAllTags();

        $this->load->view('admin/template/header', $dataHeader);


        $this->form_validation->set_rules('judul', 'Judul', 'required|trim');
        $this->form_validation->set_rules('teks', 'Teks', 'required|trim');

        if ($this->form_validation->run()) {
            if (isset($_POST['submit'])) {
                $judul = $this->input->post('judul');
                $teks = $this->input->post('teks');
                $tgl_buat = date("y-m-d");
                $tgl_edit = $tgl_buat;
                $email = getAdminEmailSession();
                $id = $this->Admin_model->getAdminByEmail($email);

                $date = new DateTime();

                $config['upload_path'] = './asset/file/article/';
                $config['file_name'] = "article_" . $date->getTimestamp();
                $config['file_ext_tolower'] = TRUE;
                $config['overwrite'] = TRUE;
                $config['allowed_types'] = 'jpeg|jpg|png';
                $config['max_size'] = 500;

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('gambar')) {
                    $error = array('error' => $this->upload->display_errors());
                    $this->session->set_flashdata('upload_article', "<p class='text-red text-center m-0'>error :</p><small class='text-red text-center'>" . $error['error'] . "</small>");
                    $this->load->view('admin/article/new', $data);
                } else {
                    $data = $this->upload->data();
                    $filename = $data['file_name'];
                    $dataArticle = [
                        "judul" => $judul,
                        "teks" => $teks,
                        "gambar" => $filename,
                        "tgl_buat" => $tgl_buat,
                        "tgl_edit" => $tgl_edit,
                        "id_admin" => $id['id_admin']
                    ];

                    $this->Article_model->uploadArticle($dataArticle);
                    $this->session->set_flashdata('upload_article', '<p class="text-yellow text-center m-0">Article berhasil diunggah</p>');

                    $lastArticle = $this->Article_model->getLastArticle();
                    $tag = array($this->input->post('tag1'), $this->input->post('tag2'), $this->input->post('tag3'), $this->input->post('tag4'));
                    var_dump($tag);

                    foreach ($tag as $index => $value) {
                        if ($value != "0") {
                            $dataArticleTag = [
                                "id_article" => $lastArticle['id_article'],
                                "id_tag" => $value
                            ];
                            $this->Article_model->insertArticleTag($dataArticleTag);
                        }
                    }
                    redirect('po-admin/article');
                }

                $data['form_filled'] = 1;
            }
        } else {
            $this->load->view('admin/article/new', $data);
        }

        $this->load->view('admin/template/footer');
    }

    public function edit($article_id)
    {
        $dataHeader = $this->setHeaderData("Edit Article");

        $this->load->view('admin/template/header', $dataHeader);

        $this->form_validation->set_rules('judul', 'Judul', 'required|trim');
        $this->form_validation->set_rules('teks', 'Teks', 'required|trim');

        $data['article'] = $this->Article_model->getDetailArticle($article_id);
        $data['tags'] = $this->Article_model->getAllTags();
        $data['selected_tags'] = $this->Article_model->getTags($article_id);

        if ($this->form_validation->run()) {
            if (isset($_POST['submit'])) {
                $judul = $this->input->post('judul');
                $teks = $this->input->post('teks');
                $tgl_buat = $data['article']['tgl_buat'];
                $tgl_edit = date("y-m-d");
                $email = getAdminEmailSession();
                $id = $this->Admin_model->getAdminByEmail($email);
                $filename = $data['article']['gambar'];

                $config['upload_path'] = './asset/file/article/';
                $config['file_name'] = $filename;
                $config['file_ext_tolower'] = TRUE;
                $config['overwrite'] = TRUE;
                $config['allowed_types'] = 'jpeg|jpg|png';
                $config['max_size'] = 500;

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('gambar')) {
                    // $error = array('error' => $this->upload->display_errors());
                    // $this->session->set_flashdata('upload_article', "<p class='text-red text-center m-0'>error :</p><small class='text-red text-center'>" . $error['error'] . "</small>");
                    //$this->load->view('admin/article/edit', $data);

                    $data = $this->upload->data();
                    $filename = $data['file_name'];
                    $dataArticle = [
                        "judul" => $judul,
                        "teks" => $teks,
                        "tgl_buat" => $tgl_buat,
                        "tgl_edit" => $tgl_edit,
                        "id_admin" => $id['id_admin']
                    ];
                } else {
                    $data = $this->upload->data();
                    $filename = $data['file_name'];
                    $dataArticle = [
                        "judul" => $judul,
                        "teks" => $teks,
                        "gambar" => $filename,
                        "tgl_buat" => $tgl_buat,
                        "tgl_edit" => $tgl_edit,
                        "id_admin" => $id['id_admin']
                    ];
                }

                $this->Article_model->updateArticle($article_id, $dataArticle);
                $this->session->set_flashdata('upload_article', '<p class="text-yellow text-center m-0">Article berhasil diunggah</p>');

                $tag = array($this->input->post('tag1'), $this->input->post('tag2'), $this->input->post('tag3'), $this->input->post('tag4'));

                $this->Article_model->deleteArticleTags($article_id);

                foreach ($tag as $index => $value) {
                    if ($value != "0") {
                        $dataArticleTag = [
                            "id_article" => $article_id,
                            "id_tag" => $value
                        ];
                        $this->Article_model->insertArticleTag($dataArticleTag);
                    }
                }
                redirect('po-admin/article');

                $data['form_filled'] = 1;
            }
        } else {
            $this->load->view('admin/article/edit', $data);
        }

        $this->load->view('admin/template/footer');
    }

    public function delete($id)
    {
        $this->Article_model->deleteArticle($id);
        redirect('/po-admin/article');
    }

    public function tag()
    {
        $dataHeader = $this->setHeaderData("Article Tag");

        $data['tags'] = $this->Article_model->getAllTags();

        $this->load->view('admin/template/header', $dataHeader);
        $this->load->view('admin/article/tag', $data);
        $this->load->view('admin/template/footer');
    }

    public function tag_new()
    {
        $this->form_validation->set_rules('tag', 'Tag', 'required|trim');

        if ($this->form_validation->run()) {
            if (isset($_POST['submit'])) {
                $tag = $this->input->post('tag');

                if ($this->Article_model->checkTagExist($tag)) {
                } else {
                    $this->Article_model->insertTag($tag);
                }
            }
        }
        redirect('/po-admin/article/tag');
    }

    public function tag_edit($id)
    {
        $this->form_validation->set_rules('namaTag', 'Nama Tag', 'required|trim');

        if ($this->form_validation->run()) {
            if (isset($_POST['submitTag'])) {
                $tag = $this->input->post('namaTag');

                $this->Article_model->updateTag($id, $tag);
            }
        }
        redirect('/po-admin/article/tag');
    }

    public function tag_delete($id)
    {
        $this->Article_model->deleteTag($id);
        redirect('/po-admin/article/tag');
    }

    private function setHeaderData($title)
    {
        $email = getAdminEmailSession();
        $admin = $this->Admin_model->getAdminByEmail($email);

        $data['header_title'] = $title;
        $data['header_admin'] = $admin['nama'];
        // fill '' or 'menu-open'
        $data['nav_menu_open'] = '';
        // fill '' or 'my-bg-black'
        $data['nav_dashboard'] = '';
        $data['nav_client'] = '';
        $data['nav_video'] = '';
        $data['nav_web'] = '';
        $data['nav_payment'] = '';
        $data['nav_packet'] = '';
        $data['nav_article'] = 'my-bg-black';
        $data['nav_admin'] = '';
        return $data;
    }
}
