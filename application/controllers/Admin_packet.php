<?php

class Admin_packet extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if (!logged_in_admin())
            redirect('/po-admin/login');
        $this->load->model('Admin_model');
        $this->load->model('Packet_model');
        $this->load->helper('text');
    }

    public function index()
	{
        $dataHeader = $this->setHeaderData("Packet");

        $data['packet'] = $this->Packet_model->getAllPacket();

		$this->load->view('admin/template/header', $dataHeader);
		$this->load->view('admin/packet/index', $data);
		$this->load->view('admin/template/footer');
	}

    public function new(){
        $this->form_validation->set_rules('kode', 'Kode', 'required|trim');
        $this->form_validation->set_rules('nama', 'Nama', 'required|trim|min_length[5]|max_length[17]');
        $this->form_validation->set_rules('jenis', 'Jenis', 'required|trim');
        $this->form_validation->set_rules('harga_awal', 'Harga Awal', 'trim');
        $this->form_validation->set_rules('harga', 'Harga', 'required|trim');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required|trim');
        $this->form_validation->set_rules('detail', 'Detail', 'required|trim');

        if ($this->form_validation->run()) {
            if (isset($_POST['submit'])) {
                $kode = $this->input->post('kode');
                $nama = $this->input->post('nama');
                $jenis = $this->input->post('jenis');
                $harga_awal = $this->input->post('harga_awal');
                $harga = $this->input->post('harga');
                $deskripsi = $this->input->post('deskripsi');
                $detail = $this->input->post('detail');
                
                $this->Packet_model->addPacket($kode, $nama, $jenis, $harga_awal, $harga, $deskripsi, $detail);
            }
        }
        redirect('/po-admin/packet');
    }

    public function edit($id){
        $this->form_validation->set_rules('kode', 'Kode', 'required|trim');
        $this->form_validation->set_rules('nama', 'Nama', 'required|trim|min_length[5]|max_length[17]');
        $this->form_validation->set_rules('jenis', 'Jenis', 'required|trim');
        $this->form_validation->set_rules('harga_awal', 'Harga Awal', 'trim');
        $this->form_validation->set_rules('harga', 'Harga', 'required|trim');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required|trim');
        $this->form_validation->set_rules('detail', 'Detail', 'required|trim');

        if ($this->form_validation->run()) {
            if (isset($_POST['submit'])) {
                $kode = $this->input->post('kode');
                $nama = $this->input->post('nama');
                $jenis = $this->input->post('jenis');
                $harga_awal = $this->input->post('harga_awal');
                $harga = $this->input->post('harga');
                $deskripsi = $this->input->post('deskripsi');
                $detail = $this->input->post('detail');
                
                $this->Packet_model->updatePacket($id, $kode, $nama, $jenis, $harga_awal, $harga, $deskripsi, $detail);
            }
        }
        redirect('/po-admin/packet');
    }

    public function delete($id){
        $this->Packet_model->deletePacket($id);
        redirect('/po-admin/packet');
    }

    private function setHeaderData($title)
    {
        $email = getAdminEmailSession();
        $admin = $this->Admin_model->getAdminByEmail($email);

        $data['header_title'] = $title;
        $data['header_admin'] = $admin['nama'];
        // fill '' or 'menu-open'
        $data['nav_menu_open'] = '';
        // fill '' or 'my-bg-black'
        $data['nav_dashboard'] = '';
        $data['nav_client'] = '';
        $data['nav_video'] = '';
        $data['nav_web'] = '';
        $data['nav_payment'] = '';
        $data['nav_packet'] = 'my-bg-black';
        $data['nav_article'] = '';
        $data['nav_admin'] = '';
        return $data;
    }
}