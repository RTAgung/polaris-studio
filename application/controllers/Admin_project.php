<?php

class Admin_project extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!logged_in_admin())
            redirect('/po-admin/login');
        $this->load->model('Admin_model');
        $this->load->model('Project_model');
        $this->load->model('Payment_model');
    }

    public function web()
    {
        $countCurrent = $this->Project_model->countAllProject('web', 'current');
        if (isset($_GET['c_m']))
            $currentProject = $this->Project_model->getAllProject('web', 'current', $countCurrent);
        else
            $currentProject = $this->Project_model->getAllProject('web', 'current');

        $countHistory = $this->Project_model->countAllProject('web', 'history');
        if (isset($_GET['h_m']))
            $historyProject = $this->Project_model->getAllProject('web', 'history', $countHistory);
        else
            $historyProject = $this->Project_model->getAllProject('web', 'history');

        $data['currentProject'] = $currentProject;
        $data['countCurrent'] = $countCurrent;
        $data['historyProject'] = $historyProject;
        $data['countHistory'] = $countHistory;

        $dataHeader = $this->setHeaderDataWeb("Web");

        $this->load->view('admin/template/header', $dataHeader);
        $this->load->view('admin/project/web', $data);
        $this->load->view('admin/template/footer');
    }

    public function web_detail($id_project)
    {
        if ($this->Project_model->checkProjectExist($id_project, "web")) {
            $this->form_validation->set_rules("komentar", "Komentar", "required|trim");
            $this->form_validation->set_rules("label", "Label", "trim");

            if ($this->form_validation->run()) {
                $label = $this->input->post('label');
                $komentar = $this->input->post('komentar');

                $this->Project_model->insertChat($komentar, $id_project, 'admin', $label);
                redirect("/po-admin/project/web/$id_project");
            } else {
                date_default_timezone_set('Asia/Jakarta');

                $project = $this->Project_model->getFullProject($id_project);
                $project['tgl_mulai'] = dateFormat($project['tgl_mulai']);
                $client = $this->Project_model->getProjectAndClient($id_project);
                $client['whatsapp'] = konversiNomorHP($client['whatsapp']);
                $web = $this->Project_model->getDataWeb($id_project);
                $chat = $this->Project_model->getChat($id_project);
                $invoice = $this->Payment_model->getInvoiceByProjectId($id_project);

                $data['project'] = $project;
                $data['client'] = $client;
                $data['web'] = $web;
                $data['chat'] = $chat;
                $data['invoice'] = $invoice;
                $dataHeader = $this->setHeaderDataWeb("Detail Web");

                $this->load->view('admin/template/header', $dataHeader);
                $this->load->view('admin/project/web_detail', $data);
                $this->load->view('admin/template/footer');
                $this->session->unset_userdata('upload_invoice');
            }
        } else
            show_404();
    }

    public function web_edit($id_project)
    {
        if ($this->Project_model->checkProjectExist($id_project, 'web')) {
            $this->form_validation->set_rules('judulProyek', 'Judul Proyek', 'required|trim');
            $this->form_validation->set_rules('jenisUsahaInstansi', 'Jenis Usaha Instansi', 'required|trim');
            $this->form_validation->set_rules('asset', 'Asset', 'required|trim|max_length[255]');
            $this->form_validation->set_rules('catatan', 'Catatan', 'trim|max_length[500]');

            if ($this->form_validation->run()) {
                $dataInput = $this->getDataFormWeb();

                $this->Project_model->updateDataFormWeb($dataInput, $id_project);
                redirect("/po-admin/project/web/$id_project");
            } else {
                $project = $this->Project_model->getProjectAndClient($id_project);
                $detail = $this->Project_model->getDetailProject($id_project);
                $web = $this->Project_model->getDataWeb($id_project);

                $data = [
                    'project' => $project,
                    'detail' => $detail,
                    'web' => $web
                ];
                $dataHeader = $this->setHeaderDataWeb("Edit Web");

                $this->load->view('admin/template/header', $dataHeader);
                $this->load->view('admin/project/web_edit', $data);
                $this->load->view('admin/template/footer');
            }
        } else
            show_404();
    }

    private function getDataFormWeb()
    {
        $dataInputForm = array(
            "judul" => $this->input->post('judulProyek'),
            "jenisUsaha" => $this->input->post('jenisUsahaInstansi'),
            "tujuan" => array(
                0 => $this->input->post('tujuanProfile'),
                1 => $this->input->post('tujuanTokoOnline'),
                2 => $this->input->post('tujuanUpdateInfo'),
                3 => $this->input->post('tujuanOther'),
                4 => $this->input->post('tujuanTextOther')
            ),
            "menu" => array(
                0 => $this->input->post('menuAbout'),
                1 => $this->input->post('menuProduct'),
                2 => $this->input->post('menuContact'),
                3 => $this->input->post('menuShop'),
                4 => $this->input->post('menuOther'),
                5 => $this->input->post('menuTextOther'),
            ),
            "domain" => $this->input->post('domain'),
            "hosting" => $this->input->post('hosting'),
            "asset" => $this->input->post('asset'),
            "catatan" => $this->input->post('catatan')
        );

        $tujuan = "";
        $state = true;
        for ($i = 0; $i < 4; $i++) {
            if (!empty($dataInputForm['tujuan'][$i])) {
                if ($i == 3)
                    $i++;
                if ($state) {
                    $tujuan = $tujuan . $dataInputForm['tujuan'][$i];
                    $state = false;
                } else
                    $tujuan = $tujuan . ", " . $dataInputForm['tujuan'][$i];
            }
        }

        $menu = "";
        $state = true;
        for ($i = 0; $i < 5; $i++) {
            if (!empty($dataInputForm['menu'][$i])) {
                if ($i == 4)
                    $i++;
                if ($state) {
                    $menu = $menu . $dataInputForm['menu'][$i];
                    $state = false;
                } else
                    $menu = $menu . ", " . $dataInputForm['menu'][$i];
            }
        }

        $dataInput = array(
            "judul" => $dataInputForm['judul'],
            "jenisUsaha" => $dataInputForm['jenisUsaha'],
            "tujuan" => $tujuan,
            "menu" => $menu,
            "domain" => $dataInputForm['domain'],
            "hosting" => $dataInputForm['hosting'],
            "asset" => $dataInputForm['asset'],
            "catatan" => $dataInputForm['catatan']
        );

        return $dataInput;
    }

    public function video()
    {
        $countCurrent = $this->Project_model->countAllProject('video', 'current');
        if (isset($_GET['c_m']))
            $currentProject = $this->Project_model->getAllProject('video', 'current', $countCurrent);
        else
            $currentProject = $this->Project_model->getAllProject('video', 'current');

        $countHistory = $this->Project_model->countAllProject('video', 'history');
        if (isset($_GET['h_m']))
            $historyProject = $this->Project_model->getAllProject('video', 'history', $countHistory);
        else
            $historyProject = $this->Project_model->getAllProject('video', 'history');

        $data['currentProject'] = $currentProject;
        $data['countCurrent'] = $countCurrent;
        $data['historyProject'] = $historyProject;
        $data['countHistory'] = $countHistory;
        $dataHeader = $this->setHeaderDataVideo("Video");

        $this->load->view('admin/template/header', $dataHeader);
        $this->load->view('admin/project/video', $data);
        $this->load->view('admin/template/footer');
    }

    public function video_detail($id_project)
    {
        if ($this->Project_model->checkProjectExist($id_project, 'video')) {
            $this->form_validation->set_rules("komentar", "Komentar", "required|trim");
            $this->form_validation->set_rules("label", "Label", "trim");

            if ($this->form_validation->run()) {
                $label = $this->input->post('label');
                $komentar = $this->input->post('komentar');

                $this->Project_model->insertChat($komentar, $id_project, 'admin', $label);
                redirect("/po-admin/project/video/$id_project");
            } else {
                date_default_timezone_set('Asia/Jakarta');

                $project = $this->Project_model->getFullProject($id_project);
                $project['tgl_mulai'] = dateFormat($project['tgl_mulai']);
                $client = $this->Project_model->getProjectAndClient($id_project);
                $client['whatsapp'] = konversiNomorHP($client['whatsapp']);
                $video = $this->Project_model->getDataVideo($id_project);
                $chat = $this->Project_model->getChat($id_project);
                $invoice = $this->Payment_model->getInvoiceByProjectId($id_project);

                $data['project'] = $project;
                $data['client'] = $client;
                $data['video'] = $video;
                $data['chat'] = $chat;
                $data['invoice'] = $invoice;
                $dataHeader = $this->setHeaderDataVideo("Detail Video");

                $this->load->view('admin/template/header', $dataHeader);
                $this->load->view('admin/project/video_detail', $data);
                $this->load->view('admin/template/footer');
                $this->session->unset_userdata('upload_invoice');
            }
        } else
            show_404();
    }

    public function video_edit($id_project)
    {
        if ($this->Project_model->checkProjectExist($id_project, 'video')) {
            $this->form_validation->set_rules('judulProyek', 'Judul Proyek', 'required|trim');
            $this->form_validation->set_rules('jenisProdukInstansi', 'Jenis Produk', 'required|trim');
            $this->form_validation->set_rules('merk', 'Merk', 'required|trim');
            $this->form_validation->set_rules('lokasi', 'Lokasi', 'required|trim');
            $this->form_validation->set_rules('tone', 'Tone', 'required|trim');
            $this->form_validation->set_rules('pesan', 'Pesan', 'required|trim|max_length[255]');
            $this->form_validation->set_rules('asset', 'Asset', 'required|trim|max_length[255]');
            $this->form_validation->set_rules('catatan', 'Catatan', 'trim|max_length[500]');

            if ($this->form_validation->run()) {
                $dataInput = $this->getDataFormVideo();

                $this->Project_model->updateDataFormVideo($dataInput, $id_project);
                redirect("/po-admin/project/video/$id_project");
            } else {
                $project = $this->Project_model->getProjectAndClient($id_project);
                $detail = $this->Project_model->getDetailProject($id_project);
                $video = $this->Project_model->getDataVideo($id_project);

                $data = [
                    'project' => $project,
                    'detail' => $detail,
                    'video' => $video
                ];
                $dataHeader = $this->setHeaderDataVideo("Edit Video");

                $this->load->view('admin/template/header', $dataHeader);
                $this->load->view('admin/project/video_edit', $data);
                $this->load->view('admin/template/footer');
            }
        } else
            show_404();
    }

    private function getDataFormVideo()
    {
        $dataInput = array(
            "judul" => $this->input->post('judulProyek'),
            "jenisProduk" => $this->input->post('jenisProdukInstansi'),
            "merk" => $this->input->post('merk'),
            "usia" => $this->input->post('usia'),
            "lokasi" => $this->input->post('lokasi'),
            "jenisKelamin" => $this->input->post('jenisKelamin'),
            "tone" => $this->input->post('tone'),
            "pesan" => $this->input->post('pesan'),
            "model" => $this->input->post('model'),
            "asset" => $this->input->post('asset'),
            "catatan" => $this->input->post('catatan')
        );
        return $dataInput;
    }

    public function set_status($from, $id_project)
    {
        if ($this->Project_model->checkProjectExist($id_project, $from)) {
            if (isset($_POST['submitFinish'])) {
                $this->Project_model->setStatusProject('finished', $id_project);
            }
            if (isset($_POST['submitCancel'])) {
                $this->Project_model->setStatusProject('canceled', $id_project);
            }
            if (isset($_POST['submitReopen'])) {
                $this->Project_model->setStatusProject('on progress', $id_project);
            }

            redirect("/po-admin/project/$from/$id_project");
        } else
            show_404();
    }

    public function delete($id_project)
    {
        if (isset($_POST['submitDelete'])) {
            $this->Project_model->deleteProject($id_project);
            if (isset($_GET['f']))
                redirect('/po-admin/project/' . $_GET['f']);
            else
                redirect('/po-admin/dashboard');
        } else {
            show_404();
        }
    }

    private function setHeaderDataVideo($title)
    {
        $email = getAdminEmailSession();
        $admin = $this->Admin_model->getAdminByEmail($email);

        $data['header_title'] = $title;
        $data['header_admin'] = $admin['nama'];
        // fill '' or 'menu-open'
        $data['nav_menu_open'] = 'menu-open';
        // fill '' or 'my-bg-black'
        $data['nav_dashboard'] = '';
        $data['nav_client'] = '';
        $data['nav_video'] = 'my-bg-black';
        $data['nav_web'] = '';
        $data['nav_payment'] = '';
        $data['nav_packet'] = '';
        $data['nav_article'] = '';
        $data['nav_admin'] = '';
        return $data;
    }

    private function setHeaderDataWeb($title)
    {
        $email = getAdminEmailSession();
        $admin = $this->Admin_model->getAdminByEmail($email);

        $data['header_title'] = $title;
        $data['header_admin'] = $admin['nama'];
        // fill '' or 'menu-open'
        $data['nav_menu_open'] = 'menu-open';
        // fill '' or 'my-bg-black'
        $data['nav_dashboard'] = '';
        $data['nav_client'] = '';
        $data['nav_video'] = '';
        $data['nav_web'] = 'my-bg-black';
        $data['nav_payment'] = '';
        $data['nav_packet'] = '';
        $data['nav_article'] = '';
        $data['nav_admin'] = '';
        return $data;
    }
}
