<?php

class Client_project extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!logged_in())
            redirect('/client/login');
        $this->load->model('Client_model');
        $this->load->model('Project_model');
    }

    public function index()
    {
        $dataHeader = $this->setHeaderData("Proyek");

        $email = getClientEmailSession();
        $client = $this->Client_model->getIdClientByEmail($email);
        $sumOfProject = $this->Project_model->countAllProjectByClientId($client['id_client']);
        if (isset($_GET['more'])) {
            $data['project'] = $this->Project_model->getProjectByClientId($client['id_client'], $sumOfProject);
        } else {
            $data['project'] = $this->Project_model->getProjectByClientId($client['id_client']);
        }
        $data['countProject'] = $sumOfProject;

        $this->load->view('client/template/header', $dataHeader);
        $this->load->view('client/project/index', $data);
        $this->load->view('client/template/footer');
    }

    public function form_order($projectId)
    {
        if (!$this->Project_model->checkProjectExist($projectId) || $this->Project_model->checkFormRequired($projectId)) {
            show_404();
        } else {
            $email = getClientEmailSession();
            $client = $this->Client_model->getClientByEmail($email);
            if (empty($client['alamat']) || empty($client['nama_pemilik'])) {
                $this->session->set_flashdata('lengkapi_profile', '<p class="my-4 text-danger fw-bold">lengkapi data terlebih dahulu sebelum melakukan transaksi</p>');
                redirect('/client/dashboard/edit-profile');
            }

            $packet = $this->Project_model->getPacketByProjectId($projectId);
            $jenisPaket = $packet['jenis'];

            if ($jenisPaket == "web") {
                $this->form_web($projectId);
            } else if ($jenisPaket == "video") {
                $this->form_video($projectId);
            }
        }
    }

    private function form_web($projectId)
    {
        $dataHeader = $this->setHeaderData("Form Permintaan Web");

        $this->form_validation->set_rules('judulProyek', 'Judul Proyek', 'required|trim');
        $this->form_validation->set_rules('jenisUsahaInstansi', 'Jenis Usaha Instansi', 'required|trim');
        $this->form_validation->set_rules('asset', 'Aset', 'required|trim|max_length[255]');
        $this->form_validation->set_rules('catatan', 'Catatan', 'trim|max_length[500]');

        if ($this->form_validation->run()) {
            $this->insert_form_web($projectId);
        } else {
            $this->load->view('client/template/header', $dataHeader);
            $this->load->view('client/project/form_paket_web');
            $this->load->view('client/template/footer');
        }
    }

    private function insert_form_web($projectId)
    {
        $dataInput = $this->getDataFormWeb();

        $this->Project_model->insertDataFormWeb($dataInput, $projectId);
        redirect('/client/project');
    }

    private function getDataFormWeb()
    {
        $dataInputForm = array(
            "judul" => $this->input->post('judulProyek'),
            "jenisUsaha" => $this->input->post('jenisUsahaInstansi'),
            "tujuan" => array(
                0 => $this->input->post('tujuanProfile'),
                1 => $this->input->post('tujuanTokoOnline'),
                2 => $this->input->post('tujuanUpdateInfo'),
                3 => $this->input->post('tujuanOther'),
                4 => $this->input->post('tujuanTextOther')
            ),
            "menu" => array(
                0 => $this->input->post('menuAbout'),
                1 => $this->input->post('menuProduct'),
                2 => $this->input->post('menuContact'),
                3 => $this->input->post('menuShop'),
                4 => $this->input->post('menuOther'),
                5 => $this->input->post('menuTextOther'),
            ),
            "domain" => $this->input->post('domain'),
            "hosting" => $this->input->post('hosting'),
            "asset" => $this->input->post('asset'),
            "catatan" => $this->input->post('catatan')
        );

        $tujuan = "";
        $state = true;
        for ($i = 0; $i < 4; $i++) {
            if (!empty($dataInputForm['tujuan'][$i])) {
                if ($i == 3)
                    $i++;
                if ($state) {
                    $tujuan = $tujuan . $dataInputForm['tujuan'][$i];
                    $state = false;
                } else
                    $tujuan = $tujuan . ", " . $dataInputForm['tujuan'][$i];
            }
        }

        $menu = "";
        $state = true;
        for ($i = 0; $i < 5; $i++) {
            if (!empty($dataInputForm['menu'][$i])) {
                if ($i == 4)
                    $i++;
                if ($state) {
                    $menu = $menu . $dataInputForm['menu'][$i];
                    $state = false;
                } else
                    $menu = $menu . ", " . $dataInputForm['menu'][$i];
            }
        }

        $dataInput = array(
            "judul" => $dataInputForm['judul'],
            "jenisUsaha" => $dataInputForm['jenisUsaha'],
            "tujuan" => $tujuan,
            "menu" => $menu,
            "domain" => $dataInputForm['domain'],
            "hosting" => $dataInputForm['hosting'],
            "asset" => $dataInputForm['asset'],
            "catatan" => $dataInputForm['catatan']
        );

        return $dataInput;
    }

    private function form_video($projectId)
    {
        $dataHeader = $this->setHeaderData("Form Permintaan Video");

        $this->form_validation->set_rules('judulProyek', 'Judul Proyek', 'required|trim');
        $this->form_validation->set_rules('jenisProdukInstansi', 'Jenis Produk', 'required|trim');
        $this->form_validation->set_rules('merk', 'Merk', 'required|trim');
        $this->form_validation->set_rules('lokasi', 'Lokasi', 'required|trim');
        $this->form_validation->set_rules('tone', 'Tone', 'required|trim');
        $this->form_validation->set_rules('pesan', 'Pesan', 'required|trim|max_length[255]');
        $this->form_validation->set_rules('asset', 'Aset', 'required|trim|max_length[255]');
        $this->form_validation->set_rules('catatan', 'Catatan', 'trim|max_length[500]');

        if ($this->form_validation->run()) {
            $this->insert_form_video($projectId);
        } else {
            $this->load->view('client/template/header', $dataHeader);
            $this->load->view('client/project/form_paket_video');
            $this->load->view('client/template/footer');
        }
    }

    private function insert_form_video($projectId)
    {
        $dataInput = $this->getDataFormVideo();

        $this->Project_model->insertDataFormVideo($dataInput, $projectId);
        redirect('/client/project');
    }

    private function getDataFormVideo()
    {
        $dataInput = array(
            "judul" => $this->input->post('judulProyek'),
            "jenisProduk" => $this->input->post('jenisProdukInstansi'),
            "merk" => $this->input->post('merk'),
            "usia" => $this->input->post('usia'),
            "lokasi" => $this->input->post('lokasi'),
            "jenisKelamin" => $this->input->post('jenisKelamin'),
            "tone" => $this->input->post('tone'),
            "pesan" => $this->input->post('pesan'),
            "model" => $this->input->post('model'),
            "asset" => $this->input->post('asset'),
            "catatan" => $this->input->post('catatan')
        );
        return $dataInput;
    }

    public function detail($projectId)
    {
        if (!$this->Project_model->checkProjectExist($projectId) || !$this->Project_model->checkFormRequired($projectId)) {
            show_404();
        } else {
            if (isset($_POST['submitChat'])) {
                $from = "chat";
                $this->form_validation->set_rules("komentar", "Komentar", "required|trim");
            } else if (isset($_POST['submitAsset'])) {
                $from = "asset";
                $this->form_validation->set_rules("asset", "Aset", "required|trim|max_length[255]");
            }

            if ($this->form_validation->run()) {
                if ($from == "chat")
                    $this->insertChat($projectId);
                else if ($from == "asset")
                    $this->updateAsset($projectId);
            } else {
                $data['detail'] = $this->Project_model->getFullProject($projectId);
                $data['detail']['tgl_mulai'] = dateFormat($data['detail']['tgl_mulai']);
                $data['chat'] = $this->Project_model->getChat($projectId);
                for ($i = 0; $i < count($data['chat']); $i++) {
                    $data['chat'][$i]['tgl_kirim'] = dateFormat($data['chat'][$i]['tgl_kirim']);
                }

                $dataHeader = $this->setHeaderData("Detail Proyek");

                $this->load->view('client/template/header', $dataHeader);
                $this->load->view('client/project/detail', $data);
                $this->load->view('client/template/footer');
            }
        }
    }

    private function insertChat($projectId)
    {
        $chat = $this->input->post('komentar');

        $this->Project_model->insertChat($chat, $projectId, "client");
        redirect("/client/project/$projectId");
    }

    private function updateAsset($projectId)
    {
        $asset = $this->input->post('asset');

        $this->Project_model->updateAsset($asset, $projectId);
        redirect("/client/project/$projectId");
    }

    private function setHeaderData($title)
    {
        $konsultasi = "Halo polaris, \n\nSaya ingin konsultasi";
        $data['konsultasi'] = rawurlencode($konsultasi);
        $data['header_title'] = $title;
        $data['header_dashboard'] = 'navbar-hover';
        $data['header_project'] = 'navbar-hover-active';
        $data['header_payment'] = 'navbar-hover';
        return $data;
    }
}
