<?php

class Admin_payment extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!logged_in_admin())
            redirect('/po-admin/login');
        $this->load->model('Payment_model');
        $this->load->model('Project_model');
        $this->load->model('Admin_model');
    }

    public function index()
    {
        // get current invoice
        $countCurrent = $this->Payment_model->countAllInvoice('current');
        if (isset($_GET['c_m']))
            $currentInvoice = $this->Payment_model->getAllInvoice('current', $countCurrent);
        else
            $currentInvoice = $this->Payment_model->getAllInvoice('current');

        for ($i = 0; $i < count($currentInvoice); $i++) {
            $projectId = $currentInvoice[$i]['id_order'];

            $packet = $this->Project_model->getPacketByProjectId($projectId);
            $currentInvoice[$i]['harga_paket'] = $packet['harga'];
            $currentInvoice[$i]['nama_paket'] = $packet['nama'];

            $detailProject = $this->Project_model->getDetailProject($projectId);
            $currentInvoice[$i]['judul'] = $detailProject['judul'];
        }

        // get history invoice
        $countHistory = $this->Payment_model->countAllInvoice('history');
        if (isset($_GET['h_m']))
            $historyInvoice = $this->Payment_model->getAllInvoice('history', $countHistory);
        else
            $historyInvoice = $this->Payment_model->getAllInvoice('history');

        for ($i = 0; $i < count($historyInvoice); $i++) {
            $projectId = $historyInvoice[$i]['id_order'];

            $packet = $this->Project_model->getPacketByProjectId($projectId);
            $historyInvoice[$i]['harga_paket'] = $packet['harga'];
            $historyInvoice[$i]['nama_paket'] = $packet['nama'];

            $detailProject = $this->Project_model->getDetailProject($projectId);
            $historyInvoice[$i]['judul'] = $detailProject['judul'];
        }

        $data = [
            'currentInvoice' => $currentInvoice,
            'countCurrent' => $countCurrent,
            'historyInvoice' => $historyInvoice,
            'countHistory' => $countHistory
        ];
        $dataHeader = $this->setHeaderData("Payment");

        $this->load->view('admin/template/header', $dataHeader);
        $this->load->view('admin/payment/index', $data);
        $this->load->view('admin/template/footer');
    }

    public function detail($id_invoice)
    {
        if ($this->Payment_model->checkExistInvoice($id_invoice)) {
            $this->form_validation->set_rules('id', 'Id', 'required|trim');
            $this->form_validation->set_rules('tanggal', 'Tanggal', 'required|trim');
            $this->form_validation->set_rules('term', 'Term', 'required|trim');
            $this->form_validation->set_rules('biaya', 'Biaya', 'required|trim');

            if ($this->form_validation->run()) {
                $this->update($id_invoice);
            } else {

                $dataInvoice = $this->Payment_model->getInvoiceByInvoiceId($id_invoice);
                $dataInvoice['tgl_keluar'] = dateFormat($dataInvoice['tgl_keluar'], "d F Y");

                $projectId = $dataInvoice['id_order'];
                $packet = $this->Project_model->getPacketByProjectId($projectId);
                $invoiceByProject = $this->Payment_model->getInvoiceByProjectId($projectId, $dataInvoice['term']);

                $detailInvoice['harga_paket'] = $packet['harga'];
                $detailInvoice['nama_paket'] = $packet['nama'];
                $detailInvoice['detail_invoice'] = $invoiceByProject;
                $detailProject = $this->Project_model->getDetailProject($projectId);
                $detailPayment = $this->Payment_model->getPaymentByInvoiceId($id_invoice);

                $data['invoice'] = $dataInvoice;
                $data['detail_invoice'] = $detailInvoice;
                $data['payment'] = $detailPayment;
                $data['project'] = $detailProject;
                $dataHeader = $this->setHeaderData("Detail Payment");
                $this->load->view('admin/template/header', $dataHeader);
                $this->load->view('admin/payment/detail', $data);
                $this->load->view('admin/template/footer');
                $this->session->unset_userdata('upload_invoice');
            }
        } else
            show_404();
    }

    public function confirm($id_invoice)
    {
        if (isset($_POST['submitPayment'])) {
            $this->Payment_model->confirmPayment($id_invoice);
        } else
            show_404();

        if (isset($_GET['c'])) {
            redirect('/po-admin/client/' . $_GET['c']);
        }
        if (isset($_GET['p'])) {
            redirect('/po-admin/payment/' . $_GET['p']);
        }
        redirect('/po-admin/payment');
    }

    public function cancel($id_invoice)
    {
        if (isset($_POST['submitCancel'])) {
            $this->Payment_model->cancelInvoice($id_invoice);
            redirect('/po-admin/payment/' . $id_invoice);
        } else
            show_404();
    }

    private function update($id_invoice)
    {
        if (isset($_POST['submitInvoice'])) {
            $kode = $this->input->post('id');
            $tanggal = $this->input->post('tanggal');
            $term = $this->input->post('term');
            $biaya = $this->input->post('biaya');

            $config['upload_path'] = './asset/file/invoice/';
            $config['file_name'] = "invoice_" . $kode . "_term" . $term;
            $config['file_ext_tolower'] = TRUE;
            $config['overwrite'] = TRUE;
            $config['allowed_types'] = 'jpeg|jpg|png|pdf';
            $config['max_size'] = 500;

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('lampiran')) {
                $error = array('error' => $this->upload->display_errors());
                $this->session->set_flashdata('upload_invoice', "<p class='text-red text-center m-0'>error :</p><small class='text-red text-center'>" . $error['error'] . "</small>");
            } else {
                $data = $this->upload->data();
                $filename = $data['file_name'];
                $data = [
                    'tgl_keluar' => $tanggal,
                    'biaya' => $biaya,
                    'term' => $term,
                    'status' => 'unpaid',
                    'lampiran' => $filename,
                ];

                $this->Payment_model->updateInvoice($data, $id_invoice);
                $this->session->set_flashdata('upload_invoice', '<p class="text-yellow text-center m-0">Invoice berhasil dikirim</p>');
            }
            redirect("/po-admin/payment/$id_invoice");
        } else
            show_404();
    }

    public function upload($id_project)
    {
        if (isset($_POST['submitInvoice'])) {
            $this->form_validation->set_rules('id', 'Id', 'required|trim');
            $this->form_validation->set_rules('tanggal', 'Tanggal', 'required|trim');
            $this->form_validation->set_rules('term', 'Term', 'required|trim');
            $this->form_validation->set_rules('biaya', 'Biaya', 'required|trim');

            $f = $_GET['f'];
            if ($this->form_validation->run()) {
                $kode = $this->input->post('id');
                $tanggal = $this->input->post('tanggal');
                $term = $this->input->post('term');
                $biaya = $this->input->post('biaya');

                $config['upload_path'] = './asset/file/invoice/';
                $config['file_name'] = "invoice_" . $kode . "_term" . $term;
                $config['file_ext_tolower'] = TRUE;
                $config['overwrite'] = TRUE;
                $config['allowed_types'] = 'jpeg|jpg|png|pdf';
                $config['max_size'] = 500;

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('lampiran')) {
                    $error = array('error' => $this->upload->display_errors());
                    $this->session->set_flashdata('upload_invoice', "<p class='text-red text-center m-0'>error :</p><small class='text-red text-center'>" . $error['error'] . "</small>");
                } else {
                    $data = $this->upload->data();
                    $filename = $data['file_name'];
                    $data = [
                        'kode_invoice' => $kode,
                        'tgl_keluar' => $tanggal,
                        'biaya' => $biaya,
                        'term' => $term,
                        'status' => 'unpaid',
                        'lampiran' => $filename,
                        'id_order' => $id_project
                    ];

                    $this->Payment_model->uploadInvoice($data);
                    $this->session->set_flashdata('upload_invoice', '<p class="text-yellow text-center m-0">Invoice berhasil dikirim</p>');
                }
            } else {
                $this->session->set_flashdata('upload_invoice', "<small class='text-red text-center'>oops... something error</small>");
            }
            redirect("/po-admin/project/$f/$id_project");
        } else
            show_404();
    }

    private function setHeaderData($title)
    {
        $email = getAdminEmailSession();
        $admin = $this->Admin_model->getAdminByEmail($email);

        $data['header_title'] = $title;
        $data['header_admin'] = $admin['nama'];
        // fill '' or 'menu-open'
        $data['nav_menu_open'] = '';
        // fill '' or 'my-bg-black'
        $data['nav_dashboard'] = '';
        $data['nav_client'] = '';
        $data['nav_video'] = '';
        $data['nav_web'] = '';
        $data['nav_payment'] = 'my-bg-black';
        $data['nav_packet'] = '';
        $data['nav_article'] = '';
        $data['nav_admin'] = '';
        return $data;
    }
}
