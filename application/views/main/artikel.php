<div class="p-5">
    <div class="text-center">
        <h1 class="texthead-right-blue mt-5" style="background-size: 68% 100%;">blog</h1>
    </div>

    <div class="p-5">
        <!-- Article -->
        <div class="row">
            <div class="col-md-1">
                <!-- kosong -->
            </div>

            <?php
            $batas = 2;
            foreach ($article as $index => $value) {
                if ($index == 0) {
            ?>
                    <div class="col-md-7 text-center px-3 pb-5 pt-3">
                        <img class="d-flex justify-content-center" width="100%" style="object-fit: cover;" src="<?= base_url('/asset/file/article/' . $value['gambar']) ?>" alt="">
                        <a href="<?= base_url('/main/artikel/detail/' . $value['id_article']) ?>">
                            <h1 class="navbar-hover pt-3 fs-4"><?= $value['judul'] ?></h1>
                        </a>
                        <p><?= nl2br($value['teks']) ?></p>
                    </div>
                    <?php
                } else if ($index >= 1 && $index <= $batas) {
                    if ($index == 1) {
                    ?>
                        <div class="col-md-3 text-center px-3 pb-5 pt-3">
                        <?php
                    }
                        ?>
                        <div>
                            <img class="d-flex justify-content-center" width="100%" style="object-fit: cover;" src="<?= base_url('/asset/file/article/' . $value['gambar']) ?>" alt="">
                            <a href="<?= base_url('/main/artikel/detail/' . $value['id_article']) ?>">
                                <h4 class="navbar-hover pt-3 fs-4"><?= $value['judul'] ?></h4>
                            </a>
                            <p><?= nl2br($value['teks']) ?></p>
                        </div>
                        <?php
                        if ($index == $batas || $index == count($article) - 1) {
                        ?>
                        </div>
                    <?php
                        }
                    ?>

            <?php
                } else if ($index == 2) {
                }
            }
            ?>
            <div class="col-md-1">
                <!-- kosong -->
            </div>
        </div>

        <!-- Article Tips -->
        <div class="row">
            <div class="col-md-1">
                <!-- kosong -->
            </div>

            <div class="col-md p-5">
                <h5>Artikel & Tips</h5>
                <hr width="100%">
                <div class="row">
                    <?php
                    $row_limit = 1;
                    $column_limit = 1;
                    foreach ($article as $index => $value) {
                        if ($index <= $batas) {
                            //nothing to do
                        } else if ($index != count($article) - 1) {
                            if ($row_limit == 1) {
                    ?>
                                <div class="col-md">
                                    <ul class="list-unstyled mb-0">
                                        <li>
                                            <a href="<?= base_url('/main/artikel/detail/' . $value['id_article']) ?>" class="nav-link article-hover p-1"><?= $value['judul'] ?></a>
                                        </li>
                                    <?php
                                    $row_limit++;
                                } else if ($row_limit < 5) {
                                    ?>
                                        <li>
                                            <a href="<?= base_url('/main/artikel/detail/' . $value['id_article']) ?>" class="nav-link article-hover p-1"><?= $value['judul'] ?></a>
                                        </li>
                                    <?php
                                    $row_limit++;
                                } else {
                                    $row_limit = 1;
                                    $column_limit++;
                                    ?>
                                        <li>
                                            <a href="<?= base_url('/main/artikel/detail/' . $value['id_article']) ?>" class="nav-link article-hover p-1"><?= $value['judul'] ?></a>
                                        </li>
                                    </ul>
                                </div>

                                <?php
                                if($column_limit > 3){
                                    break;
                                }
                                ?>
                                
                                <div class="col-md">
                                    <ul class="list-unstyled mb-0">
                                        <li>
                                            <a href="<?= base_url('/main/artikel/detail/' . $value['id_article']) ?>" class="nav-link article-hover p-1"><?= $value['judul'] ?></a>
                                        </li>
                                    <?php
                                    $row_limit++;
                                }
                            } else {
                                    ?>
                                    <li>
                                        <a href="<?= base_url('/main/artikel/detail/' . $value['id_article']) ?>" class="nav-link article-hover p-1"><?= $value['judul'] ?></a>
                                    </li>
                                    </ul>
                                </div>
                        <?php
                            }
                        }
                        ?>
                </div>
            </div>

            <div class="col-md-1">
                <!-- Kosong -->
            </div>
        </div>
    </div>
</div>