<div class="container p-5">
    <div class="row justify-content-center">
        <div class="col-lg-7 col-12">
            <div class="d-flex justify-content-center mb-4">
                <h2 class="texthead-right-yellow" style="background-size: 64% 100%;">Kontak</h2>
            </div>
            <form class="px-4" action="" method="POST">
                <div class="mb-3">
                    <label for="nama" class="form-label">nama</label>
                    <input type="text" class="form-control form-rounded-0" name="nama" id="nama">
                </div>
                <div class="mb-3">
                    <label for="email" class="form-label">email</label>
                    <input type="email" class="form-control form-rounded-0" name="email" id="email">
                </div>
                <div class="mb-3">
                    <label for="telepon" class="form-label">telepon</label>
                    <input type="number" class="form-control form-rounded-0" name="telepon" id="telepon">
                </div>
                <div class="mb-3">
                    <label for="pesan" class="form-label">pesan</label>
                    <textarea class="form-control form-rounded-0" name="pesan" id="pesan" rows="3"></textarea>
                </div>
                <div class="row pt-3">
                    <div class="col-md-2">
                        <button type="submit" class="btn form-btn-rounded-0" name="submit">kirim</button>
                    </div>
                </div>

                <div class="row pt-3 mt-5">
                    <div class="col-md-5">
                        <img class="" width="75%" style="object-fit: cover;" src="<?= base_url('/asset/image/general/polaris_a.png') ?>" alt="">
                    </div>
                    <div class="col-md-5">
                        <div class="text-start">
                            Monggang Lor RT.39 <br>
                            Sewon, Bantul, Yogyakarta<br>
                            <br>
                            Email:<br>
                            polaristud@gmail.com<br>
                            <br>
                            WhatsApp:<br>
                            087822418041<br>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>