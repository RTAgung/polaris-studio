<div class="p-5">
    <!-- TOP -->
    <div class="row d-flex align-items-end">
        <div class="col-md-3">
            <!-- KOSONG -->
        </div>
        <div class="col-md-3 d-flex justify-content-end">
            <img class="pt-5 mr-4" width="80%" style="object-fit: cover;" src="<?= base_url('/asset/image/general/ilustrasi_web.png') ?>" alt="">
        </div>
        <div class="col-md-3">
            <h1 class="align-text-bottom">Bangun Website</h1>
            <p>
                Yuk bangun website-mu sendiri, tingkatkan<br>
                pengunjungmu sendiri, dirumahmu sendiri.<br>
            </p>
            <div class="d-flex flex-row font-weight-bold">
                <div class="m-1 px-2 rounded" style="background-color: #F6DAE0; font-weight: bold;">web profile</div>
                <div class="m-1 px-2 rounded" style="background-color: #F6DAE0; font-weight: bold;">web berita</div>
                <div class="m-1 px-2 rounded" style="background-color: #F6DAE0; font-weight: bold;">toko online</div>
            </div>
        </div>
        <div class="col-md-3">
            <!-- KOSONG -->
        </div>
    </div>


    <!-- PORTOFOLIO -->
    <div class="mt-5 pt-5 pb-5">
        <div class="row justify-content-md-center">
            <div class="col-md-3">
                <a href="#">
                    <img class="bg-shadow" width="80%" style="object-fit: cover; height: 160px;" src="<?= base_url('/asset/image/general/portofolio_web_1.png') ?>" alt="">
                </a>
            </div>
            <div class="col-md-3">
                <a href="#">
                    <img class="bg-shadow" width="80%" style="object-fit: cover; height: 160px;" src="<?= base_url('/asset/image/general/portofolio_web_2.png') ?>" alt="">
                </a>
            </div>
            <div class="col-md-3">
                <a href="#">
                    <img class="bg-shadow" width="80%" style="object-fit: cover; height: 160px;" src="<?= base_url('/asset/image/general/portofolio_web_3.png') ?>" alt="">
                </a>
            </div>
        </div>

        <div class="row justify-content-md-center mt-4">
            <div class="col-md-3">
                <a href="#">
                    <img class="bg-shadow" width="80%" style="object-fit: cover; height: 160px;" src="<?= base_url('/asset/image/general/portofolio_web_4.png') ?>" alt="">
                </a>
            </div>
            <div class="col-md-3">
                <a href="#">
                    <img class="bg-shadow" width="80%" style="object-fit: cover; height: 160px;" src="<?= base_url('/asset/image/general/portofolio_web_5.png') ?>" alt="">
                </a>
            </div>
            <div class="col-md-3">
                <a href="#">
                    <img class="bg-shadow" width="80%" style="object-fit: cover; height: 160px;" src="<?= base_url('/asset/image/general/portofolio_web_6.png') ?>" alt="">
                </a>
            </div>
        </div>
    </div>


    <div class="text-center pt-5 mt-5">
        <h2 class="texthead-right-yellow" style="background-size: 70% 100%;">Paket & Biaya</h2>
        <?php
        foreach ($packet as $index => $value) {
            if (fmod($index, 2) == 0) {
        ?>
                <div class="row justify-content-md-center mt-4">
                    <div class="col-md-2">
                        <div class="card" style="height: 600px;">
                            <div class="card-header text-white pb-0" style="background-color: #2C226D;">
                                <h5 class="mb-2"><?= $value['nama'] ?></h5>
                            </div>
                            <div class="card-body">
                                <p class="card-text text-start" style="height: 200px;"><?= nl2br($value['deskripsi']) ?></p>

                                <p class="card-title" style="height: 150px;"><?= nl2br($value['detail']) ?></p>

                                <p class="card-title"><s>Rp <?= number_format($value['harga_awal'], 0, ".", ".") ?></s></p>

                                <h5 class="card-title align-bottom">Rp <?= number_format($value['harga'], 0, ".", ".") ?></h5>

                                <a href="<?= base_url('/main/form-web') ?>" type="button" class="btn text-white mt-5" style="background-color: #47A6D6;">pilih paket</a>
                            </div>
                        </div>
                    </div>
                    <?php
                    if ($index == count($packet) - 1) {
                    ?>
                </div>
            <?php
                    }
                } else if (fmod($index, 2) == 1) {
            ?>
            <div class="col-md-2">
                <div class="card" style="height: 600px;">
                    <div class="card-header text-white pb-0" style="background-color: #2C226D;">
                        <h5 class="mb-2"><?= $value['nama'] ?></h5>
                    </div>
                    <div class="card-body">

                        <p class="card-text text-start" style="height: 200px;"><?= nl2br($value['deskripsi']) ?></p>

                        <p class="card-title" style="height: 150px;"><?= nl2br($value['detail']) ?></p>


                        <p class="card-title"><s>Rp <?= number_format($value['harga_awal'], 0, ".", ".") ?></s></p>

                        <h5 class="card-title align-bottom">Rp <?= number_format($value['harga'], 0, ".", ".") ?></h5>

                        <a href="<?= base_url('/main/form-web') ?>" type="button" class="btn text-white mt-5" style="background-color: #47A6D6;">pilih paket</a>
                    </div>
                </div>
            </div>
    </div>
<?php
                }
            }
?>
</div>

<div class="text-center pt-5 mt-5">
    <p><b>Paket diatas sudah termasuk:</b><br>
        Konsultasi / Revisi / Profesional Desainer Grafis & Web Programmer</p>
</div>
</div>