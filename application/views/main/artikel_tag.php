<div class="p-5">
    <div class="text-center mb-4">
        <h2 class="texthead-right-blue mt-5" style="background-size: 68% 100%;">#<?= $tag['nama'] ?></h2>
    </div>

    <?php
    foreach ($article as $index => $value) {
        $valueDate = date("d M Y", strtotime($value['tgl_buat']));
    ?>
        <div class="shadow bg-body rounded-15 p-4 mb-4">
            <div class="row">
                <div class="col-md-3">
                    <img width="100%" style="object-fit: cover;" src="<?= base_url('/asset/file/article/' . $value['gambar']) ?>" alt="">
                </div>
                <div class="col-md">
                    <a href="<?= base_url('/main/artikel/detail/' . $value['id_article']) ?>">
                        <h1 class="navbar-hover pt-3 fs-5"><?= $value['judul'] ?></h1>
                    </a>
                    <small class="text-muted"><?= $valueDate ?></small>
                    <p class="mt-3"><?= nl2br($value['teks']) ?></p>

                </div>
            </div>
        </div>
    <?php
    }
    ?>
</div>