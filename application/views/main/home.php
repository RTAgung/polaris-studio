<!-- CAROUSEL -->
<div id="carouselExampleControls" class="carousel slide pb-5 pt-5" data-ride="carousel">
    <div class="carousel-inner ">
        <div class="carousel-item active">
            <img class="d-block w-75 mx-auto" src="<?= base_url('asset/image/general/video_promosi.png') ?>" alt="First slide">
        </div>
        <div class="carousel-item">
            <img class="d-block w-75 mx-auto" src="<?= base_url('asset/image/general/web_promosi.png') ?>" alt="Second slide">
        </div>
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
        <span class="material-icons" aria-hidden="true" style="font-size:70px;color:#EACB2B;">keyboard_arrow_left</span>
        <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
        <span class="material-icons" aria-hidden="true" style="font-size:70px;color:#EACB2B;">keyboard_arrow_right</span>
        <span class="visually-hidden">Next</span>
    </button>
</div>



<!-- SETIAP MERK MEMPUNYAI KISAH -->
<div class="row pt-5 m-0">
    <div class="col-md-2">
        <!-- Kosong -->
    </div>
    <div class="col-md-10 py-5 texthead-right-yellow" style="background-size: 70% 85%; background-position: top right;">
        <div class="row">
            <div class="col-md-6">
                <img class="" width="85%" style="object-fit: cover;" src="<?= base_url('/asset/image/general/mbak_dan_adek_ketawa.png') ?>" alt="">

            </div>

            <div class="col-md-6">
                <div class="text-start">
                    <h2>setiap merk <br>mempunyai kisah</h2>
                    <p>Storytelling adalah cara yang paling ampuh<br>
                        untuk menceritakan ide pada dunia.<br>
                        <br>
                        <b>Promosi dengan konten yang menarik</b><br>
                        <br>
                        Konten yang menarik meningkatkan<br>
                        kesadaran terhadap merk lebih tinggi<br>
                        daripada promosi secara hardsell.<br>
                    </p>
                    <a href="<?= base_url('/main/tentang') ?>">
                        <i class="material-icons" style="font-size:30px;color:white;">arrow_forward</i>
                    </a>

                </div>
            </div>

        </div>
    </div>
</div>






<!-- Desain web & Video Promosi -->
<div class="row py-5 m-0">
    <div class="col-md-3 text-center">
        <!-- Kosong -->
    </div>
    <div class="col-md-3 text-center">
        <a href="<?= base_url('/main/web') ?>">
            <img class="mb-5" width="250px" style="object-fit: cover;" src="<?= base_url('/asset/image/general/ilustrasi_web2.png') ?>" alt="">
            <h4 class="text-dark">Desain Web</h4>
            <p class="text-dark">Yuk bangun website-mu sendiri,<br>
                tingkatkan pengunjungmu sendiri,<br>
                dirumahmu sendiri.<br>
            </p>
        </a>
    </div>

    <div class="col-md-3 text-center">
        <a href="<?= base_url('/main/video') ?>">
            <img class="mb-5" width="165px" style="object-fit: cover;" src="<?= base_url('/asset/image/general/ilustrasi_video2.png') ?>" alt="">
            <h4 class="text-dark">Video Promosi</h4>
            <p class="text-dark">Video saat ini menjadi konsumsi<br>
                utama dan sangat powerfull.<br>
                Buat video iklanmu sendiri di sini.<br>
            </p>
        </a>

    </div>
    <div class="col-md-3 text-center">
        <!-- Kosong -->
    </div>
</div>

<!-- Video Youtube -->

<div class="row py-5 m-0">
    <div class="col-md-3 text-center">
        <!-- Kosong -->
    </div>
    <div class="col-md-6 text-center">
        <div class="iframe-container">
            <iframe src="https://www.youtube.com/embed/5oe0MVFc4Qg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" loading="lazy" allowfullscreen></iframe>
        </div>
    </div>

    <div class="col-md-3 text-center">
        <!-- Kosong -->
    </div>
</div>



<!-- Cocok untuk konten di sosial media -->

<div class="text-center pt-5">
    <h2 class="texthead-left-yellow" style="background-size: 85% 100%;">Cocok untuk konten di sosial media</h2>
    <br>
    <h2 class="texthead-right-yellow" style="background-size: 80% 100%;">paket terjangkau untuk UMKM hingga brand besar.</h2>
    <a class="nav-link navbar-hover font-weight-bold py-5 px-4 h4" href="<?= base_url('/main/video') ?>">
        <span class="navbar-hover align-middle">Pilih Paket</span>
        <i class="material-icons align-middle" style="font-size:30px;color:#EACB2B;">arrow_forward</i>
    </a>
</div>