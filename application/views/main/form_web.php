<div class="container p-5">
    <?php
    if (isset($form_filled)) {
    ?>
        <div class="row justify-content-center">
            <div class="col-lg-7 col-12">
                <div class="d-flex justify-content-center mb-4">
                    <h5 class="text-center" style="background-size: 65% 100%;">Data anda telah dikirim<br>Silakan konfirmasi dan diskusi melalui kontak berikut</h5>
                </div>
                <div class="d-flex justify-content-center mb-4">
                    <a target="_blank" href="https://wa.me/6287822418041?text=<?= $konfirmasi ?>" class="btn rounded-pill" id="floating-wa-button">
                        <i class="fab fa-whatsapp align-middle text-white" style="font-size: 18px;"></i>
                        <h5 class="d-inline align-middle text-white" style="font-size: 16px;">konfirmasi</h5>
                    </a>
                </div>
            </div>
        </div>
    <?php
    } else {
    ?>
        <div class="row justify-content-center">
            <div class="col-lg-7 col-12">
                <div class="d-flex justify-content-center mb-4">
                    <h2 class="texthead-right-yellow" style="background-size: 65% 100%;">form paket web</h2>
                </div>
                <form class="px-4" action="<?= base_url('/main/form-web') ?>" method="POST">
                    <div class="mb-3">
                        <label for="nama" class="form-label">nama usaha</label>
                        <input type="text" class="form-control form-rounded-0" name="nama" id="nama" required>
                        <small class="text-danger">
                            <?= form_error('nama') ?>
                        </small>
                    </div>
                    <div class="mb-3">
                        <label for="email" class="form-label">email</label>
                        <input type="email" class="form-control form-rounded-0" name="email" id="email" required>
                        <small class="text-danger">
                            <?= form_error('email') ?>
                        </small>
                    </div>
                    <div class="mb-3">
                        <label for="whatsapp" class="form-label">No.HP/ WhatsApp </label>
                        <input type="number" class="form-control form-rounded-0" name="whatsapp" id="whatsapp" required>
                        <small class="text-danger">
                            <?= form_error('whatsapp') ?>
                        </small>
                    </div>
                    <div class="mb-3">
                        <label for="instagram" class="form-label">akun instagram</label>
                        <input type="text" class="form-control form-rounded-0" name="instagram" id="instagram" required>
                        <small class="text-danger">
                            <?= form_error('instagram') ?>
                        </small>
                    </div>
                    <div class="mb-3">
                        <label for="deskripsi" class="form-label">deskripsi singkat</label>
                        <textarea class="form-control form-rounded-0" name="deskripsi" id="deskripsi" rows="3" required></textarea>
                        <small class="text-danger">
                            <?= form_error('deskripsi') ?>
                        </small>
                    </div>

                    <div class="mb-3">
                        <label for="lampiran" class="form-label fw-normal">paket</label>
                        <select class="form-select" name="paket" id="paket" aria-label="Default select example">
                            <?php
                            foreach ($packet as $index => $value) {
                            ?>
                                <option value="<?= $value['id_packet'] ?>"><?= $value['nama'] ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>

                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="cheked" name="privacyPolicy" id="flexCheckDefault" required oninvalid="this.setCustomValidity('Read and accept Privacy Policy')" oninput="this.setCustomValidity('')">
                        <label class="form-check-label" for="flexCheckDefault">
                            I have read and accept <a href="#" data-bs-toggle="modal" data-bs-target="#privacyPolicyModal">Privacy Policy</a>.
                        </label>
                    </div>

                    <div class="row pt-3">
                        <div class="col-md-2">
                            <button type="submit" class="btn form-btn-rounded-0" name="submit">kirim</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    <?php
    }
    ?>
</div>



<!-- Modal privacy policy -->
<div class="modal fade" id="privacyPolicyModal" tabindex="-1" aria-labelledby="privacyPolicyModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="btn-close float-end" data-bs-dismiss="modal" aria-label="Close"></button>
                <div class="text-center">
                    <h5 class="fw-bold m-0 mt-4">Privacy Policy</h5>
                </div>

                <p class="m-4">
                    1. Jual beli jasa konten-konten Polaris sudah diketahui oleh kedua belah pihak<br>
                    2. Hak cipta sepenuhnya oleh Polaris<br>
                    3. Jika terjadi perselisihan, maka diselesaikan secara kekeluargaan<br>
                    4. Polaris akan menjamin segala data pribadi yang di daftarkan <br>
                    5. Mencuri atau mengambil konten-konten tanpa seizing pihak Polaris, akan diproses secara hukum yang berlaku<br>

                </p>
            </div>
        </div>
    </div>
</div>