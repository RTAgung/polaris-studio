<div class="px-5 pt-0">
    <div class="p-5">
        <div class="row">
            <div class="col-md-1">
                <!-- kosong -->
            </div>
            <div class="col-md-10 text-justify px-3 pb-5 pt-3">
                <h1 class="navbar-hover pt-3 fs-4"><?= $detail_artikel['judul'] ?></h1>
                <img class="mx-auto d-block" width="100%" style="object-fit: cover;" src="<?= base_url('/asset/file/article/' . $detail_artikel['gambar']) ?>" alt="">
                <p class="pt-5"><?= nl2br($detail_artikel['teks']) ?><br><br></p>
                <div class="d-flex flex-row bd-highlight mb-3">
                    <?php
                    foreach ($tags as $index => $value) {
                    ?>
                        <a href="<?= base_url('/main/artikel/tag/' . $value['id_tag'])?>" class="text-white badge bg-success m-1 p-2 bd-highlight">#<?= $value['nama'] ?></a>

                    <?php
                    }
                    ?>
                </div>
            </div>

            <div class="col-md-1">
                <!-- kosong -->
            </div>
        </div>

        <hr width="100%">

        <div class="row justify-content-between">
            <div class="col-md-1">
                <!-- kosong -->
            </div>
            <?php
            if($articleByTag!=0){
                foreach($articleByTag as $index => $value){
                    ?>
                    <div class="col-md-3 text-justify px-3 pb-5 pt-3">
                        <img class="mx-auto d-block" width="100%" style="object-fit: cover;" src="<?= base_url('/asset/image/article/'.$value['gambar']) ?>" alt="">
                        <a href="<?= base_url('/main/artikel/detail/'.$value['id_article']) ?>">
                            <h5 class="navbar-hover pt-3"><?=$value['judul']?></h5>
                        </a>
                    </div>
                    
                    <?php
                }
            }
            ?>
            <div class="col-md-1">
                <!-- kosong -->
            </div>
        </div>
    </div>
</div>