<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <!-- Material Icon -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Internal CSS -->
    <link rel="stylesheet" href="<?= base_url('asset/css/style.css') ?>">
    <link rel="stylesheet" href="<?= base_url('asset/css/text.css') ?>">

    <!-- Google recaptcha -->
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>

    <title><?= $header_title; ?></title>

    <link rel="icon" href="<?= base_url('/asset/image/general/polaris_logo_mini.png') ?>" type="image/x-icon">
</head>

<body>

    <nav class="navbar navbar-expand-lg navbar-light bg-white">
        <div class="container my-4 py-3 ">
            <button class="navbar-toggler mb-3" type="button" data-bs-toggle="collapse" data-bs-target="#navbarToggler" aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-center" id="navbarToggler">
                <a class="navbar-brand mx-4" href="<?= base_url('/main') ?>">
                    <img src="<?= base_url('asset/image/general/logo_hitam.png') ?>" width="190px" class="d-inline-block ">
                </a>
                <ul class="navbar-nav mx-4">
                    <li class="nav-item ">
                        <a class="nav-link navbar-hover fw-bold pt-3 px-4 h5 " href="<?= base_url('/main/tentang') ?>">
                            <span class="<?= $header_tentang ?>">tentang</span>
                        </a>
                    </li>
                    <li class="nav-item navbar-hover">
                        <a class="nav-link navbar-hover fw-bold pt-3 px-4 h5" href="<?= base_url('/main/web') ?>">
                            <span class="<?= $header_web ?>">buat web</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link navbar-hover fw-bold pt-3 px-4 h5" href="<?= base_url('/main/video') ?>">
                            <span class="<?= $header_video ?>">buat video</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link navbar-hover fw-bold pt-3 px-4 h5" href="<?= base_url('/main/artikel') ?>">
                            <span class="<?= $header_artikel ?>">artikel</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="btn btn-primary border-0 fw-bold mx-4 mt-2 px-4 h5 bg-blue rounded-pill text-white" href="<?= base_url('/client/login') ?>">login</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <!-- Floating button -->
    <div class="fixed-bottom m-5">
        <a target="_blank" href="https://wa.me/6287822418041?text=<?= $konsultasi ?>" class="btn rounded-pill position-absolute bottom-0 end-0 pt-1" id="floating-wa-button">
            <i class="fab fa-whatsapp align-middle text-white" style="font-size: 18px;"></i>
            <h5 class="d-inline align-middle text-white" style="font-size: 16px;">konsultasi</h5>
        </a>
    </div>