<!-- Footer -->
<footer class="text-center text-white text-lg-start mt-5">
    <!-- Grid container -->
    <div class="container p-5">
        <!--Grid row-->
        <div class="row text-left p-5 justify-content-md-center">

            <!--Grid column-->
            <div class="col-lg-2 col-md-3 mb-4 mb-lg-0">
                <ul class="list-unstyled mb-0">
                    <li>
                        <a href="<?= base_url('/main/tentang') ?>" class="nav-link footer-hover p-0">Tentang</a>
                    </li>
                    <li>
                        <a href="<?= base_url('/main/web') ?>" class="nav-link footer-hover p-0">Buat Web</a>
                    </li>
                    <li>
                        <a href="<?= base_url('/main/video') ?>" class="nav-link footer-hover p-0">Buat Video</a>
                    </li>
                    <li>
                        <a href="<?= base_url('/main/artikel') ?>" class="nav-link footer-hover p-0">Artikel</a>
                    </li>
                    <li>
                        <a href="<?= base_url('/main/kontak') ?>" class="nav-link footer-hover p-0">Kontak</a>
                    </li>
                </ul>
            </div>
            <!--Grid column-->

            <!--Grid column-->
            <div class="col-lg-4 col-md-6 mb-4 mb-lg-0">
                <ul class="list-unstyled mb-0">
                    <li>
                        <p>Monggang Lor RT.39 Sewon<br>
                            Bantul, Yogyakarta<br>
                            Email: polaristud@gmail.com<br>
                            Open:<br>
                            Sunday – Monday / 10.00 AM – 5 PM</p>
                    </li>
                </ul>
            </div>
            <!--Grid column-->

            <!--Grid column-->
            <div class="col-lg-3 col-md-6 mb-4 mb-lg-0">
                <ul class="list-unstyled mb-0">
                    <li>
                        <a href="https://www.facebook.com" target="_blank"><img src="<?= base_url('asset/image/general/bx_bxl-facebook.png') ?>" width="25"></a>
                        <a href="https://www.instagram.com/polaris.stud/" class="ml-3" target="_blank"><img src="<?= base_url('asset/image/general/akar-icons_instagram-fill.png') ?>" width="25"></a>
                        <a href="https://www.youtube.com/channel/UC0tpal1J8_7W0jifSTEJJ1Q" class="ml-3" target="_blank"><img src="<?= base_url('asset/image/general/clarity_play-solid.png') ?>" width="25"></a>
                    </li>
                    <li>
                        <p class="nav-link px-0 mt-2 mb-0">berlangganan berita</p>
                    </li>
                    <li>
                        <form action="#">
                            <div class="form-group">
                                <input type="text" class="form-control form-rounded-50" placeholder="Email">
                            </div>
                            <button type="submit" class="px-0 btn footer-hover fw-bold"><span class="link-hover-underline">Kirim</span></button>
                        </form>
                    </li>
                </ul>
            </div>
            <!--Grid column-->
        </div>
        <!--Grid row-->
    </div>
    <!-- Grid container -->

    <!-- Copyright -->
    <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
        © 2021 Copyright
    </div>
    <!-- Copyright -->
</footer>
<!-- Footer -->

<!-- Bootstrap Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>

<!-- font awesome -->
<script src="https://kit.fontawesome.com/862e40d34f.js" crossorigin="anonymous"></script>

</body>

</html>