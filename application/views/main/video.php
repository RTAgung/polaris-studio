<div class="p-5">
    <!-- TOP -->
    <div class="row d-flex align-items-end">
        <div class="col-md-3">
            <!-- KOSONG -->
        </div>
        <div class="col-md-3 d-flex justify-content-end">
            <img class="pt-5 mr-4" width="55%" style="object-fit: cover;" src="<?= base_url('/asset/image/general/ilustrasi_video.png') ?>" alt="">
        </div>
        <div class="col-md-3">
            <h1 class="align-text-bottom">Video Promosi</h1>
            <p>
                Konten video saat ini menjadi konsumsi<br>
                utama dan punya dampak lebih powerfull.<br>
                Buat video iklanmu sendiri di sini.<br>
            </p>
        </div>
        <div class="col-md-3">
            <!-- KOSONG -->
        </div>
    </div>


    <!-- PORTOFOLIO -->
    <div class="mt-5 pt-5 pb-5">
        <div class="row justify-content-md-center">
            <div class="col-md-3">
                <a href="<?= base_url('main/video/detail/1') ?>">
                    <img class="bg-shadow" width="80%" style="object-fit: cover; height: 160px;" src="<?= base_url('/asset/image/general/portofolio_video1.jpg') ?>" alt="">
                </a>
            </div>
            <div class="col-md-3">
                <a href="<?= base_url('main/video/detail/2') ?>">
                    <img class="bg-shadow" width="80%" style="object-fit: cover; height: 160px;" src="<?= base_url('/asset/image/general/portofolio_video2.jpg') ?>" alt="">
                </a>
            </div>
            <div class="col-md-3">
                <a href="<?= base_url('main/video/detail/3') ?>">
                    <img class="bg-shadow" width="80%" style="object-fit: cover; height: 160px;" src="<?= base_url('/asset/image/general/portofolio_video3.jpg') ?>" alt="">
                </a>
            </div>
        </div>

        <div class="row justify-content-md-center mt-4">
            <div class="col-md-3">
                <a href="<?= base_url('main/video/detail/4') ?>">
                    <img class="bg-shadow" width="80%" style="object-fit: cover; height: 160px;" src="<?= base_url('/asset/image/general/portofolio_video4.jpg') ?>" alt="">
                </a>
            </div>
            <div class="col-md-3">
                <a href="<?= base_url('main/video/detail/5') ?>">
                    <img class="bg-shadow" width="80%" style="object-fit: cover; height: 160px;" src="<?= base_url('/asset/image/general/portofolio_video5.jpg') ?>" alt="">
                </a>
            </div>
            <div class="col-md-3">
                <a href="<?= base_url('main/video/detail/6') ?>">
                    <img class="bg-shadow" width="80%" style="object-fit: cover; height: 160px;" src="<?= base_url('/asset/image/general/portofolio_video6.jpg') ?>" alt="">
                </a>
            </div>
        </div>
    </div>


    <div class="text-center pt-5 mt-5">
        <h2 class="texthead-right-yellow" style="background-size: 70% 100%;">Paket & Biaya</h2>
        <?php
        foreach ($packet as $index => $value) {
            if (fmod($index, 2) == 0) {
        ?>
                <div class="row justify-content-md-center mt-4">
                    <div class="col-md-2">
                        <div class="card" style="height: 600px;">
                            <div class="card-header text-white pb-0" style="background-color: #2C226D;">
                                <h5 class="mb-2"><?= $value['nama'] ?></h5>
                            </div>
                            <div class="card-body">
                                <p class="card-text text-start" style="height: 200px;"><?= nl2br($value['deskripsi']) ?></p>

                                <p class="card-title" style="height: 150px;"><?= nl2br($value['detail']) ?></p>

                                <p class="card-title"><s>Rp <?= number_format($value['harga_awal'], 0, ".", ".") ?></s></p>

                                <h5 class="card-title align-bottom">Rp <?= number_format($value['harga'], 0, ".", ".") ?></h5>

                                <a href="<?= base_url('/main/form-video') ?>" type="button" class="btn text-white mt-5" style="background-color: #47A6D6;">pilih paket</a>
                            </div>
                        </div>
                    </div>
                    <?php
                    if ($index == count($packet) - 1) {
                    ?>
                </div>
            <?php
                    }
                } else if (fmod($index, 2) == 1) {
            ?>
            <div class="col-md-2">
                <div class="card" style="height: 600px;">
                    <div class="card-header text-white pb-0" style="background-color: #2C226D;">
                        <h5 class="mb-2"><?= $value['nama'] ?></h5>
                    </div>
                    <div class="card-body">

                        <p class="card-text text-start" style="height: 200px;"><?= nl2br($value['deskripsi']) ?></p>

                        <p class="card-title" style="height: 150px;"><?= nl2br($value['detail']) ?></p>


                        <p class="card-title"><s>Rp <?= number_format($value['harga_awal'], 0, ".", ".") ?></s></p>

                        <h5 class="card-title align-bottom">Rp <?= number_format($value['harga'], 0, ".", ".") ?></h5>

                        <a href="<?= base_url('/main/form-video') ?>" type="button" class="btn text-white mt-5" style="background-color: #47A6D6;">pilih paket</a>
                    </div>
                </div>
            </div>
    </div>
<?php
                }
            }
?>

<div class="text-center pt-5 mt-5">
    <p><b>Paket diatas sudah termasuk:</b><br>
        Konsultasi / Revisi / Profesional Desainer grafis & video & fotografer<br>
        membuat storyboard, melakukan shoot video, melakukan editing – final</p>
</div>
</div>
</div>