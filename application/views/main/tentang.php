<div class="text-center pt-5">
    <h1 class="texthead-right-red fs-2" style="background-size: 80% 100%;">Polaris Studio</h1>
</div>


<div class="row pt-5 m-0">
    <div class="col-md-4">
        <!-- Kosong -->
    </div>
    <div class="col-md-8 py-5 texthead-right-yellow" style="background-size: 56%;">
        <div class="row">
            <div class="col-md-6">
                <img class="" width="85%" style="object-fit: cover;" src="<?= base_url('/asset/image/general/ilustrasi_video3.png') ?>" alt="">

            </div>

            <div class="col-md-6">
                <div class="text-start">
                    <p>Di era saat ini, hampir semua brand mau<br>
                        tidak mau naik level ke dunia digital.<br>
                        Tapi tidak banyak yang mampu menjawab<br>
                        segala permasalahan digital dikarenakan<br>
                        dukungan yang kurang memadai.<br>
                        <br>
                        Polaris dengan Tim Kreatif yang berkualitas<br>
                        dan peralatan yang memadai, membantu Merk<br>
                        kamu untuk masuk ke dalam dunia digital.<br>
                        <br>
                        Studio audio visual yang membantu<br>
                        mulai dari memproduksi video promosi<br>
                        hingga membangun website.<br>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row pt-5 m-0">
    <div class="col-md-8 py-5 texthead-left-red" style="background-size: 56% 90%;">
        <div class="row">
            <div class="col-md-6 d-flex justify-content-end p-5">
                <div class="text-end align-self-center">
                    <h2 class="display-6 fs-3 text-white">mewujudkan konsep menjadi nyata</h2>
                </div>
            </div>

            <div class="col-md-6">
                <img class="" width="85%" style="object-fit: cover;" src="<?= base_url('/asset/image/general/tentang_5.png') ?>" alt="">
            </div>
        </div>
    </div>
</div>

<div class="row pt-5 m-0">
    <div class="col-md-2">
        <!-- Kosong -->
    </div>
    <div class="col-md-10 py-5 texthead-right-blue" style="background-size: 70%; background-position: top right;">
        <div class="row">
            <div class="col-md-6">
                <img class="" width="85%" style="object-fit: cover;" src="<?= base_url('/asset/image/general/tentang_4.png') ?>" alt="">
            </div>

            <div class="col-md-6">
                <div class="text-start text-white">
                    <h2 class="display-6 fs-3">Tim profesional yang siap<br>
                        membangun brand kamu <br></h2>
                    <p>Brand Strategist, Desainer Grafis, Videografer,<br>
                        Fotografer dan Developer kami siap <br>
                        untuk membangun brand bersama. <br>
                    </p>
                    <a href="<?= base_url('/main/tentang') ?>">
                        <i class="material-icons" style="font-size:30px;color:white;">arrow_forward</i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>