<div class="row py-5 m-0">
    <div class="col-md-2 text-center">
        <!-- Kosong -->
    </div>
    <div class="col-md-8 text-center">
        <h4 class="text-start pb-4">
            <?= $id['title'] ?>
        </h4>
        <div class="iframe-container">
            <iframe src="<?= $id['link'] ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" loading="lazy" allowfullscreen></iframe>
        </div>
        <p class="text-start pt-4">
            <?= $id['deskripsi'] ?>
        </p>
    </div>

    <div class="col-md-2 text-center">
        <!-- Kosong -->
    </div>
</div>