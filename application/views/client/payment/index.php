<div class="container p-5">
	<div class="row justify-content-md-center">
		<div class="col-lg-7">
			<div class="d-flex justify-content-center mb-4">
				<h2 class="texthead-right-red" style="background-size: 57% 100%;">pembayaran</h2>
			</div>

			<!-- form search -->
			<div class="shadow bg-body rounded-15">
				<form class="mt-5" action="">
					<div class="input-group mb-3">
						<input type="text" class="form-control rounded-15 border-0" name="search" placeholder="cari disini" aria-label="Recipient's username" aria-describedby="button-addon2" style="border-color: #4D9DCF;">
						<button class="btn btn-secondary rounded-15 border-0 bg-red" type="button" id="button-addon2">
							<i class="material-icons align-middle">search</i>
							cari
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="row justify-content-md-center">

		<!-- Payment On Progress -->
		<div class="col-lg-7 mb-4">
			<div class="shadow bg-body rounded-15 p-4">
				<h5 class="text-center">daftar pembayaran</h5>
				<?= $this->session->flashdata('upload_payment') ?>
				<!-- table content -->
				<table width="100%" class="my-4 mt-5" id="tablePayment">
					<?php
					$count = $countInvoice;
					foreach ($invoices as $index => $i_item) :
						$count--;
					?>
						<tr>
							<td><?= $index + 1 ?>.</td>
							<td colspan="2">
								<div class="d-flex bd-highlight">
									<div class="me-auto bd-highlight">
										<p class="m-0 fw-bold">Invoice <?= $i_item['judul'] ?> (term <?= $i_item['term'] ?>)</p>
										<small>ID <?= $i_item['kode_invoice'] ?></small>
									</div>
									<div class="bd-highlight mx-3" style="align-self: center;">
										<span class="rounded-2 border-red py-1 px-2 border-1 text-smaller"><?= $i_item['status'] ?></span>
									</div>
									<div class="bd-highlight">
										<div class="text-center">
											<small class="mt-1 d-block"><?= $i_item['tgl_keluar'] ?></small>
											<button class="material-icons m-0 align-middle bg-transparent border-0" id="buttonArrow" data-bs-target="detailPayment<?= $i_item['id_invoice'] ?>">keyboard_arrow_down</button>
										</div>
									</div>
								</div>
								<div class="js-hideDetailPayment" id="detailPayment<?= $i_item['id_invoice'] ?>">
									<div class="m-2 border-top border-bottom">
										<div class="mx-2">
											<div class="row">
												<div class="col-10">
													<table width="100%" id="tableDetailPayment">
														<?php $hargaPaket = $i_item['harga_paket']; ?>
														<tr>
															<td class="p-0" width="50%">
																<p class="text-small">Paket <?= $i_item['nama_paket'] ?></p>
															</td>
															<td class="p-0" width="50%">
																<p class="fw-bold text-small">: Rp. <?= number_format($hargaPaket, 0, ".", ".") ?></p>
															</td>
														</tr>
														<?php $hargaBayar = 0; ?>
														<?php foreach ($i_item['detail_invoice'] as $d_item) : ?>
															<tr>
																<td class="p-0" width="50%">
																	<p class="ms-2 text-smaller">Term <?= $d_item['term'] ?></p>
																</td>
																<td class="p-0" width="50%">
																	<p class="text-smaller">
																		<span class="fw-bold">: Rp. <?= number_format($d_item['biaya'], 0, ".", ".") ?></span>
																		<?php if ($d_item['status'] == "paid") : ?>
																			<?php $hargaBayar = $hargaBayar + $d_item['biaya']; ?>
																			&nbsp; <i class="ms-2 me-1 material-icons align-middle text-smaller text-yellow">check_circle</i><?= $d_item['status'] ?>
																		<?php else : ?>
																			&nbsp; <i class="ms-2 me-1 material-icons align-middle text-smaller text-red">error</i><?= $d_item['status'] ?>
																		<?php endif ?>
																	</p>
																</td>
															</tr>
														<?php endforeach ?>
														<tr>
															<?php $hargaSisa = $hargaPaket - $hargaBayar; ?>
															<td class="p-0" width="50%">
																<p class="text-small">Sisa</p>
															</td>
															<td class="p-0" width="50%">
																<p class="fw-bold text-small">: Rp. <?= number_format($hargaSisa, 0, ".", ".") ?></p>
															</td>
														</tr>
													</table>
												</div>
												<div class="col-2">
													<div class="d-flex justify-content-end">
														<a target="_blank" class="btn btn-primary btn-sm border-0 m-1 align-middle bg-blue" href="<?= base_url('/asset/file/invoice/' . $i_item['lampiran']) ?>">
															<i class="material-icons align-middle" style="font-size: 18px;">download</i>
														</a>
													</div>
													<?php if ($d_item['status'] == "unpaid") : ?>
														<div class="d-flex justify-content-end">
															<button class="btn btn-dark border-0 btn-sm m-1 bg-red fw-bold" data-bs-toggle="modal" data-bs-target="#paymentModal<?= $i_item['id_invoice'] ?>">bayar</button>
														</div>
													<?php endif ?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</td>
						</tr>
					<?php endforeach ?>
				</table>

				<?php if ($count > 0) : ?>
					<p class="text-end"><a class="text-underline text-dark link-hover-underline" href="<?= base_url('/client/payment?more=all') ?>">tampilkan semua</a></p>
				<?php endif ?>

				<!-- pagination -->
				<!-- <div class="pt-4">
					<nav aria-label="Page navigation example">
						<ul class="pagination justify-content-center text-smaller">
							<li class="page-item">
								<a class="page-link pagination-red pagination-red-disabled" href="#" aria-label="Previous">
									<span aria-hidden="true">&laquo;</span>
								</a>
							</li>
							<li class="page-item"><a class="page-link pagination-red pagination-red-active" href="#">1</a></li>
							<li class="page-item"><a class="page-link pagination-red" href="#">2</a></li>
							<li class="page-item"><a class="page-link pagination-red" href="#">3</a></li>
							<li class="page-item">
								<a class="page-link pagination-red" href="#" aria-label="Next">
									<span aria-hidden="true">&raquo;</span>
								</a>
							</li>
						</ul>
					</nav>
				</div> -->
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<?php foreach ($invoices as $index => $i_item) : ?>
	<div class="modal fade" id="paymentModal<?= $i_item['id_invoice'] ?>" tabindex="-1" aria-labelledby="paymentModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-body">
					<button type="button" class="btn-close float-end" data-bs-dismiss="modal" aria-label="Close"></button>
					<div class="text-center">
						<p class="fw-bold m-0 mt-4">Invoice <?= $i_item['judul'] ?> (term <?= $i_item['term'] ?>)</p>
						<p class="m-0 text-small">ID <?= $i_item['kode_invoice'] ?></p>
					</div>
					<div class="mt-4 mx-3">
						<table width="100%" id="tableDetailPayment">
							<?php $hargaPaket = $i_item['harga_paket']; ?>
							<tr>
								<td class="p-0" width="50%">
									<p>Paket <?= $i_item['nama_paket'] ?></p>
								</td>
								<td class="p-0" width="50%">
									<p class="fw-bold">: Rp. <?= number_format($hargaPaket, 0, ".", ".") ?></p>
								</td>
							</tr>
							<?php $hargaBayar = 0; ?>
							<?php foreach ($i_item['detail_invoice'] as $d_item) : ?>
								<tr>
									<td class="p-0" width="50%">
										<p class="text-small">Term <?= $d_item['term'] ?></p>
									</td>
									<td class="p-0" width="50%">
										<p class="text-small">
											<span class="fw-bold">: Rp. <?= number_format($d_item['biaya'], 0, ".", ".") ?></span>
											<?php if ($d_item['status'] == "paid") : ?>
												<?php $hargaBayar = $hargaBayar + $d_item['biaya']; ?>
												&nbsp; <i class="ms-2 me-1 material-icons align-middle text-small text-yellow">check_circle</i><?= $d_item['status'] ?>
											<?php else : ?>
												&nbsp; <i class="ms-2 me-1 material-icons align-middle text-small text-red">error</i><?= $d_item['status'] ?>
											<?php endif ?>
										</p>
									</td>
								</tr>
							<?php endforeach ?>
							<tr>
								<?php $hargaSisa = $hargaPaket - $hargaBayar; ?>
								<td class="p-0" width="50%">
									<p>Sisa</p>
								</td>
								<td class="p-0" width="50%">
									<p class="fw-bold">: Rp. <?= number_format($hargaSisa, 0, ".", ".") ?></p>
								</td>
							</tr>
						</table>
						<form class="mt-4" action="<?= base_url('/client/payment/upload/' . $i_item['id_invoice']) ?>" method="POST" enctype="multipart/form-data">
							<div class="mb-3">
								<label for="buktiPembayaran" class="form-label">unggah bukti pembayaran</label>
								<input type="file" class="form-control form-rounded-0" name="buktiPembayaran" id="buktiPembayaran" required>
								<small class="text-danger">ukuran maksimum : 500 KB, jenis file : pdf/png/jpg</small>
							</div>
							<button type="submit" class="btn form-btn-rounded-0 mt-2 mb-4" name="submit">kirim</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endforeach ?>