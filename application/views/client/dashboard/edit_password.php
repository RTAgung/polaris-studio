<div class="container p-5">
    <div class="row justify-content-center">
        <div class="col-lg-7 col-12">
            <div class="d-flex justify-content-center mb-4">
                <h2 class="texthead-right-yellow" style="background-size: 53% 100%;">edit kata sandi</h2>
            </div>
            <form class="px-4" action="" method="POST">
                <div class="mb-3">
                    <label for="passwordLama" class="form-label">kata sandi lama</label>
                    <input type="password" class="form-control form-rounded-0" name="passwordLama" id="passwordLama" required>
                    <small class="text-danger">
                        <?= form_error('passwordLama') ?>
                    </small>
                    <?= $this->session->flashdata('message1') ?>
                </div>
                <div class="mb-3">
                    <label for="passwordBaru" class="form-label">kata sandi baru</label>
                    <input type="password" class="form-control form-rounded-0" name="passwordBaru" id="passwordBaru" required>
                    <small class="text-danger">
                        <?= form_error('passwordBaru') ?>
                    </small>
                </div>
                <div class="mb-3">
                    <label for="konfirmasiPasswordBaru" class="form-label">konfirmasi kata sandi baru</label>
                    <input type="password" class="form-control form-rounded-0" name="konfirmasiPasswordBaru" id="konfirmasiPasswordBaru" required>
                    <small class="text-danger">
                        <?= form_error('konfirmasiPasswordBaru') ?>
                    </small>
                </div>
                <div class="pt-3">
                    <button type="submit" class="btn form-btn-rounded-0" name="submit">simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>