<div class="container p-5">
    <div class="row justify-content-center">
        <div class="col-lg-7 col-12">
            <div class="d-flex justify-content-center mb-4">
                <h2 class="texthead-right-yellow" style="background-size: 64% 100%;">edit profil</h2>
            </div>
            <form class="px-4" action="" method="POST">
                <?= $this->session->flashdata('lengkapi_profile') ?>
                <div class="mb-3">
                    <label for="namaInstansi" class="form-label">nama instansi</label>
                    <input type="text" class="form-control form-rounded-0" name="namaInstansi" id="namaInstansi" value="<?= $client['nama_instansi'] ?>" disabled readonly required>
                </div>
                <div class="mb-3">
                    <label for="namaPemilikInstansi" class="form-label">nama pemilik instansi</label>
                    <input type="text" class="form-control form-rounded-0" name="namaPemilikInstansi" id="namaPemilikInstansi" value="<?= set_value('namaPemilikInstansi', $client['nama_pemilik']) ?>" required>
                    <small class="text-danger">
                        <?= form_error('namaPemilikInstansi') ?>
                    </small>
                </div>
                <div class="mb-3">
                    <label for="email" class="form-label">email</label>
                    <input type="email" class="form-control form-rounded-0" name="email" id="email" value="<?= $client['email'] ?>" disabled readonly required>
                </div>
                <div class="mb-3">
                    <label for="alamat" class="form-label">alamat</label>
                    <textarea class="form-control form-rounded-0" name="alamat" id="alamat" rows="3" required><?= set_value('alamat', $client['alamat']) ?></textarea>
                    <small class="text-danger">
                        <?= form_error('alamat') ?>
                    </small>
                </div>
                <div class="mb-3">
                    <label for="instagram" class="form-label">instagram</label>
                    <input type="text" class="form-control form-rounded-0" name="instagram" id="instagram" value="<?= set_value('instagram', $client['instagram']) ?>" required>
                    <small class="text-danger">
                        <?= form_error('instagram') ?>
                    </small>
                </div>
                <div class="mb-3">
                    <label for="whatsapp" class="form-label">whatsapp</label>
                    <input type="text" class="form-control form-rounded-0" name="whatsapp" id="whatsapp" value="<?= set_value('whatsapp', $client['whatsapp']) ?>" required>
                    <small class="text-danger">
                        <?= form_error('whatsapp') ?>
                    </small>
                </div>
                <div class="row pt-3">
                    <div class="col-md-2">
                        <button type="submit" class="btn form-btn-rounded-0" name="submit">simpan</button>
                    </div>
                    <div class="col-md-10 pt-1">
                        <small>
                            <a class="link-hover-underline" href="<?= base_url('/client/dashboard/edit-password') ?>" style="color: blue;">ingin ubah kata sandi?</a>
                        </small>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>