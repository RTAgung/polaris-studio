<div class="container px-lg-5">
	<div class="row p-4 px-lg-5 justify-content-md-center">

		<!-- Dashboard Profile -->
		<div class="col-lg-4 col-12">
			<div class="shadow mb-3 bg-body rounded-15">
				<div class="text-white bg-blue p-4 rounded-15">
					<table width="100%">
						<tr>
							<td>
								<h4><?= $client['nama_instansi'] ?></h4>
								<p><?= $client['nama_pemilik'] ?></p>
							</td>
							<td class="d-flex flex-row-reverse bd-highlight">
								<a class="text-white" href="<?= base_url('/client/dashboard/edit-profile') ?>">
									<i class="fas fa-pen-square icon-edit-profile"></i>
								</a>
							</td>
						</tr>
					</table>
					<div class="row">
						<div class="col-lg-2 col-1">
							<i class="material-icons">mail</i>
						</div>
						<div class="col-lg-10 col-11">
							<p><?= $client['email'] ?></p>
						</div>
						<div class="col-lg-2 col-1">
							<i class="fab fa-instagram" style="font-size: 24px;"></i>
						</div>
						<div class="col-lg-10 col-11">
							<p><?= $client['instagram'] ?></p>
						</div>
						<div class="col-lg-2 col-1">
							<i class="fab fa-whatsapp" style="font-size: 24px;"></i>
						</div>
						<div class="col-lg-10 col-11">
							<p><?= $client['whatsapp'] ?></p>
						</div>
						<div class="col-lg-2 col-1">
							<i class="material-icons">business</i>
						</div>
						<div class="col-lg-10 col-11">
							<p>
								<?php
								if (empty($client['alamat']))
									echo "belum dilengkapi";
								else
									echo $client['alamat'];
								?>
							</p>
						</div>
					</div>
				</div>
			</div>
			<?php if (empty($client['alamat']) || empty($client['nama_pemilik'])) : ?>
				<div class="px-4 pb-4">
					<i class="material-icons align-middle text-red">error</i>
					<a class="text-dark mx-2" href="<?= base_url('/client/dashboard/edit-profile') ?>">
						<div class="link-lengkapi-data d-inline">
							<span class="btn-text-underline">lengkapi data</span>
							<i class="material-icons align-middle">arrow_forward</i>
						</div>
					</a>
				</div>
			<?php endif ?>
		</div>

		<div class="col-lg-7">

			<!-- Dashboard Current Project -->
			<div class="shadow mb-4 bg-body rounded-15 p-4">
				<h4 class="texthead-right-yellow mb-3" style="background-size: 70% 100%;">proyek saat ini</h4>
				<table width="100%" class="my-3" id="tableProject">
					<?php
					$countP = $countProject;
					foreach ($project as $index => $p_item) :
						$countP--;
					?>
						<tr>
							<td class="top-start"><?= $index + 1 ?></td>
							<td class="top-start">
								<p class="m-0 fw-bold">
									<?php
									if (empty($p_item['judul']))
										echo "Judul Proyek";
									else
										echo $p_item['judul'];
									?>
								</p>
								<small>Paket <?= $p_item['nama'] ?></small>
							</td>
							<td class="middle-end">
								<span class="rounded-2 border-blue py-1 px-2 border-1 text-smaller"><?= $p_item['status'] ?></span>
							</td>
							<td class="middle-end">
								<?php if ($p_item['status'] == "pending") : ?>
									<a href="<?php echo base_url('/client/project/' . $p_item['id_order'] . '/form-order') ?>" class="btn btn-dark fw-bold border-0 rounded-pill bg-red text-small">isi form</a>
								<?php else : ?>
									<a href="<?php echo base_url('/client/project/' . $p_item['id_order']) ?>" class="btn btn-primary fw-bold border-0 rounded-pill bg-blue text-small">detail</a>
								<?php endif ?>
							</td>
						</tr>
					<?php endforeach ?>
				</table>
				<?php if ($countP > 0) : ?>
					<div class="text-right">
						<a href="<?= base_url('/client/project') ?>" class="text-underline text-dark link-hover-underline"><?= $countP ?> lihat lebih...</a>
					</div>
				<?php endif ?>
			</div>

			<!-- Dashboard Current Payment -->
			<div class="shadow mb-4 bg-body rounded-15 p-4">
				<h4 class="texthead-right-red mb-3" style="background-size: 61% 100%;">pembayaran saat ini</h4>
				<?= $this->session->flashdata('upload_payment') ?>
				<table width="100%" class="my-3" id="tablePayment">
					<?php
					$countI = $countInvoice;
					foreach ($invoices as $index => $i_item) :
						$countI--;
					?>
						<tr>
							<td><?= $index + 1 ?>.</td>
							<td colspan="2">
								<div class="d-flex bd-highlight">
									<div class="me-auto bd-highlight">
										<p class="m-0 fw-bold">Invoice <?= $i_item['judul'] ?> (term <?= $i_item['term'] ?>)</p>
										<small>ID <?= $i_item['kode_invoice'] ?></small>
									</div>
									<div class="bd-highlight mx-3" style="align-self: center;">
										<span class="rounded-2 border-red py-1 px-2 border-1 text-smaller"><?= $i_item['status'] ?></span>
									</div>
									<div class="bd-highlight">
										<div class="text-center">
											<small class="mt-1 d-block"><?= $i_item['tgl_keluar'] ?></small>
											<button class="material-icons m-0 align-middle bg-transparent border-0" id="buttonArrow" data-bs-target="detailPayment<?= $i_item['id_invoice'] ?>">keyboard_arrow_down</button>
										</div>
									</div>
								</div>
								<div class="js-hideDetailPayment" id="detailPayment<?= $i_item['id_invoice'] ?>">
									<div class="m-2 border-top border-bottom">
										<div class="mx-2">
											<div class="row">
												<div class="col-10">
													<table width="100%" id="tableDetailPayment">
														<?php $hargaPaket = $i_item['harga_paket']; ?>
														<tr>
															<td class="p-0" width="50%">
																<p class="text-small">Paket <?= $i_item['nama_paket'] ?></p>
															</td>
															<td class="p-0" width="50%">
																<p class="fw-bold text-small">: Rp. <?= number_format($hargaPaket, 0, ".", ".") ?></p>
															</td>
														</tr>
														<?php $hargaBayar = 0; ?>
														<?php foreach ($i_item['detail_invoice'] as $d_item) : ?>
															<tr>
																<td class="p-0" width="50%">
																	<p class="ms-2 text-smaller">Term <?= $d_item['term'] ?></p>
																</td>
																<td class="p-0" width="50%">
																	<p class="text-smaller">
																		<span class="fw-bold">: Rp. <?= number_format($d_item['biaya'], 0, ".", ".") ?></span>
																		<?php if ($d_item['status'] == "paid") : ?>
																			<?php $hargaBayar = $hargaBayar + $d_item['biaya']; ?>
																			&nbsp; <i class="ms-2 me-1 material-icons align-middle text-smaller text-yellow">check_circle</i><?= $d_item['status'] ?>
																		<?php else : ?>
																			&nbsp; <i class="ms-2 me-1 material-icons align-middle text-smaller text-red">error</i><?= $d_item['status'] ?>
																		<?php endif ?>
																	</p>
																</td>
															</tr>
														<?php endforeach ?>
														<tr>
															<?php $hargaSisa = $hargaPaket - $hargaBayar; ?>
															<td class="p-0" width="50%">
																<p class="text-small">Sisa</p>
															</td>
															<td class="p-0" width="50%">
																<p class="fw-bold text-small">: Rp. <?= number_format($hargaSisa, 0, ".", ".") ?></p>
															</td>
														</tr>
													</table>
												</div>
												<div class="col-2">
													<div class="d-flex justify-content-end">
														<a target="_blank" class="btn btn-primary btn-sm border-0 m-1 align-middle bg-blue" href="<?= base_url('/asset/file/invoice/' . $i_item['lampiran']) ?>">
															<i class="material-icons align-middle" style="font-size: 18px;">download</i>
														</a>
													</div>
													<?php if ($d_item['status'] == "unpaid") : ?>
														<div class="d-flex justify-content-end">
															<button class="btn btn-dark border-0 btn-sm m-1 bg-red fw-bold" data-bs-toggle="modal" data-bs-target="#paymentModal<?= $i_item['id_invoice'] ?>">bayar</button>
														</div>
													<?php endif ?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</td>
						</tr>
					<?php endforeach ?>
				</table>
				<?php if ($countI > 0) : ?>
					<div class="text-right">
						<a href="<?= base_url('/client/payment') ?>" class="text-underline text-dark link-hover-underline"><?= $countI ?> lihat lebih...</a>
					</div>
				<?php endif ?>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<?php foreach ($invoices as $index => $i_item) : ?>
	<div class="modal fade" id="paymentModal<?= $i_item['id_invoice'] ?>" tabindex="-1" aria-labelledby="paymentModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-body">
					<button type="button" class="btn-close float-end" data-bs-dismiss="modal" aria-label="Close"></button>
					<div class="text-center">
						<p class="fw-bold m-0 mt-4">Invoice <?= $i_item['judul'] ?> (term <?= $i_item['term'] ?>)</p>
						<p class="m-0 text-small">ID <?= $i_item['kode_invoice'] ?></p>
					</div>
					<div class="mt-4 mx-3">
						<table width="100%" id="tableDetailPayment">
							<?php $hargaPaket = $i_item['harga_paket']; ?>
							<tr>
								<td class="p-0" width="50%">
									<p>Paket <?= $i_item['nama_paket'] ?></p>
								</td>
								<td class="p-0" width="50%">
									<p class="fw-bold">: Rp. <?= number_format($hargaPaket, 0, ".", ".") ?></p>
								</td>
							</tr>
							<?php $hargaBayar = 0; ?>
							<?php foreach ($i_item['detail_invoice'] as $d_item) : ?>
								<tr>
									<td class="p-0" width="50%">
										<p class="text-small">Term <?= $d_item['term'] ?></p>
									</td>
									<td class="p-0" width="50%">
										<p class="text-small">
											<span class="fw-bold">: Rp. <?= number_format($d_item['biaya'], 0, ".", ".") ?></span>
											<?php if ($d_item['status'] == "paid") : ?>
												<?php $hargaBayar = $hargaBayar + $d_item['biaya']; ?>
												&nbsp; <i class="ms-2 me-1 material-icons align-middle text-small text-yellow">check_circle</i><?= $d_item['status'] ?>
											<?php else : ?>
												&nbsp; <i class="ms-2 me-1 material-icons align-middle text-small text-red">error</i><?= $d_item['status'] ?>
											<?php endif ?>
										</p>
									</td>
								</tr>
							<?php endforeach ?>
							<tr>
								<?php $hargaSisa = $hargaPaket - $hargaBayar; ?>
								<td class="p-0" width="50%">
									<p>Sisa</p>
								</td>
								<td class="p-0" width="50%">
									<p class="fw-bold">: Rp. <?= number_format($hargaSisa, 0, ".", ".") ?></p>
								</td>
							</tr>
						</table>
						<form class="mt-4" action="<?= base_url('/client/payment/upload/' . $i_item['id_invoice']) . '?d=d' ?>" method="POST" enctype="multipart/form-data">
							<div class="mb-3">
								<label for="buktiPembayaran" class="form-label">unggah bukti pembayaran</label>
								<input type="file" class="form-control form-rounded-0" name="buktiPembayaran" id="buktiPembayaran" required>
								<small class="text-danger">ukuran maksimum : 500 KB, jenis file : pdf/png/jpg</small>
							</div>
							<button type="submit" class="btn form-btn-rounded-0 mt-2 mb-4" name="submit">kirim</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endforeach ?>