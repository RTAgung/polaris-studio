<div class="container p-5 overflow-hidden">
    <div class="shadow mb-5 bg-body rounded-15">
        <div class="row">
            <div class="col-lg-7">
                <img class="rounded-15" height="100%" width="100%" style="object-fit: cover;" src="<?= base_url('/asset/image/general/mbak_pink.png') ?>" alt="">
            </div>
            <div class="col-lg-5 py-5">
                <div class="text-center mt-4">
                    <h2 class="texthead-right-yellow" style="background-size: 52% 100%;">masuk</h2>
                </div>
                <form class="px-5" action="" method="POST">
                    <div class="mb-3">
                        <label for="email" class="form-label">email</label>
                        <input type="email" class="form-control form-rounded-0" name="email" id="email" value="<?= set_value('email') ?>" required>
                        <small class="text-danger">
                            <?= form_error('email') ?>
                        </small>
                        <?= $this->session->flashdata('message1') ?>
                    </div>
                    <div class="mb-3">
                        <label for="password" class="form-label">kata sandi</label>
                        <input type="password" class="form-control form-rounded-0" name="password" id="password" required>
                        <small class="text-danger">
                            <?= form_error('password') ?>
                        </small>
                        <?= $this->session->flashdata('message2') ?>
                    </div>
                    <div class="mb-3">
                        <small>lupa password? <a target="_blank" href="https://wa.me/6287822418041?text=<?= $hubungi_lupa_password ?>">hubungi admin</a></small>
                    </div>
                    <button type="submit" class="btn form-btn-rounded-0 mb-4">masuk</button>
                </form>
            </div>
        </div>
    </div>
</div>