<?php
if ($detail['status'] == 'finished' || $detail['status'] == 'canceled')
    $statusDisabled = "disabled";
else
    $statusDisabled = "";
?>
<div class="container px-5 py-4">
    <div class="row justify-content-md-center">
        <div class="col-lg-8">
            <h3 class="text-center mb-2"><?= $detail['judul'] ?></h3>
            <p class="text-center fs-5 mb-2">Paket <?= $detail['nama'] ?></p>
            <p class="text-center text-small">mulai pada <?= $detail['tgl_mulai'] ?></p>
            <div class="d-flex justify-content-between m-4 mb-3">
                <button class="btn btn-dark border-0 bg-red fw-bold" data-bs-toggle="modal" data-bs-target="#assetModal">edit aset</button>
                <span class="rounded-15 border-red py-1 border-2 px-2 text-red fw-bold"><?= $detail['status'] ?></span>
            </div>
            <div class="border-red rounded-15 p-4">

                <!-- chat box scrolling -->

                <div class="overflow-auto px-3" style="height: 500px;" id="chat-box">
                    <?php foreach ($chat as $itemChat) : ?>
                        <?php if ($itemChat['pengirim'] == "client") : ?>
                            <div class="row justify-content-end m-0">
                                <div class="shadow-sm px-4 py-2 mb-3 rounded-15 chat-box chat-bg-green">
                                    <div class="d-block">
                                        <small class="text-smaller"><?= $itemChat['tgl_kirim'] ?></small>
                                    </div>
                                    <p class="m-0">
                                        <?php
                                        $teks = $itemChat['teks'];
                                        $url = '@(http)?(s)?(://)?(([a-zA-Z])([-\w]+\.)+([^\s\.]+[^\s]*)+[^,.\s])@';
                                        $teks = preg_replace($url, '<a href="http$2://$4" target="_blank" title="$0">$0</a>', $teks);
                                        echo nl2br($teks);
                                        ?>
                                    </p>
                                </div>
                            </div>
                        <?php elseif ($itemChat['pengirim'] == "admin") : ?>
                            <div class="shadow-sm px-4 py-2 mb-3 rounded-15 chat-box chat-bg-grey">
                                <div class="d-block">
                                    <small class="text-smaller"><?= $itemChat['tgl_kirim'] ?></small>
                                    <?php if (!empty($itemChat['label'])) : ?>
                                        &nbsp;<i class="material-icons align-middle text-smaller ms-2 text-red">sell</i>
                                        <small class="text-smaller"><?= $itemChat['label'] ?></small>
                                    <?php endif ?>
                                </div>
                                <p class="m-0">
                                    <?php
                                    $teks = $itemChat['teks'];
                                    $url = '@(http)?(s)?(://)?(([a-zA-Z])([-\w]+\.)+([^\s\.]+[^\s]*)+[^,.\s])@';
                                    $teks = preg_replace($url, '<a href="http$2://$4" target="_blank" title="$0">$0</a>', $teks);
                                    echo nl2br($teks);
                                    ?>
                                </p>
                            </div>
                        <?php endif ?>
                    <?php endforeach ?>
                </div>

                <!-- form chat -->

                <form class="mt-4" action="" method="POST">
                    <div class="input-group">
                        <textarea type="text" class="form-control border-red border-end-0 rounded-15" <?= $statusDisabled ?> id="chat-form" name="komentar" placeholder="tulis komentar" aria-label="tulis komentar" aria-describedby="basic-addon" rows="1" required></textarea>
                        <button type="submit" class="input-group-text border-red bg-transparent border-start-0 rounded-15" name="submitChat" id="basic-addon" <?= $statusDisabled ?>>
                            <i class="material-icons align-middle text-red chat-icon">send</i>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="assetModal" tabindex="-1" aria-labelledby="assetModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="btn-close float-end" data-bs-dismiss="modal" aria-label="Close"></button>
                <div class="text-center">
                    <p class="fw-bold m-0 mt-4">Aset <?= $detail['judul'] ?></p>
                </div>
                <div class="mt-4 mx-3">
                    <form class="mt-4" action="" method="POST">
                        <div class="mb-3">
                            <label for="asset" class="form-label text-small">link aset yang digunakan</label>
                            <input type="text" class="form-control form-rounded-0" name="asset" id="asset" value="<?= set_value('asset', $detail['asset']) ?>" required <?= $statusDisabled ?>>
                            <small class="text-danger">
                                <?= form_error('asset') ?>
                            </small>
                        </div>
                        <div class="d-grid gap-2 col-2 mx-auto">
                            <button type="submit" class="btn form-btn-rounded-0 mt-2 mb-4" name="submitAsset" <?= $statusDisabled ?>>simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>