<div class="container p-5">
    <div class="row justify-content-center">
        <div class="col-lg-7 col-12">
            <div class="d-flex justify-content-center mb-4">
                <h2 class="texthead-right-yellow" style="background-size: 71% 100%;">form permintaan web</h2>
            </div>
            <form class="px-4" action="" method="POST">
                <div class="mb-3">
                    <label for="judulProyek" class="form-label">judul proyek</label>
                    <input type="text" class="form-control form-rounded-0" name="judulProyek" id="judulProyek" required>
                    <small class="text-danger">
                        <?= form_error('judulProyek') ?>
                    </small>
                </div>
                <div class="mb-3">
                    <label for="jenisUsahaInstansi" class="form-label">jenis usaha instansi</label>
                    <input type="text" class="form-control form-rounded-0" name="jenisUsahaInstansi" id="jenisUsahaInstansi" required>
                    <small class="text-danger">
                        <?= form_error('jenisUsahaInstansi') ?>
                    </small>
                </div>
                <div class="mb-3">
                    <label class="form-label">tujuan website</label>
                    <div class="ms-3">
                        <div class="form-check">
                            <input class="form-check-input" name="tujuanProfile" type="checkbox" id="tujuanCheckProfile" value="profile">
                            <label class="form-check-label" for="tujuanCheckProfile">
                                profile
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" name="tujuanTokoOnline" type="checkbox" id="tujuanCheckTokoOnline" value="toko online">
                            <label class="form-check-label" for="tujuanCheckTokoOnline">
                                toko online
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" name="tujuanUpdateInfo" type="checkbox" id="tujuanCheckUpdateInfo" value="update info">
                            <label class="form-check-label" for="tujuanCheckUpdateInfo">
                                update info
                            </label>
                        </div>
                        <div class="form-check">
                            <div class="row">
                                <div class="col-auto">
                                    <input class="form-check-input" name="tujuanOther" type="checkbox" id="tujuanCheckOther" value="1">
                                    <label class="form-check-label" for="tujuanCheckOther">
                                        other
                                    </label>
                                </div>
                                <div class="col ms-4">
                                    <input type="text" class="form-control form-control-sm form-rounded-0" name="tujuanTextOther" id="tujuanCheckTextOther">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mb-3">
                    <label for="menuWebsite" class="form-label">menu website</label>
                    <div class="ms-3">
                        <div class="form-check">
                            <input class="form-check-input" name="menuAbout" type="checkbox" value="about us/profile" id="menuCheckAbout">
                            <label class="form-check-label" for="menuCheckAbout">
                                about us / profile
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" name="menuProduct" type="checkbox" value="product/services" id="menuCheckProduct">
                            <label class="form-check-label" for="menuCheckProduct">
                                product / services
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" name="menuContact" type="checkbox" value="contact us" id="menuCheckContact">
                            <label class="form-check-label" for="menuCheckContact">
                                contact us
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" name="menuShop" type="checkbox" value="shop" id="menuCheckShop">
                            <label class="form-check-label" for="menuCheckShop">
                                shop
                            </label>
                        </div>
                        <div class="form-check">
                            <div class="row">
                                <div class="col-auto">
                                    <input class="form-check-input" name="menuOther" type="checkbox" value="1" id="menuCheckOther">
                                    <label class="form-check-label" for="menuCheckOther">
                                        other
                                    </label>
                                </div>
                                <div class="col ms-4">
                                    <input type="text" class="form-control form-control-sm form-rounded-0" name="menuTextOther" id="menuCheckTextOther">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mb-3">
                    <label for="domain" class="form-label d-block">sudah memiliki domain?</label>
                    <div class="ms-3">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="domain" id="domainRadioYa" value="1">
                            <label class="form-check-label" for="domainRadioYa">ya</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="domain" id="domainRadioTidak" value="0" checked>
                            <label class="form-check-label" for="domainRadioTidak">tidak</label>
                        </div>
                    </div>
                    <small class="text-tiny ms-3">*biaya paket diluar biaya domain dan hosting</small>
                </div>
                <div class="mb-3">
                    <label for="hosting" class="form-label d-block">sudah memiliki hosting?</label>
                    <div class="ms-3">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="hosting" id="hostingRadioYa" value="1">
                            <label class="form-check-label" for="hostingRadioYa">ya</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="hosting" id="hostingRadioTidak" value="0" checked>
                            <label class="form-check-label" for="hostingRadioTidak">tidak</label>
                        </div>
                    </div>
                    <small class="text-tiny ms-3">*biaya paket diluar biaya domain dan hosting</small>
                </div>
                <div class="mb-3">
                    <label for="asset" class="form-label">unggah link aset yang digunakan (Google Drive/Dropbox/dan lain-lain)</label>
                    <input type="text" class="form-control form-rounded-0" name="asset" id="asset" required>
                    <small class="text-danger">
                        <?= form_error('asset') ?>
                    </small>
                </div>
                <div class="mb-3">
                    <label for="catatan" class="form-label">catatan tambahan (opsional)</label>
                    <textarea class="form-control form-rounded-0" name="catatan" id="catatan" rows="3"></textarea>
                    <small class="text-danger">
                        <?= form_error('catatan') ?>
                    </small>
                </div>
                <button type="submit" class="btn form-btn-rounded-0 mt-3" name="submit">simpan</button>
            </form>
        </div>
    </div>
</div>