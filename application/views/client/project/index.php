<div class="container p-5">
  <div class="row justify-content-md-center">
    <div class="col-lg-6">
      <div class="d-flex justify-content-center mb-4">
        <h2 class="texthead-right-blue" style="background-size: 53% 100%;">proyek</h2>
      </div>

      <!-- form search -->
      <div class="shadow bg-body rounded-15">
        <form class="mt-5" action="">
          <div class="input-group mb-3">
            <input type="text" class="form-control rounded-15 border-0" name="search" placeholder="cari disini" aria-label="Recipient's username" aria-describedby="button-addon2" style="border-color: #4D9DCF;">
            <button class="btn btn-primary rounded-15 border-0 bg-blue" type="button" id="button-addon2">
              <i class="material-icons align-middle">search</i>
              cari
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="row justify-content-md-center">

    <!-- Project On Progress -->
    <div class="col-lg-6 mb-4">
      <div class="shadow bg-body rounded-15 p-4">
        <h5 class="text-center">daftar proyek</h5>

        <!-- table content -->
        <table width="100%" class="my-4 mt-5" id="tableProject">
          <?php
          $count = $countProject;
          foreach ($project as $index => $p_item) :
            $count--;
          ?>
            <tr>
              <td class="top-start"><?= $index + 1 ?></td>
              <td class="top-start">
                <p class="m-0 fw-bold">
                  <?php
                  if (empty($p_item['judul']))
                    echo "Judul Proyek";
                  else
                    echo $p_item['judul'];
                  ?>
                </p>
                <small>Paket <?= $p_item['nama'] ?></small>
              </td>
              <td class="middle-end">
                <span class="rounded-2 border-blue py-1 px-2 border-1 text-smaller"><?= $p_item['status'] ?></span>
              </td>
              <td class="middle-end">
                <?php if ($p_item['status'] == "pending") : ?>
                  <a href="<?php echo base_url('/client/project/' . $p_item['id_order'] . '/form-order') ?>" class="btn btn-dark fw-bold border-0 rounded-pill bg-red text-small">isi form</a>
                <?php else : ?>
                  <a href="<?php echo base_url('/client/project/' . $p_item['id_order']) ?>" class="btn btn-primary fw-bold border-0 rounded-pill bg-blue text-small">detail</a>
                <?php endif ?>
              </td>
            </tr>
          <?php endforeach ?>
        </table>

        <?php if ($count > 0) : ?>
          <p class="text-end"><a class="text-underline text-dark link-hover-underline" href="<?= base_url('/client/project?more=all') ?>">tampilkan semua</a></p>
        <?php endif ?>
        <!-- pagination -->
        <!-- <div class="pt-4">
          <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-center text-smaller">
              <li class="page-item">
                <a class="page-link pagination-blue pagination-blue-disabled" href="#" aria-label="Previous">
                  <span aria-hidden="true">&laquo;</span>
                </a>
              </li>
              <li class="page-item"><a class="page-link pagination-blue pagination-blue-active" href="#">1</a></li>
              <li class="page-item"><a class="page-link pagination-blue" href="#">2</a></li>
              <li class="page-item"><a class="page-link pagination-blue" href="#">3</a></li>
              <li class="page-item">
                <a class="page-link pagination-blue" href="#" aria-label="Next">
                  <span aria-hidden="true">&raquo;</span>
                </a>
              </li>
            </ul>
          </nav>
        </div> -->
      </div>
    </div>
  </div>
</div>