<div class="container p-5">
    <div class="row justify-content-center">
        <div class="col-lg-7 col-12">
            <div class="d-flex justify-content-center mb-4">
                <h2 class="texthead-right-yellow" style="background-size: 69% 100%;">form permintaan video</h2>
            </div>
            <form class="px-4" action="" method="POST">
                <div class="mb-3">
                    <label for="judulProyek" class="form-label">judul proyek</label>
                    <input type="text" class="form-control form-rounded-0" name="judulProyek" id="judulProyek" required>
                    <small class="text-danger">
                        <?= form_error('judulProyek') ?>
                    </small>
                </div>
                <div class="mb-3">
                    <label for="jenisProdukInstansi" class="form-label">jenis produk / jasa instansi</label>
                    <input type="text" class="form-control form-rounded-0" name="jenisProdukInstansi" id="jenisProdukInstansi" required>
                    <small class="text-danger">
                        <?= form_error('jenisProdukInstansi') ?>
                    </small>
                </div>
                <div class="mb-3">
                    <label for="merk" class="form-label">merk / brand</label>
                    <input type="text" class="form-control form-rounded-0" name="merk" id="merk" required>
                    <small class="text-danger">
                        <?= form_error('merk') ?>
                    </small>
                </div>
                <div class="mb-3">
                    <label for="usia" class="form-label d-block">target usia</label>
                    <div class="ms-3">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="usia" id="usiaRadio17" value="17-25 tahun" checked>
                            <label class="form-check-label" for="usiaRadio17">17-25 tahun</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="usia" id="usiaRadio26" value="26-40 tahun">
                            <label class="form-check-label" for="usiaRadio26">26-40 tahun</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="usia" id="usiaRadio41" value="41-55 tahun">
                            <label class="form-check-label" for="usiaRadio41">41-55 tahun</label>
                        </div>
                    </div>
                </div>
                <div class="mb-3">
                    <label for="lokasi" class="form-label">target lokasi geografis</label>
                    <input type="text" class="form-control form-rounded-0" name="lokasi" id="lokasi" required>
                    <small class="text-danger">
                        <?= form_error('lokasi') ?>
                    </small>
                </div>
                <div class="mb-3">
                    <label for="jenisKelamin" class="form-label d-block">target jenis kelamin</label>
                    <div class="ms-3">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="jenisKelamin" id="jenisKelaminRadioLK" value="laki-laki" checked>
                            <label class="form-check-label" for="jenisKelaminRadioLK">laki-laki</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="jenisKelamin" id="jenisKelaminRadioPR" value="perempuan">
                            <label class="form-check-label" for="jenisKelaminRadioPR">perempuan</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="jenisKelamin" id="jenisKelaminRadioLKPR" value="laki-laki dan perempuan">
                            <label class="form-check-label" for="jenisKelaminRadioLKPR">laki-laki dan perempuan</label>
                        </div>
                    </div>
                </div>
                <div class="mb-3">
                    <label for="tone" class="form-label">tone warna video</label>
                    <input type="text" class="form-control form-rounded-0" name="tone" id="tone" required>
                    <small class="text-danger">
                        <?= form_error('tone') ?>
                    </small>
                </div>
                <div class="mb-3">
                    <label for="pesan" class="form-label">pesan yang ingin disampaikan / Tagline, dll</label>
                    <input type="text" class="form-control form-rounded-0" name="pesan" id="pesan" required>
                    <small class="text-danger">
                        <?= form_error('pesan') ?>
                    </small>
                </div>
                <div class="mb-3">
                    <label for="model" class="form-label d-block">tambahan model / talent (jika perlu)</label>
                    <div class="ms-3">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="model" id="modelRadioPolaris" value="model dari POLARIS">
                            <label class="form-check-label" for="modelRadioPolaris">model dari POLARIS</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="model" id="modelRadioSendiri" value="model sendiri">
                            <label class="form-check-label" for="modelRadioSendiri">model sendiri</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="model" id="modelRadioTanpa" value="tanpa model" checked>
                            <label class="form-check-label" for="modelRadioTanpa">tanpa model</label>
                        </div>
                    </div>
                </div>
                <div class="mb-3">
                    <label for="asset" class="form-label">unggah link aset yang digunakan (Google Drive/Dropbox/dan lain-lain)</label>
                    <input type="text" class="form-control form-rounded-0" name="asset" id="asset" required>
                    <small class="text-danger">
                        <?= form_error('asset') ?>
                    </small>
                </div>
                <div class="mb-3">
                    <label for="catatan" class="form-label">catatan tambahan (opsional)</label>
                    <textarea class="form-control form-rounded-0" name="catatan" id="catatan" rows="3"></textarea>
                    <small class="text-danger">
                        <?= form_error('catatan') ?>
                    </small>
                </div>
                <button type="submit" class="btn form-btn-rounded-0 mt-3" name="submit">simpan</button>
            </form>
        </div>
    </div>
</div>