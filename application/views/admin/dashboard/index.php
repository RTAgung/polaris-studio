<div class="container-fluid">
    <div class="shadow bg-body rounded-15 p-4 mb-4">
        <h5>Client</h5>
        <div class="table-responsive">
            <table class="table table-sm table-hover">
                <thead>
                    <tr>
                        <th>No. </th>
                        <th>Instansi</th>
                        <th>Email</th>
                        <th>Telepon</th>
                        <th class="text-center">Project</th>
                        <th class="text-center">Terdaftar</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $count = $clients['count'];
                    foreach ($clients['data'] as $index => $client) :
                        $count--;
                    ?>
                        <tr>
                            <td class="align-middle"><?= $index + 1 ?>. </td>
                            <td class="align-middle"><?= $client['nama_instansi'] ?></td>
                            <td class="align-middle"><?= $client['email'] ?></td>
                            <td class="align-middle">
                                <?= $client['whatsapp'] ?>
                                <a target="_blank" href="https://wa.me/<?= $client['whatsapp'] ?>" class="btn btn-sm btn-outline-primary border-blue border-1 my-text-blue ms-2 rounded-pill my-text-smaller">
                                    <i class="fab fa-whatsapp my-text-small"></i>
                                    call
                                </a>
                            </td>
                            <td class="align-middle text-center"><?= $client['project'] ?></td>
                            <td class="align-middle text-center">
                                <?php if ($client['is_registered']) : ?>
                                    <span class="material-icons align-middle text-yellow">
                                        check_circle
                                    </span>
                                <?php else : ?>
                                    <span class="material-icons align-middle my-text-red">
                                        error
                                    </span>
                                <?php endif ?>
                            </td>
                            <td class="align-middle">
                                <div class="float-end">
                                    <?php if ($client['is_registered']) : ?>
                                        <a href="<?= base_url('/po-admin/client/' . $client['id_client']) ?>" class="btn btn-primary btn-sm my-bg-blue border-0">
                                            <span class="material-icons align-middle my-text-small">
                                                arrow_forward
                                            </span>
                                        </a>
                                    <?php else : ?>
                                        <button class="btn btn-danger btn-sm my-bg-red border-0" data-bs-toggle="modal" data-bs-target="#passwordModal<?= $client['id_client'] ?>">
                                            <span class="material-icons align-middle my-text-small">
                                                add
                                            </span>
                                            password
                                        </button>
                                        <button class="btn btn-danger btn-sm my-bg-red border-0" data-bs-toggle="modal" data-bs-target="#deleteModal<?= $client['id_client'] ?>">
                                            <span class="material-icons align-middle text-small">
                                                delete_forever
                                            </span>
                                        </button>
                                    <?php endif ?>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
        <?php if ($count > 0) : ?>
            <p class="text-end mb-0"><a class="text-underline text-dark link-hover-underline" href="<?= base_url('/po-admin/client') ?>">show more</a></p>
        <?php endif ?>
    </div>

    <div class="shadow bg-body rounded-15 p-4 mb-4">
        <h5>Current Project</h5>
        <div class="ms-4">
            <h5>Video</h5>
            <div class="table-responsive">
                <table class="table table-sm table-hover">
                    <thead>
                        <tr>
                            <th>No. </th>
                            <th>Judul</th>
                            <th>Paket</th>
                            <th class="text-center">Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $count = $video['count'];
                        foreach ($video['data'] as $index => $item_video) :
                            $count--;
                        ?>
                            <tr>
                                <td class="align-middle"><?= $index + 1 ?>. </td>
                                <td class="align-middle">
                                    <?php if (empty($item_video['judul'])) echo "Judul Project";
                                    else echo $item_video['judul'] ?>
                                </td>
                                <td class="align-middle">Paket <?= $item_video['nama'] ?></td>
                                <td class="align-middle text-center"><?= $item_video['status'] ?></td>
                                <td class="align-middle">
                                    <div class="float-end">
                                        <a target="_blank" href="<?= $item_video['asset'] ?>" class="btn btn-warning btn-sm my-bg-yellow border-0">
                                            <span class="material-icons align-middle my-text-small">
                                                cloud_download
                                            </span>
                                        </a>
                                        <a href="<?= base_url('/po-admin/project/video/' . $item_video['id_order']) ?>" class="btn btn-primary btn-sm my-bg-blue border-0">
                                            <span class="material-icons align-middle my-text-small">
                                                arrow_forward
                                            </span>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
            <?php if ($count > 0) : ?>
                <p class="text-end mb-0"><a class="text-underline text-dark link-hover-underline" href="<?= base_url('/po-admin/project/video') ?>">show more</a></p>
            <?php endif ?>
            <h5>Web</h5>
            <div class="table-responsive">
                <table class="table table-sm table-hover">
                    <thead>
                        <tr>
                            <th>No. </th>
                            <th>Judul</th>
                            <th>Paket</th>
                            <th class="text-center">Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $count = $web['count'];
                        foreach ($web['data'] as $index => $item_web) :
                            $count--;
                        ?>
                            <tr>
                                <td class="align-middle"><?= $index + 1 ?>. </td>
                                <td class="align-middle">
                                    <?php if (empty($item_web['judul'])) echo "Judul Project";
                                    else echo $item_web['judul'] ?>
                                </td>
                                <td class="align-middle">Paket <?= $item_web['nama'] ?></td>
                                <td class="align-middle text-center"><?= $item_web['status'] ?></td>
                                <td class="align-middle">
                                    <div class="float-end">
                                        <a target="_blank" href="<?= $item_web['asset'] ?>" class="btn btn-warning btn-sm my-bg-yellow border-0">
                                            <span class="material-icons align-middle my-text-small">
                                                cloud_download
                                            </span>
                                        </a>
                                        <a href="<?= base_url('/po-admin/project/web/' . $item_web['id_order']) ?>" class="btn btn-primary btn-sm my-bg-blue border-0">
                                            <span class="material-icons align-middle my-text-small">
                                                arrow_forward
                                            </span>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
            <?php if ($count > 0) : ?>
                <p class="text-end mb-0"><a class="text-underline text-dark link-hover-underline" href="<?= base_url('/po-admin/project/web') ?>">show more</a></p>
            <?php endif ?>
        </div>
    </div>

    <div class="shadow bg-body rounded-15 p-4 mb-4">
        <h5>Current Payment</h5>
        <div class="table-responsive">
            <table class="table table-sm table-hover">
                <thead>
                    <tr>
                        <th>No. </th>
                        <th>Invoice</th>
                        <th>Paket</th>
                        <th class="text-center">Term</th>
                        <th>Harga</th>
                        <th class="text-center">Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $count = $payments['count'];
                    foreach ($payments['data'] as $index => $payment) :
                        $count--;
                    ?>
                        <tr>
                            <td class="align-middle"><?= $index + 1 ?>. </td>
                            <td class="align-middle">Invoice <?= $payment['judul'] ?></td>
                            <td class="align-middle">Paket <?= $payment['nama_paket'] ?></td>
                            <td class="align-middle text-center"><?= $payment['term'] ?></td>
                            <td class="align-middle">Rp. <?= number_format($payment['harga_paket'], 0, ".", ".") ?></td>
                            <td class="align-middle text-center"><?= $payment['status'] ?></td>
                            <td class="align-middle">
                                <div class="float-end">
                                    <?php
                                    if ($payment['status'] == "paid" || $payment['status'] == "waiting")
                                        $disabled = "";
                                    else
                                        $disabled = "disabled";
                                    ?>
                                    <button class="btn btn-warning btn-sm my-bg-yellow border-0 <?= $disabled ?>" data-bs-toggle="modal" data-bs-target="#paymentModal<?= $payment['id_invoice'] ?>">
                                        <span class="material-icons align-middle my-text-small">
                                            receipt_long
                                        </span>
                                    </button>
                                    <a href="<?= base_url('/po-admin/payment/' . $payment['id_invoice']) ?>" class="btn btn-primary btn-sm my-bg-blue border-0">
                                        <span class="material-icons align-middle my-text-small">
                                            arrow_forward
                                        </span>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
        <?php if ($count > 0) : ?>
            <p class="text-end mb-0"><a class="text-underline text-dark link-hover-underline" href="<?= base_url('/po-admin/payment') ?>">show more</a></p>
        <?php endif ?>
    </div>
</div>

<!-- Payment Utility Modal -->
<?php foreach ($payments['data'] as $index => $payment) : ?>
    <!-- Modal Payment -->
    <div class="modal fade" id="paymentModal<?= $payment['id_invoice'] ?>" tabindex="-1" aria-labelledby="paymentModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="btn-close float-end" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="text-center">
                        <h5 class="fw-bold m-0 mt-4">Invoice <?= $payment['judul'] ?></h5>
                        <p class="m-0 fw-bold">Paket <?= $payment['nama_paket'] ?></p>
                        <p class="m-0 my-text-small">Term <?= $payment['term'] ?></p>
                    </div>
                    <div class="m-3">
                        <p>harga : <span class="fw-bold">Rp. <?= number_format($payment['harga_paket'], 0, ".", ".") ?></span></p>
                        <div class="row">
                            <div class="col-md-6 mb-2 text-center">
                                <a target="_blank" href="<?= base_url('/asset/file/payment/' . $payment['bukti']) ?>" class="btn btn-primary border-0 my-bg-blue fw-bold" style="width: 210px;">
                                    <span class="material-icons align-middle me-1">
                                        paid
                                    </span>
                                    bukti pembayaran
                                </a>
                            </div>
                            <div class="col-md-6 mb-2 text-center">
                                <?php if ($payment['status'] == 'waiting')
                                    $disabled = "";
                                else
                                    $disabled = "disabled";
                                ?>
                                <button class="btn btn-warning border-0 my-bg-yellow fw-bold <?= $disabled ?>" data-bs-toggle="modal" data-bs-target="#konfirmasiModal<?= $payment['id_invoice'] ?>" style="width: 210px;">
                                    <span class="material-icons align-middle me-1">
                                        check_circle
                                    </span>
                                    konfirmasi
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Konfirmasi Pembayaran -->
    <div class="modal fade" id="konfirmasiModal<?= $payment['id_invoice'] ?>" tabindex="-1" aria-labelledby="konfirmasiModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Konfirmasi Pembayaran</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    apakah anda yakin melakukan konfirmasi pada pembayaran ini?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary border-0 my-bg-blue" data-bs-dismiss="modal">tidak</button>
                    <form action="<?= base_url('/po-admin/payment/' . $payment['id_invoice'] . '/confirm') ?>" method="POST">
                        <button type="submit" class="btn btn-danger border-0 my-bg-red" name="submitPayment">ya</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php endforeach ?>

<!-- Client Utility Modal -->
<?php foreach ($clients['data'] as $client) :  ?>
    <!-- Modal Generate Password -->
    <div class="modal fade" id="passwordModal<?= $client['id_client'] ?>" tabindex="-1" aria-labelledby="passwordModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="btn-close float-end" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="text-center">
                        <h5 class="fw-bold m-0 mt-4">Generate Password</h5>
                        <p class="m-0"><?= $client['nama_instansi'] ?></p>
                    </div>
                    <div class="mt-4 mx-3">
                        <form class="mt-4" action="" method="POST">
                            <input type="text" name="id" value="<?= $client['id_client'] ?>" hidden readonly>
                            <div class="mb-3">
                                <label for="password" class="form-label text-small">Password</label>
                                <div class="row">
                                    <div class="col-8">
                                        <input type="text" class="form-control form-rounded-0" name="password" id="form-password<?= $client['id_client'] ?>" required>
                                    </div>
                                    <div class="col-4 d-grid">
                                        <p class="btn form-btn-rounded-0 m-0" id="buttonGenerate" data-bs-target="form-password<?= $client['id_client'] ?>">generate</p>
                                    </div>
                                </div>
                                <small class="text-danger">
                                    <?= form_error('password') ?>
                                </small>
                            </div>
                            <div class="d-grid gap-2 col-2 mx-auto">
                                <button type="submit" class="btn form-btn-rounded-0 mt-2 mb-4" name="submit">simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Delete Client -->
    <div class="modal fade" id="deleteModal<?= $client['id_client'] ?>" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete Client</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    apakah anda yakin ingin menghapus akun <span class="fw-bold"><?= $client['nama_instansi'] ?></span> selamanya?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary border-0 my-bg-blue" data-bs-dismiss="modal">tidak</button>
                    <form action="<?= base_url('/po-admin/client/' . $client['id_client'] . '/delete') ?>" method="POST">
                        <button type="submit" class="btn btn-danger border-0 my-bg-red" name="submitDelete">ya</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php endforeach ?>