<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">

    <!-- Internal CSS -->
    <link rel="stylesheet" href="<?= base_url('asset/css/style.css') ?>">
    <link rel="stylesheet" href="<?= base_url('asset/css/admin-style.css') ?>">
    <link rel="stylesheet" href="<?= base_url('asset/css/text.css') ?>">

    <title>Login</title>

    <link rel="icon" href="<?= base_url('/asset/image/general/polaris_logo_mini.png') ?>" type="image/x-icon">
</head>

<body class="my-bg-darker">

    <div class="container py-5 justify-content-center">

        <img src="<?= base_url('asset/image/general/logo_putih.png') ?>" class="mx-auto d-block" width="200px">

        <div class="container py-5">
            <div class="row justify-content-center">
                <div class="col-lg-5 my-5 py-5 px-4 my-bg-black text-white">
                    <h2 class="text-center mb-4">login</h2>
                    <form class="px-5" action="" method="POST">
                        <div class="mb-3">
                            <label for="email" class="form-label">email</label>
                            <input type="email" class="form-control form-rounded-0 bg-white" name="email" id="email" value="<?= set_value('email') ?>" required>
                            <small class="text-danger">
                                <?= form_error('email') ?>
                            </small>
                            <?= $this->session->flashdata('msg_email') ?>
                        </div>
                        <div class="mb-3">
                            <label for="password" class="form-label">password</label>
                            <input type="password" class="form-control form-rounded-0 bg-white" name="password" id="password" required>
                            <small class="text-danger">
                                <?= form_error('password') ?>
                            </small>
                            <?= $this->session->flashdata('msg_password') ?>
                        </div>
                        <div class="d-grid gap-2 col-3 mx-auto">
                            <button type="submit" class="btn form-btn-rounded-0 bg-white my-4">login</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>

</body>

</html>