<div class="container-fluid">
    <h4>Packets</h4>

    <div class="shadow bg-body rounded-15 p-4 mb-4">

        <div class="mb-4">
            <a href="#" class="text-dark link-hover-underline" data-bs-toggle="modal" data-bs-target="#addPacketModal">
                <span class="material-icons align-middle text-green fs-2">
                    add_circle
                </span>
            </a>

            <div class="table-responsive">
                <table class="table table-sm table-hover">
                    <thead>
                        <tr>
                            <th>No. </th>
                            <th>Kode Paket</th>
                            <th>Nama Paket</th>
                            <th>Jenis</th>
                            <th>Harga Awal</th>
                            <th>Harga</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($packet as $index => $value) {
                        ?>
                            <tr>
                                <td class="align-middle"><?= $index + 1 ?></td>
                                <td class="align-middle"><?= $value['kode'] ?></td>
                                <td class="align-middle"><?= $value['nama'] ?></td>
                                <td class="align-middle"><?= $value['jenis'] ?></td>
                                <td class="align-middle">Rp. <?= number_format($value['harga_awal'], 0, ".", ".") ?></td>
                                <td class="align-middle">Rp. <?= number_format($value['harga'], 0, ".", ".") ?></td>
                                <td class="align-middle">
                                    <div class="float-end">
                                        <a class="btn btn-primary btn-sm my-bg-blue" data-bs-toggle="modal" data-bs-target="#editPacketModal<?= $value['id_packet'] ?>">
                                            <span class="material-icons align-middle my-text-small">
                                                edit
                                            </span>
                                        </a>
                                        <a class="btn btn-danger btn-sm" data-bs-toggle="modal" data-bs-target="#deleteModal<?= $value['id_packet'] ?>">
                                            <span class="material-icons align-middle my-text-small">
                                                delete
                                            </span>
                                        </a>
                                        <a class="btn btn-warning btn-sm" data-bs-toggle="modal" data-bs-target="#detailPacketModal<?= $value['id_packet'] ?>">
                                            <span class="material-icons align-middle my-text-small">
                                                arrow_forward
                                            </span>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>








<?php
foreach ($packet as $index => $value) {
?>
    <!-- EDIT -->
    <div class="modal fade" id="editPacketModal<?= $value['id_packet'] ?>" tabindex="-1" aria-labelledby="editPacketModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="btn-close float-end" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="text-center">
                        <h5 class="fw-bold m-0 mt-4">Edit Packet</h5>
                        <p class="m-0 fw-bold"><?= $value['nama'] ?></p>
                    </div>

                    <div class="m-3">
                        <form action="<?= base_url('/po-admin/packet/edit/' . $value['id_packet']) ?>" method="POST">
                            <div class="mb-3">
                                <label for="lampiran" class="form-label fw-normal">kode</label>
                                <input type="text" class="form-control form-rounded-0" value="<?= $value['kode'] ?>" name="kode" id="kode" required>
                            </div>
                            <div class="mb-3">
                                <label for="lampiran" class="form-label fw-normal">nama</label>
                                <input type="text" class="form-control form-rounded-0" value="<?= $value['nama'] ?>" name="nama" id="nama" required>
                                <small class="text-secondary">
                                    *minimum 5 huruf, maksimum 17 huruf
                                </small>
                            </div>
                            <div class="mb-3">
                                <label for="lampiran" class="form-label fw-normal">jenis</label>
                                <select class="form-select" name="jenis" id="jenis" aria-label="Default select example">
                                    <option value="web" <?php if ($value['jenis'] == "web") echo "selected" ?>>Web</option>
                                    <option value="video" <?php if ($value['jenis'] == "video") echo "selected" ?>>Video</option>
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="lampiran" class="form-label fw-normal">harga awal</label>
                                <input type="number" class="form-control form-rounded-0" value="<?= $value['harga_awal'] ?>" name="harga_awal" id="harga" required>
                            </div>
                            <div class="mb-3">
                                <label for="lampiran" class="form-label fw-normal">harga</label>
                                <input type="number" class="form-control form-rounded-0" value="<?= $value['harga'] ?>" name="harga" id="harga" required>
                            </div>
                            <div class="mb-3">
                                <label for="lampiran" class="form-label fw-normal">deskripsi</label>
                                <textarea type="text" class="form-control form-rounded-0" name="deskripsi" id="deskripsi" required><?= $value['deskripsi'] ?></textarea>
                            </div>
                            <div class="mb-3">
                                <label for="lampiran" class="form-label fw-normal">detail</label>
                                <textarea type="text" class="form-control form-rounded-0" name="detail" id="detail" required><?= $value['detail'] ?></textarea>
                            </div>
                            <button type="submit" class="btn form-btn-rounded-0 mt-2" name="submit">simpan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- DETAIL -->
    <div class="modal fade" id="detailPacketModal<?= $value['id_packet'] ?>" tabindex="-1" aria-labelledby="detailPacketModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="btn-close float-end" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="text-center">
                        <h5 class="fw-bold m-0 mt-4">Detail Packet</h5>
                        <p class="m-0 fw-bold"><?= $value['nama'] ?></p>
                    </div>

                    <div class="row m-3">
                        <div class="col-md-5">kode</div>
                        <div class="col-md"><?= $value['kode'] ?></div>
                    </div>

                    <div class="row m-3">
                        <div class="col-md-5">jenis</div>
                        <div class="col-md"><?= $value['jenis'] ?></div>
                    </div>

                    <div class="row m-3">
                        <div class="col-md-5">harga awal</div>
                        <div class="col-md">Rp. <?= number_format($value['harga_awal'], 0, ".", ".") ?></div>
                    </div>

                    <div class="row m-3">
                        <div class="col-md-5">harga</div>
                        <div class="col-md">Rp. <?= number_format($value['harga'], 0, ".", ".") ?></div>
                    </div>

                    <div class="row m-3">
                        <div class="col-md-5">deskripsi</div>
                        <div class="col-md"><?= nl2br($value['deskripsi']) ?></div>
                    </div>

                    <div class="row m-3">
                        <div class="col-md-5">detail</div>
                        <div class="col-md"><?= nl2br($value['detail']) ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Delete packet -->
    <div class="modal fade" id="deleteModal<?= $value['id_packet'] ?>" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete Packet</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    apakah anda yakin ingin menghapus akun <span class="fw-bold"><?= $value['nama'] ?></span> selamanya?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary border-0 my-bg-blue" data-bs-dismiss="modal">tidak</button>
                    <form action="<?= base_url('/po-admin/packet/delete/' . $value['id_packet']) ?>" method="POST">
                        <button type="submit" class="btn btn-danger border-0 my-bg-red" name="submit">ya</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php
}
?>

<!-- ADD -->
<div class="modal fade" id="addPacketModal" tabindex="-1" aria-labelledby="addPacketModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="btn-close float-end" data-bs-dismiss="modal" aria-label="Close"></button>
                <div class="text-center">
                    <h5 class="fw-bold m-0 mt-4">Add Packet</h5>
                    <p class="m-0 fw-bold"></p>
                </div>

                <div class="m-3">
                    <form action="<?= base_url('/po-admin/packet/new') ?>" method="POST">
                        <div class="mb-3">
                            <label for="lampiran" class="form-label fw-normal">kode</label>
                            <input type="text" class="form-control form-rounded-0" name="kode" id="kode" required>
                        </div>
                        <div class="mb-3">
                            <label for="lampiran" class="form-label fw-normal">nama</label>
                            <input type="text" class="form-control form-rounded-0" name="nama" id="nama" required>
                            <small class="text-secondary">
                                *minimum 5 huruf, maksimum 17 huruf
                            </small>
                        </div>
                        <div class="mb-3">
                            <label for="lampiran" class="form-label fw-normal">jenis</label>
                            <select class="form-select" name="jenis" id="jenis" aria-label="Default select example">
                                <option value="web" selected>Web</option>
                                <option value="video">Video</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="lampiran" class="form-label fw-normal">harga awal</label>
                            <input type="number" class="form-control form-rounded-0" name="harga_awal" id="harga_awal" value="0" min="0">
                        </div>
                        <div class="mb-3">
                            <label for="lampiran" class="form-label fw-normal">harga</label>
                            <input type="number" class="form-control form-rounded-0" name="harga" id="harga" min="0" required>
                        </div>
                        <div class="mb-3">
                            <label for="lampiran" class="form-label fw-normal">deskripsi</label>
                            <textarea type="text" class="form-control form-rounded-0" name="deskripsi" id="deskripsi" required></textarea>
                        </div>
                        <div class="mb-3">
                            <label for="lampiran" class="form-label fw-normal">detail</label>
                            <textarea type="text" class="form-control form-rounded-0" name="detail" id="detail" required></textarea>
                        </div>
                        <button type="submit" class="btn form-btn-rounded-0 mt-2" name="submit">simpan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>