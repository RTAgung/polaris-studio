<?php if (empty($detail['judul'])) $judul = "Judul Project";
else $judul = $detail['judul'] ?>

<div class="container mb-5">
    <div class="row justify-content-center">
        <div class="col-lg-7 col-12">
            <div class="text-center mb-4">
                <h4 class="mb-1"><?= $judul ?></h4>
                <p class="mb-1 fs-5"><?= $project['nama_instansi'] ?></p>
            </div>
            <form class="px-4" action="" method="POST">
                <div class="mb-3">
                    <label for="judulProyek" class="form-label">judul proyek</label>
                    <input type="text" class="form-control form-rounded-0" name="judulProyek" id="judulProyek" value="<?= set_value('judulProyek', $judul) ?>" required>
                    <small class="text-danger">
                        <?= form_error('judulProyek') ?>
                    </small>
                </div>
                <div class="mb-3">
                    <label for="jenisUsahaInstansi" class="form-label">jenis usaha instansi</label>
                    <input type="text" class="form-control form-rounded-0" name="jenisUsahaInstansi" id="jenisUsahaInstansi" value="<?= set_value('jenisUsahaInstansi', $web['jenis_usaha']) ?>" required>
                    <small class="text-danger">
                        <?= form_error('jenisUsahaInstansi') ?>
                    </small>
                </div>
                <div class="mb-3">
                    <label class="form-label">tujuan website</label>
                    <div class="ms-3">
                        <?php $tujuan = explode(",", $web['tujuan']); ?>
                        <div class="form-check">
                            <?php
                            $tujuanProfile = "";
                            if (trim(strtolower($tujuan[0])) == 'profile') {
                                $tujuanProfile = 'checked';
                                array_shift($tujuan);
                            }
                            ?>
                            <input class="form-check-input" name="tujuanProfile" type="checkbox" id="tujuanCheckProfile" value="profile" <?= $tujuanProfile ?>>
                            <label class="form-check-label" for="tujuanCheckProfile">
                                profile
                            </label>
                        </div>
                        <div class="form-check">
                            <?php
                            $tujuanTokoOnline = "";
                            if (trim(strtolower($tujuan[0])) == 'toko online') {
                                $tujuanTokoOnline = 'checked';
                                array_shift($tujuan);
                            }
                            ?>
                            <input class="form-check-input" name="tujuanTokoOnline" type="checkbox" id="tujuanCheckTokoOnline" value="toko online" <?= $tujuanTokoOnline ?>>
                            <label class="form-check-label" for="tujuanCheckTokoOnline">
                                toko online
                            </label>
                        </div>
                        <div class="form-check">
                            <?php
                            $tujuanUpdateInfo = "";
                            if (trim(strtolower($tujuan[0])) == 'update info') {
                                $tujuanUpdateInfo = 'checked';
                                array_shift($tujuan);
                            }
                            ?>
                            <input class="form-check-input" name="tujuanUpdateInfo" type="checkbox" id="tujuanCheckUpdateInfo" value="update info" <?= $tujuanUpdateInfo ?>>
                            <label class="form-check-label" for="tujuanCheckUpdateInfo">
                                update info
                            </label>
                        </div>
                        <div class="form-check">
                            <div class="row">
                                <?php
                                $tujuanOther = "";
                                if (!empty($tujuan[0])) {
                                    $tujuanOther = 'checked';
                                }
                                ?>
                                <div class="col-auto">
                                    <input class="form-check-input" name="tujuanOther" type="checkbox" id="tujuanCheckOther" value="1" <?= $tujuanOther ?>>
                                    <label class="form-check-label" for="tujuanCheckOther">
                                        other
                                    </label>
                                </div>
                                <div class="col ms-4">
                                    <input type="text" class="form-control form-control-sm form-rounded-0" name="tujuanTextOther" id="tujuanCheckTextOther" value="<?= trim(implode(",", $tujuan)) ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mb-3">
                    <label for="menuWebsite" class="form-label">menu website</label>
                    <div class="ms-3">
                        <?php $menu = explode(",", $web['menu']); ?>
                        <div class="form-check">
                            <?php
                            $menuAbout = "";
                            if (trim(strtolower($menu[0])) == 'about us/profile') {
                                $menuAbout = 'checked';
                                array_shift($menu);
                            }
                            ?>
                            <input class="form-check-input" name="menuAbout" type="checkbox" value="about us/profile" id="menuCheckAbout" <?= $menuAbout ?>>
                            <label class="form-check-label" for="menuCheckAbout">
                                about us / profile
                            </label>
                        </div>
                        <div class="form-check">
                            <?php
                            $menuProduct = "";
                            if (trim(strtolower($menu[0])) == 'product/services') {
                                $menuProduct = 'checked';
                                array_shift($menu);
                            }
                            ?>
                            <input class="form-check-input" name="menuProduct" type="checkbox" value="product/services" id="menuCheckProduct" <?= $menuProduct ?>>
                            <label class="form-check-label" for="menuCheckProduct">
                                product / services
                            </label>
                        </div>
                        <div class="form-check">
                            <?php
                            $menuContact = "";
                            if (trim(strtolower($menu[0])) == 'contact us') {
                                $menuContact = 'checked';
                                array_shift($menu);
                            }
                            ?>
                            <input class="form-check-input" name="menuContact" type="checkbox" value="contact us" id="menuCheckContact" <?= $menuContact ?>>
                            <label class="form-check-label" for="menuCheckContact">
                                contact us
                            </label>
                        </div>
                        <div class="form-check">
                            <?php
                            $menuShop = "";
                            if (trim(strtolower($menu[0])) == 'shop') {
                                $menuShop = 'checked';
                                array_shift($menu);
                            }
                            ?>
                            <input class="form-check-input" name="menuShop" type="checkbox" value="shop" id="menuCheckShop" <?= $menuShop ?>>
                            <label class="form-check-label" for="menuCheckShop">
                                shop
                            </label>
                        </div>
                        <div class="form-check">
                            <div class="row">
                                <?php
                                $menuOther = "";
                                if (!empty($menu[0])) {
                                    $menuOther = 'checked';
                                }
                                ?>
                                <div class="col-auto">
                                    <input class="form-check-input" name="menuOther" type="checkbox" value="1" id="menuCheckOther" <?= $menuOther ?>>
                                    <label class="form-check-label" for="menuCheckOther">
                                        other
                                    </label>
                                </div>
                                <div class="col ms-4">
                                    <input type="text" class="form-control form-control-sm form-rounded-0" name="menuTextOther" id="menuCheckTextOther" value="<?= trim(implode(",", $menu)) ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mb-3">
                    <label for="domain" class="form-label d-block">sudah memiliki domain?</label>
                    <div class="ms-3">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="domain" id="domainRadioYa" value="1" <?php if ($web['punya_domain']) echo 'checked' ?>>
                            <label class="form-check-label" for="domainRadioYa">ya</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="domain" id="domainRadioTidak" value="0" <?php if (!$web['punya_domain']) echo 'checked' ?>>
                            <label class="form-check-label" for="domainRadioTidak">tidak</label>
                        </div>
                    </div>
                    <small class="text-tiny ms-3">*biaya paket diluar biaya domain dan hosting</small>
                </div>
                <div class="mb-3">
                    <label for="hosting" class="form-label d-block">sudah memiliki hosting?</label>
                    <div class="ms-3">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="hosting" id="hostingRadioYa" value="1" <?php if ($web['punya_hosting']) echo 'checked' ?>>
                            <label class="form-check-label" for="hostingRadioYa">ya</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="hosting" id="hostingRadioTidak" value="0" <?php if (!$web['punya_hosting']) echo 'checked' ?>>
                            <label class="form-check-label" for="hostingRadioTidak">tidak</label>
                        </div>
                    </div>
                    <small class="text-tiny ms-3">*biaya paket diluar biaya domain dan hosting</small>
                </div>
                <div class="mb-3">
                    <label for="asset" class="form-label">unggah link asset yang digunakan (Google Drive/Dropbox/dan lain-lain)</label>
                    <input type="text" class="form-control form-rounded-0" name="asset" id="asset" value="<?= set_value('asset', $detail['asset']) ?>" required>
                    <small class="text-danger">
                        <?= form_error('asset') ?>
                    </small>
                </div>
                <div class="mb-3">
                    <label for="catatan" class="form-label">catatan tambahan (opsional)</label>
                    <textarea class="form-control form-rounded-0" name="catatan" id="catatan" rows="3"><?= set_value('catatan', $web['catatan']) ?></textarea>
                    <small class="text-danger">
                        <?= form_error('catatan') ?>
                    </small>
                </div>
                <button type="submit" class="btn form-btn-rounded-0 mt-3" name="submit">simpan</button>
            </form>
        </div>
    </div>
</div>