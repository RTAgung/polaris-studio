<?php if (empty($detail['judul'])) $judul = "Judul Project";
else $judul = $detail['judul'] ?>

<div class="container mb-5">
    <div class="row justify-content-center">
        <div class="col-lg-7 col-12">
            <div class="text-center mb-4">
                <h4 class="mb-1"><?= $judul ?></h4>
                <p class="mb-1 fs-5"><?= $project['nama_instansi'] ?></p>
            </div>
            <form class="px-4" action="" method="POST">
                <div class="mb-3">
                    <label for="judulProyek" class="form-label">judul proyek</label>
                    <input type="text" class="form-control form-rounded-0" name="judulProyek" id="judulProyek" value="<?= set_value('judulProyek', $judul) ?>" required>
                    <small class="text-danger">
                        <?= form_error('judulProyek') ?>
                    </small>
                </div>
                <div class="mb-3">
                    <label for="jenisProdukInstansi" class="form-label">jenis produk / jasa instansi</label>
                    <input type="text" class="form-control form-rounded-0" name="jenisProdukInstansi" id="jenisProdukInstansi" value="<?= set_value('jenisProdukInstansi', $video['jenis_produk']) ?>" required>
                    <small class="text-danger">
                        <?= form_error('jenisProdukInstansi') ?>
                    </small>
                </div>
                <div class="mb-3">
                    <label for="merk" class="form-label">merk / brand</label>
                    <input type="text" class="form-control form-rounded-0" name="merk" id="merk" value="<?= set_value('merk', $video['brand']) ?>" required>
                    <small class="text-danger">
                        <?= form_error('merk') ?>
                    </small>
                </div>
                <div class="mb-3">
                    <label for="usia" class="form-label d-block">target usia</label>
                    <div class="ms-3">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="usia" id="usiaRadio17" value="17-25 tahun" <?php if ($video['target_usia'] == '17-25 tahun') echo 'checked' ?>>
                            <label class="form-check-label" for="usiaRadio17">17-25 tahun</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="usia" id="usiaRadio26" value="26-40 tahun" <?php if ($video['target_usia'] == '26-40 tahun') echo 'checked' ?>>
                            <label class="form-check-label" for="usiaRadio26">26-40 tahun</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="usia" id="usiaRadio41" value="41-55 tahun" <?php if ($video['target_usia'] == '41-55 tahun') echo 'checked' ?>>
                            <label class="form-check-label" for="usiaRadio41">41-55 tahun</label>
                        </div>
                    </div>
                </div>
                <div class="mb-3">
                    <label for="lokasi" class="form-label">target lokasi geografis</label>
                    <input type="text" class="form-control form-rounded-0" name="lokasi" id="lokasi" value="<?= set_value('lokasi', $video['target_lokasi_geografis']) ?>" required>
                    <small class="text-danger">
                        <?= form_error('lokasi') ?>
                    </small>
                </div>
                <div class="mb-3">
                    <label for="jenisKelamin" class="form-label d-block">target jenis kelamin</label>
                    <div class="ms-3">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="jenisKelamin" id="jenisKelaminRadioLK" value="laki-laki" <?php if ($video['target_jenis_kelamin'] == 'laki-laki') echo 'checked' ?>>
                            <label class="form-check-label" for="jenisKelaminRadioLK">laki-laki</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="jenisKelamin" id="jenisKelaminRadioPR" value="perempuan" <?php if ($video['target_jenis_kelamin'] == 'perempuan') echo 'checked' ?>>
                            <label class="form-check-label" for="jenisKelaminRadioPR">perempuan</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="jenisKelamin" id="jenisKelaminRadioLKPR" value="laki-laki dan perempuan" <?php if ($video['target_jenis_kelamin'] == 'laki-laki dan perempuan') echo 'checked' ?>>
                            <label class="form-check-label" for="jenisKelaminRadioLKPR">laki-laki dan perempuan</label>
                        </div>
                    </div>
                </div>
                <div class="mb-3">
                    <label for="tone" class="form-label">tone warna video</label>
                    <input type="text" class="form-control form-rounded-0" name="tone" id="tone" value="<?= set_value('tone', $video['tone_warna']) ?>" required>
                    <small class="text-danger">
                        <?= form_error('tone') ?>
                    </small>
                </div>
                <div class="mb-3">
                    <label for="pesan" class="form-label">pesan yang ingin disampaikan / Tagline, dll</label>
                    <input type="text" class="form-control form-rounded-0" name="pesan" id="pesan" value="<?= set_value('pesan', $video['pesan_video']) ?>" required>
                    <small class="text-danger">
                        <?= form_error('pesan') ?>
                    </small>
                </div>
                <div class="mb-3">
                    <label for="model" class="form-label d-block">tambahan model / talent (jika perlu)</label>
                    <div class="ms-3">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="model" id="modelRadioPolaris" value="model dari POLARIS" <?php if ($video['tambahan_model'] == 'model dari POLARIS') echo 'checked' ?>>
                            <label class="form-check-label" for="modelRadioPolaris">model dari POLARIS</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="model" id="modelRadioSendiri" value="model sendiri" <?php if ($video['tambahan_model'] == 'model sendiri') echo 'checked' ?>>
                            <label class="form-check-label" for="modelRadioSendiri">model sendiri</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="model" id="modelRadioTanpa" value="tanpa model" <?php if ($video['tambahan_model'] == 'tanpa model') echo 'checked' ?>>
                            <label class="form-check-label" for="modelRadioTanpa">tanpa model</label>
                        </div>
                    </div>
                </div>
                <div class="mb-3">
                    <label for="asset" class="form-label">unggah link asset yang digunakan (Google Drive/Dropbox/dan lain-lain)</label>
                    <input type="text" class="form-control form-rounded-0" name="asset" id="asset" value="<?= set_value('pesan', $detail['asset']) ?>" required>
                    <small class="text-danger">
                        <?= form_error('asset') ?>
                    </small>
                </div>
                <div class="mb-3">
                    <label for="catatan" class="form-label">catatan tambahan (opsional)</label>
                    <textarea class="form-control form-rounded-0" name="catatan" id="catatan" rows="3"><?= set_value('catatan', $video['catatan']) ?></textarea>
                    <small class="text-danger">
                        <?= form_error('catatan') ?>
                    </small>
                </div>
                <button type="submit" class="btn form-btn-rounded-0 mt-3" name="submit">simpan</button>
            </form>
        </div>
    </div>
</div>