<?php
if ($project['status'] == 'finished' || $project['status'] == 'canceled')
    $statusDisabled = "disabled";
else
    $statusDisabled = "";
?>
<div class="container-fluid">
    <div class="shadow bg-body rounded-15 p-4 mb-4">
        <div class="row justify-content-between">
            <div class="col-sm-5">
                <h4 class="mb-2"><?php if (empty($project['judul'])) echo "Judul Project";
                                    else echo $project['judul'] ?></h4>
                <p class="mb-2 fs-5">Paket <?= $project['nama'] ?></p>
                <p class="mb-2 text-muted">mulai <?= $project['tgl_mulai'] ?></p>
            </div>
            <div class="col-sm-2 text-sm-center">
                <label class="fs-5" style="border-bottom: 2px solid; line-height:35px;"><?= $project['status'] ?></label>
            </div>
            <div class="col-sm-5 text-sm-end">
                <p class="mb-2 fs-5">
                    <span class="float-sm-end"><?= $client['nama_instansi'] ?></span>
                    <a target="_blank" href="https://wa.me/<?= $client['whatsapp'] ?>" class="btn btn-warning my-bg-yellow mx-2 btn-sm border-0">
                        <span class="material-icons align-middle text-small">
                            call
                        </span>
                    </a>
                </p>
                <p class="mb-2"><?= $client['email'] ?></p>
                <?php
                if ($project['status'] != 'pending') :
                    if ($project['status'] == 'finished' || $project['status'] == 'canceled') : ?>
                        <button class="btn btn-sm btn-primary border-0 rounded-pill my-bg-blue fw-bold my-1" data-bs-toggle="modal" data-bs-target="#reopenModal" style="width: 100px;">
                            <i class="material-icons align-middle text-small">refresh</i>
                            re-open
                        </button>
                    <?php else : ?>
                        <button class="btn btn-sm btn-warning border-0 rounded-pill my-bg-yellow fw-bold my-1" data-bs-toggle="modal" data-bs-target="#finishModal" style="width: 80px;">finish</button>
                        <button class="btn btn-sm btn-danger border-0 rounded-pill my-bg-red fw-bold my-1" data-bs-toggle="modal" data-bs-target="#cancelModal" style="width: 80px;">cancel</button>
                <?php
                    endif;
                endif
                ?>
                <button class="btn btn-sm btn-danger border-0 rounded-pill my-bg-red fw-bold my-1" data-bs-toggle="modal" data-bs-target="#deleteModal">
                    <i class="material-icons align-middle text-small">delete_forever</i>
                </button>
            </div>
        </div>
        <p class="mb-0">
            <?php
            if (empty($project['deskripsi'])) echo "tidak ada deskripsi";
            else echo nl2br($project['deskripsi']);
            ?>
        </p>
    </div>
    <?php if ($project['status'] != 'pending') : ?>
        <div class="row justify-content-between mb-4">
            <div class="col-3">
                <a target="_blank" href="<?= $project['asset'] ?>" class="btn btn-primary border-0 my-bg-blue fw-bold">
                    <span class="material-icons align-middle me-1">
                        cloud_download
                    </span>
                    lihat asset
                </a>
            </div>
            <div class="col-3 text-center">
                <button class="btn btn-warning border-0 my-bg-yellow fw-bold" data-bs-toggle="modal" data-bs-target="#invoiceModal" <?= $statusDisabled ?>>
                    <span class="material-icons align-middle me-1">
                        receipt_long
                    </span>
                    kirim invoice
                </button>
                <?= $this->session->flashdata('upload_invoice') ?>
            </div>
            <div class="col-3">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-12">
                        <div class="shadow bg-body rounded-15 p-4 mb-4">
                            <h5>Payment</h5>
                            <div class="table-responsive ">
                                <table class="table table-sm table-hover mb-0">
                                    <thead>
                                        <tr>
                                            <th>No. </th>
                                            <th>Id</th>
                                            <th class="text-center">Term</th>
                                            <th>Harga</th>
                                            <th class="text-center">Status</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($invoice as $index => $i_inv) : ?>
                                            <tr>
                                                <td class="align-middle"><?= $index + 1 ?>. </td>
                                                <td class="align-middle"><?= $i_inv['kode_invoice'] ?></td>
                                                <td class="align-middle text-center"><?= $i_inv['term'] ?></td>
                                                <td class="align-middle">Rp. <?= number_format($i_inv['biaya'], 0, ".", ".") ?></td>
                                                <td class="align-middle text-center"><?= $i_inv['status'] ?></td>
                                                <td class="align-middle">
                                                    <div class="float-end">
                                                        <a href="<?= base_url('/po-admin/payment/' . $i_inv['id_invoice']) ?>" class="btn btn-primary border-0 btn-sm my-bg-blue">
                                                            <span class="material-icons align-middle my-text-small">
                                                                arrow_forward
                                                            </span>
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="shadow bg-body rounded-15 p-4 mb-4">
                            <div class="table-responsive ">
                                <table class="table table-borderless mb-0" id="tableDetailProject">
                                    <tr>
                                        <td width="35%">jenis produk</td>
                                        <td>
                                            <div class="d-flex justify-content-between">
                                                <div>
                                                    <span><?= $video['jenis_produk'] ?></span>
                                                </div>
                                                <div>
                                                    <a href="<?= base_url('/po-admin/project/video/' . $project['id_order'] . '/edit') ?>" class="btn btn-sm btn-warning border-0 my-bg-yellow fw-bold text-dark ms-2 <?= $statusDisabled ?>" role="button" aria-disabled="true">edit</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>merk/brand</td>
                                        <td><?= $video['brand'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>target usia</td>
                                        <td><?= $video['target_usia'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>target lokasi</td>
                                        <td><?= $video['target_lokasi_geografis'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>target jenis kelamin</td>
                                        <td><?= $video['target_jenis_kelamin'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>tone warna</td>
                                        <td><?= $video['tone_warna'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>pesan di video</td>
                                        <td><?= $video['pesan_video'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>tambahan model</td>
                                        <td><?= $video['tambahan_model'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>catatan</td>
                                        <td>
                                            <?php
                                            if (empty($video['catatan'])) echo "tidak ada";
                                            else echo nl2br($video['catatan']);
                                            ?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="shadow bg-body rounded-15 p-4 mb-4">

                    <!-- chat box -->
                    <div class="overflow-auto px-3" style="height: 450px;" id="chat-box">
                        <?php foreach ($chat as $itemChat) : ?>
                            <?php if ($itemChat['pengirim'] == "client") : ?>
                                <div class="shadow-sm px-4 py-2 mb-3 rounded-15 chat-box chat-bg-grey">
                                    <div class="d-block">
                                        <small class="text-smaller"><?= $itemChat['tgl_kirim'] ?></small>
                                    </div>
                                    <p class="m-0">
                                        <?php
                                        $teks = $itemChat['teks'];
                                        $url = '@(http)?(s)?(://)?(([a-zA-Z])([-\w]+\.)+([^\s\.]+[^\s]*)+[^,.\s])@';
                                        $teks = preg_replace($url, '<a href="http$2://$4" target="_blank" title="$0">$0</a>', $teks);
                                        echo nl2br($teks);
                                        ?>
                                    </p>
                                </div>
                            <?php elseif ($itemChat['pengirim'] == "admin") : ?>
                                <div class="row justify-content-end m-0">
                                    <div class="shadow-sm px-4 py-2 mb-3 rounded-15 chat-box chat-bg-green">
                                        <div class="d-block">
                                            <small class="text-smaller"><?= $itemChat['tgl_kirim'] ?></small>
                                            <?php if (!empty($itemChat['label'])) : ?>
                                                &nbsp;<i class="material-icons align-middle text-smaller ms-2 text-red">sell</i>
                                                <small class="text-smaller"><?= $itemChat['label'] ?></small>
                                            <?php endif ?>
                                        </div>
                                        <p class="m-0">
                                            <?php
                                            $teks = $itemChat['teks'];
                                            $url = '@(http)?(s)?(://)?(([a-zA-Z])([-\w]+\.)+([^\s\.]+[^\s]*)+[^,.\s])@';
                                            $teks = preg_replace($url, '<a href="http$2://$4" target="_blank" title="$0">$0</a>', $teks);
                                            echo nl2br($teks);
                                            ?>
                                        </p>
                                    </div>
                                </div>
                            <?php endif ?>
                        <?php endforeach ?>
                    </div>

                    <!-- form chat -->
                    <form class="mt-3" action="" method="POST">
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="input-group">
                                    <input type="text" class="form-control border-red rounded-15" <?= $statusDisabled ?> name="label" id="chat-form" placeholder="label" aria-label="label" aria-describedby="basic-addon">
                                </div>
                            </div>
                            <div class="col-sm-10">
                                <div class="input-group">
                                    <textarea type="text" class="form-control border-red border-end-0 rounded-15" <?= $statusDisabled ?> id="chat-form" name="komentar" placeholder="tulis komentar" aria-label="tulis komentar" aria-describedby="basic-addon" rows="1" required></textarea>
                                    <button type="submit" class="input-group-text border-red bg-transparent border-start-0 rounded-15" <?= $statusDisabled ?> name="submitChat" id="basic-addon">
                                        <i class="material-icons align-middle my-text-red chat-icon">send</i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    <?php endif ?>
</div>

<!-- Modal Kirim Invoice -->
<div class="modal fade" id="invoiceModal" tabindex="-1" aria-labelledby="invoiceModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="btn-close float-end" data-bs-dismiss="modal" aria-label="Close"></button>
                <div class="text-center">
                    <h5 class="fw-bold m-0 mt-4">kirim invoice</h5>
                    <p class="m-0 fw-bold">Invoice <?= $project['judul'] ?></p>
                </div>
                <div class="m-3">
                    <form action="<?= base_url('/po-admin/payment/upload/' . $project['id_order'] . '?f=video') ?>" method="POST" enctype="multipart/form-data">
                        <div class="mb-3">
                            <label for="id" class="form-label fw-normal">nomor</label>
                            <input type="text" class="form-control form-rounded-0" name="id" id="id" value="<?= set_value('id') ?>" required <?= $statusDisabled ?>>
                            <small class="text-danger">
                                <?= form_error('id') ?>
                            </small>
                        </div>
                        <div class="mb-3">
                            <label for="tanggal" class="form-label fw-normal">tanggal</label>
                            <input type="date" class="form-control form-rounded-0" name="tanggal" id="tanggal" value="<?= date("Y-m-d") ?>" required <?= $statusDisabled ?>>
                            <small class="text-danger">
                                <?= form_error('tanggal') ?>
                            </small>
                        </div>
                        <div class="row mb-3">
                            <div class="col-3">
                                <label for="term" class="form-label fw-normal">term</label>
                                <input type="number" min="1" class="form-control form-rounded-0" name="term" id="term" value="<?= set_value('term') ?>" required <?= $statusDisabled ?>>
                                <small class="text-danger">
                                    <?= form_error('term') ?>
                                </small>
                            </div>
                            <div class="col-9">
                                <label for="biaya" class="form-label fw-normal">biaya (Rp.)</label>
                                <input type="number" min="1" class="form-control form-rounded-0" name="biaya" id="biaya" value="<?= set_value('biaya') ?>" required <?= $statusDisabled ?>>
                                <small class="text-danger">
                                    <?= form_error('biaya') ?>
                                </small>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="lampiran" class="form-label fw-normal">unggah lampiran</label>
                            <input type="file" class="form-control form-rounded-0" name="lampiran" id="lampiran" required <?= $statusDisabled ?>>
                            <small class="text-danger">max size : 500 KB, typefile : pdf/png/jpg</small>
                        </div>
                        <button type="submit" class="btn form-btn-rounded-0 mt-2" name="submitInvoice" <?= $statusDisabled ?>>kirim</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Finish Project -->
<div class="modal fade" id="finishModal" tabindex="-1" aria-labelledby="finishModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Project Complete</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                apakah anda yakin ingin menyelesaikan project ini?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary border-0 my-bg-blue" data-bs-dismiss="modal">tidak</button>
                <form action="<?= base_url('/po-admin/project/status/video/' . $project['id_order']) ?>" method="POST">
                    <button type="submit" class="btn btn-danger border-0 my-bg-red" name="submitFinish">ya</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal Cancel Project -->
<div class="modal fade" id="cancelModal" tabindex="-1" aria-labelledby="cancelModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Project Cancel</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                apakah anda yakin ingin membatalkan project ini?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary border-0 my-bg-blue" data-bs-dismiss="modal">tidak</button>
                <form action="<?= base_url('/po-admin/project/status/video/' . $project['id_order']) ?>" method="POST">
                    <button type="submit" class="btn btn-danger border-0 my-bg-red" name="submitCancel">ya</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal Re Open Project -->
<div class="modal fade" id="reopenModal" tabindex="-1" aria-labelledby="reopenModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Project Re-Open</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                apakah anda yakin mengaktifkan kembali project ini?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary border-0 my-bg-blue" data-bs-dismiss="modal">tidak</button>
                <form action="<?= base_url('/po-admin/project/status/video/' . $project['id_order']) ?>" method="POST">
                    <button type="submit" class="btn btn-danger border-0 my-bg-red" name="submitReopen">ya</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal Delete Project -->
<div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Delete Project</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                apakah anda yakin ingin menghapus project ini selamanya?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary border-0 my-bg-blue" data-bs-dismiss="modal">tidak</button>
                <form action="<?= base_url('/po-admin/project/delete/' . $project['id_order'] . '?f=video') ?>" method="POST">
                    <button type="submit" class="btn btn-danger border-0 my-bg-red" name="submitDelete">ya</button>
                </form>
            </div>
        </div>
    </div>
</div>