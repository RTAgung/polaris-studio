<div class="container-fluid">
    <div class="shadow bg-body rounded-15 p-4 mb-4">
        <div class="row justify-content-between">
            <div class="col-lg-5">
                <h4>Video Project</h4>
            </div>
            <div class="col-lg-5">
                <form class="" action="">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control form-control-sm rounded-15 border-1 border-blue" name="search" placeholder="cari disini" aria-label="Recipient's username" aria-describedby="button-addon2" style="border-color: #4D9DCF;">
                        <button class="btn btn-sm btn-primary rounded-15 border-0 my-bg-blue" type="button" id="button-addon2">
                            <i class="material-icons align-middle text-small">search</i>
                            search
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-sm table-hover">
                <thead>
                    <tr>
                        <th>No. </th>
                        <th>Judul</th>
                        <th>Paket</th>
                        <th class="text-center">Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $countC = $countCurrent;
                    foreach ($currentProject as $index => $current) :
                        $countC--;
                    ?>
                        <tr>
                            <td class="align-middle"><?= $index + 1 ?>. </td>
                            <td class="align-middle"><?php if (empty($current['judul'])) echo "Judul Project";
                                                        else echo $current['judul'] ?></td>
                            <td class="align-middle">Paket <?= $current['nama'] ?></td>
                            <td class="align-middle text-center"><?= $current['status'] ?></td>
                            <td class="align-middle">
                                <div class="float-end">
                                    <a target="_blank" href="<?= $current['asset'] ?>" class="btn btn-warning btn-sm my-bg-yellow border-0">
                                        <span class="material-icons align-middle my-text-small">
                                            cloud_download
                                        </span>
                                    </a>
                                    <a href="<?= base_url('/po-admin/project/video/' . $current['id_order']) ?>" class="btn btn-primary btn-sm my-bg-blue border-0">
                                        <span class="material-icons align-middle my-text-small">
                                            arrow_forward
                                        </span>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
        <?php if ($countC > 0) : ?>
            <p class="text-end mb-0"><a class="text-underline text-dark link-hover-underline" href="<?= base_url('/po-admin/project/video?c_m=all') ?>">show all</a></p>
        <?php endif ?>
    </div>
    <div class="shadow bg-body rounded-15 p-4 mb-4">
        <div class="row justify-content-between">
            <div class="col-lg-5">
                <h4>History</h4>
            </div>
            <div class="col-lg-5">
                <form class="" action="">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control form-control-sm rounded-15 border-1 border-blue" name="search" placeholder="cari disini" aria-label="Recipient's username" aria-describedby="button-addon2" style="border-color: #4D9DCF;">
                        <button class="btn btn-sm btn-primary rounded-15 border-0 my-bg-blue" type="button" id="button-addon2">
                            <i class="material-icons align-middle text-small">search</i>
                            search
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-sm table-hover">
                <thead>
                    <tr>
                        <th>No. </th>
                        <th>Judul</th>
                        <th>Paket</th>
                        <th class="text-center">Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $countH = $countHistory;
                    foreach ($historyProject as $index => $history) :
                        $countH--;
                    ?>
                        <tr>
                            <td class="align-middle"><?= $index + 1 ?>. </td>
                            <td class="align-middle"><?= $history['judul'] ?></td>
                            <td class="align-middle">Paket <?= $history['nama'] ?></td>
                            <td class="align-middle text-center"><?= $history['status'] ?></td>
                            <td class="align-middle">
                                <div class="float-end">
                                    <a href="<?= $history['asset'] ?>" class="btn btn-warning btn-sm my-bg-yellow border-0">
                                        <span class="material-icons align-middle my-text-small">
                                            cloud_download
                                        </span>
                                    </a>
                                    <a href="<?= base_url('/po-admin/project/video/' . $history['id_order']) ?>" class="btn btn-primary btn-sm my-bg-blue border-0">
                                        <span class="material-icons align-middle my-text-small">
                                            arrow_forward
                                        </span>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
        <?php if ($countH > 0) : ?>
            <p class="text-end mb-0"><a class="text-underline text-dark link-hover-underline" href="<?= base_url('/po-admin/project/video?h_m=all') ?>">show all</a></p>
        <?php endif ?>
    </div>
</div>