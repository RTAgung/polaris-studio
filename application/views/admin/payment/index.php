<div class="container-fluid">
    <div class="shadow bg-body rounded-15 p-4 mb-4">
        <div class="row justify-content-between">
            <div class="col-lg-5">
                <h4>Payment</h4>
            </div>
            <div class="col-lg-5">
                <form class="" action="">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control form-control-sm rounded-15 border-1 border-blue" name="search" placeholder="cari disini" aria-label="Recipient's username" aria-describedby="button-addon2" style="border-color: #4D9DCF;">
                        <button class="btn btn-sm btn-primary rounded-15 border-0 my-bg-blue" type="button" id="button-addon2">
                            <i class="material-icons align-middle text-small">search</i>
                            search
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-sm table-hover">
                <thead>
                    <tr>
                        <th>No. </th>
                        <th>Invoice</th>
                        <th>Paket</th>
                        <th class="text-center">Term</th>
                        <th>Harga</th>
                        <th class="text-center">Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $countC = $countCurrent;
                    foreach ($currentInvoice as $index => $current) :
                        $countC--;
                    ?>
                        <tr>
                            <td class="align-middle"><?= $index + 1 ?>. </td>
                            <td class="align-middle">Invoice <?= $current['judul'] ?></td>
                            <td class="align-middle">Paket <?= $current['nama_paket'] ?></td>
                            <td class="align-middle text-center"><?= $current['term'] ?></td>
                            <td class="align-middle">Rp. <?= number_format($current['harga_paket'], 0, ".", ".") ?></td>
                            <td class="align-middle text-center"><?= $current['status'] ?></td>
                            <td class="align-middle">
                                <div class="float-end">
                                    <?php
                                    if ($current['status'] == "paid" || $current['status'] == "waiting")
                                        $disabled = "";
                                    else
                                        $disabled = "disabled";
                                    ?>
                                    <button class="btn btn-warning btn-sm my-bg-yellow border-0 <?= $disabled ?>" data-bs-toggle="modal" data-bs-target="#paymentModal<?= $current['id_invoice'] ?>">
                                        <span class="material-icons align-middle my-text-small">
                                            receipt_long
                                        </span>
                                    </button>
                                    <a href="<?= base_url('/po-admin/payment/' . $current['id_invoice']) ?>" class="btn btn-primary btn-sm my-bg-blue border-0">
                                        <span class="material-icons align-middle my-text-small">
                                            arrow_forward
                                        </span>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
        <?php if ($countC > 0) : ?>
            <p class="text-end mb-0"><a class="text-underline text-dark link-hover-underline" href="<?= base_url('/po-admin/payment?c_m=all') ?>">show all</a></p>
        <?php endif ?>
    </div>
    <div class="shadow bg-body rounded-15 p-4 mb-4">
        <div class="row justify-content-between">
            <div class="col-lg-5">
                <h4>History</h4>
            </div>
            <div class="col-lg-5">
                <form class="" action="">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control form-control-sm rounded-15 border-1 border-blue" name="search" placeholder="cari disini" aria-label="Recipient's username" aria-describedby="button-addon2" style="border-color: #4D9DCF;">
                        <button class="btn btn-sm btn-primary rounded-15 border-0 my-bg-blue" type="button" id="button-addon2">
                            <i class="material-icons align-middle text-small">search</i>
                            search
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-sm table-hover">
                <thead>
                    <tr>
                        <th>No. </th>
                        <th>Invoice</th>
                        <th>Paket</th>
                        <th class="text-center">Term</th>
                        <th>Harga</th>
                        <th class="text-center">Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $countH = $countHistory;
                    foreach ($historyInvoice as $index => $history) :
                        $countH--;
                    ?>
                        <tr>
                            <td class="align-middle"><?= $index + 1 ?>. </td>
                            <td class="align-middle">Invoice <?= $history['judul'] ?></td>
                            <td class="align-middle">Paket <?= $history['nama_paket'] ?></td>
                            <td class="align-middle text-center"><?= $history['term'] ?></td>
                            <td class="align-middle">Rp. <?= number_format($history['harga_paket'], 0, ".", ".") ?></td>
                            <td class="align-middle text-center"><?= $history['status'] ?></td>
                            <td class="align-middle">
                                <div class="float-end">
                                    <?php
                                    if ($history['status'] == "paid" || $history['status'] == "waiting")
                                        $disabled = "";
                                    else
                                        $disabled = "disabled";
                                    ?>
                                    <button class="btn btn-warning btn-sm my-bg-yellow border-0 <?= $disabled ?>" data-bs-toggle="modal" data-bs-target="#paymentModal<?= $history['id_invoice'] ?>">
                                        <span class="material-icons align-middle my-text-small">
                                            receipt_long
                                        </span>
                                    </button>
                                    <a href="<?= base_url('/po-admin/payment/' . $history['id_invoice']) ?>" class="btn btn-primary btn-sm my-bg-blue border-0">
                                        <span class="material-icons align-middle my-text-small">
                                            arrow_forward
                                        </span>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
        <?php if ($countH > 0) : ?>
            <p class="text-end mb-0"><a class="text-underline text-dark link-hover-underline" href="<?= base_url('/po-admin/payment?h_m=all') ?>">show all</a></p>
        <?php endif ?>
    </div>
</div>

<?php foreach ($currentInvoice as $index => $current) : ?>
    <!-- Modal Payment -->
    <div class="modal fade" id="paymentModal<?= $current['id_invoice'] ?>" tabindex="-1" aria-labelledby="paymentModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="btn-close float-end" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="text-center">
                        <h5 class="fw-bold m-0 mt-4">Invoice <?= $current['judul'] ?></h5>
                        <p class="m-0 fw-bold">Paket <?= $current['nama_paket'] ?></p>
                        <p class="m-0 my-text-small">Term <?= $current['term'] ?></p>
                    </div>
                    <div class="m-3">
                        <p>harga : <span class="fw-bold">Rp. <?= number_format($current['harga_paket'], 0, ".", ".") ?></span></p>
                        <div class="row">
                            <div class="col-md-6 mb-2 text-center">
                                <a target="_blank" href="<?= base_url('/asset/file/payment/' . $current['bukti']) ?>" class="btn btn-primary border-0 my-bg-blue fw-bold" style="width: 210px;">
                                    <span class="material-icons align-middle me-1">
                                        paid
                                    </span>
                                    bukti pembayaran
                                </a>
                            </div>
                            <div class="col-md-6 mb-2 text-center">
                                <?php if ($current['status'] == 'waiting')
                                    $disabled = "";
                                else
                                    $disabled = "disabled";
                                ?>
                                <button class="btn btn-warning border-0 my-bg-yellow fw-bold <?= $disabled ?>" data-bs-toggle="modal" data-bs-target="#konfirmasiModal<?= $current['id_invoice'] ?>" style="width: 210px;">
                                    <span class="material-icons align-middle me-1">
                                        check_circle
                                    </span>
                                    konfirmasi
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Konfirmasi Pembayaran -->
    <div class="modal fade" id="konfirmasiModal<?= $current['id_invoice'] ?>" tabindex="-1" aria-labelledby="konfirmasiModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Konfirmasi Pembayaran</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    apakah anda yakin melakukan konfirmasi pada pembayaran ini?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary border-0 my-bg-blue" data-bs-dismiss="modal">tidak</button>
                    <form action="<?= base_url('/po-admin/payment/' . $current['id_invoice'] . '/confirm') ?>" method="POST">
                        <button type="submit" class="btn btn-danger border-0 my-bg-red" name="submitPayment">ya</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php endforeach ?>

<?php foreach ($historyInvoice as $index => $history) : ?>
    <!-- Modal Payment -->
    <div class="modal fade" id="paymentModal<?= $history['id_invoice'] ?>" tabindex="-1" aria-labelledby="paymentModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="btn-close float-end" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="text-center">
                        <h5 class="fw-bold m-0 mt-4">Invoice <?= $history['judul'] ?></h5>
                        <p class="m-0 fw-bold">Paket <?= $history['nama_paket'] ?></p>
                        <p class="m-0 my-text-small">Term <?= $history['term'] ?></p>
                    </div>
                    <div class="m-3">
                        <p>harga : <span class="fw-bold">Rp. <?= number_format($history['harga_paket'], 0, ".", ".") ?></span></p>
                        <div class="row">
                            <div class="col-md-6 mb-2 text-center">
                                <a target="_blank" href="<?= base_url('/asset/file/payment/' . $history['bukti']) ?>" class="btn btn-primary border-0 my-bg-blue fw-bold" style="width: 210px;">
                                    <span class="material-icons align-middle me-1">
                                        paid
                                    </span>
                                    bukti pembayaran
                                </a>
                            </div>
                            <div class="col-md-6 mb-2 text-center">
                                <?php if ($history['status'] == 'waiting')
                                    $disabled = "";
                                else
                                    $disabled = "disabled";
                                ?>
                                <button class="btn btn-warning border-0 my-bg-yellow fw-bold <?= $disabled ?>" data-bs-toggle="modal" data-bs-target="#konfirmasiModal<?= $history['id_invoice'] ?>" style="width: 210px;">
                                    <span class="material-icons align-middle me-1">
                                        check_circle
                                    </span>
                                    konfirmasi
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Konfirmasi Pembayaran -->
    <div class="modal fade" id="konfirmasiModal<?= $history['id_invoice'] ?>" tabindex="-1" aria-labelledby="konfirmasiModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Konfirmasi Pembayaran</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    apakah anda yakin melakukan konfirmasi pada pembayaran ini?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary border-0 my-bg-blue" data-bs-dismiss="modal">tidak</button>
                    <form action="<?= base_url('/po-admin/payment/' . $history['id_invoice'] . '/confirm') ?>" method="POST">
                        <button type="submit" class="btn btn-danger border-0 my-bg-red" name="submitPayment">ya</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php endforeach ?>