<div class="container">
    <div class="text-center">
        <h3 class="my-1">Invoice <?= $project['judul'] ?></h3>
        <p class="fs-5 my-1">ID <?= $invoice['kode_invoice'] ?></p>
        <p class="text-mute my-1"><?= $invoice['tgl_keluar'] ?></p>
        <label class="border-black rounded-15 px-4 my-1">term <?= $invoice['term'] ?></label>
        <?= $this->session->flashdata('upload_invoice') ?>
    </div>
    <div class="row justify-content-center my-4">
        <div class="col-lg-9">
            <div class="shadow bg-body rounded-15 p-5 mb-4">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <table class="table table-borderless" id="tableDetailPayment">
                            <?php $hargaPaket = $detail_invoice['harga_paket']; ?>
                            <tr>
                                <td class="p-0" width="50%">
                                    <p>Paket <?= $detail_invoice['nama_paket'] ?></p>
                                </td>
                                <td class="p-0" width="50%">
                                    <p class="fw-bold">: Rp. <?= number_format($hargaPaket, 0, ".", ".") ?></p>
                                </td>
                            </tr>
                            <?php $hargaBayar = 0; ?>
                            <?php foreach ($detail_invoice['detail_invoice'] as $d_item) : ?>
                                <tr>
                                    <td class="p-0" width="50%">
                                        <p class="ms-2 text-small">Term <?= $d_item['term'] ?></p>
                                    </td>
                                    <td class="p-0" width="50%">
                                        <p class="text-small">
                                            <span class="fw-bold">: Rp. <?= number_format($d_item['biaya'], 0, ".", ".") ?></span>
                                            <?php if ($d_item['status'] == "paid") : ?>
                                                <?php $hargaBayar = $hargaBayar + $d_item['biaya']; ?>
                                                &nbsp; <i class="ms-2 me-1 material-icons align-middle text-smaller text-yellow">check_circle</i><?= $d_item['status'] ?>
                                            <?php else : ?>
                                                &nbsp; <i class="ms-2 me-1 material-icons align-middle text-smaller text-red">error</i><?= $d_item['status'] ?>
                                            <?php endif ?>
                                        </p>
                                    </td>
                                </tr>
                            <?php
                            endforeach;
                            $hargaSisa = $hargaPaket - $hargaBayar;
                            ?>
                            <tr>
                                <td class="p-0" width="50%">
                                    <p>Sisa</p>
                                </td>
                                <td class="p-0" width="50%">
                                    <p class="fw-bold">: Rp. <?= number_format($hargaSisa, 0, ".", ".") ?></p>
                                </td>
                            </tr>
                        </table>
                        <a target="_blank" href="<?= base_url('/asset/file/invoice/' . $invoice['lampiran']) ?>" class="btn btn-sm btn-primary my-bg-blue border-0 mb-2">
                            <span class="material-icons align-middle my-text-small">
                                download
                            </span>
                            invoice
                        </a>
                    </div>
                    <div class="col-md-4 text-center">
                        <?php
                        if ($invoice['status'] == "paid" || $invoice['status'] == "waiting")
                            $disabledBukti = "";
                        else
                            $disabledBukti = "disabled";

                        if ($invoice['status'] == "waiting")
                            $disabledConfirm = "";
                        else
                            $disabledConfirm = "disabled";

                        if ($invoice['status'] == "unpaid")
                            $disabledEC = "";
                        else
                            $disabledEC = "disabled";

                        if (empty($payment['bukti'])) $bukti = "";
                        else $bukti = base_url('asset/file/payment/' . $payment['bukti']);

                        ?>
                        <a target="_blank" href="<?= $bukti ?>" class="btn btn-sm btn-outline-primary border-blue my-text-blue border-1 my-1 <?= $disabledBukti ?>" style="width: 150px;">bukti pembayaran</a>
                        <br>
                        <button class="btn btn-sm btn-outline-warning border-yellow my-text-yellow border-1 my-1 <?= $disabledConfirm ?>" data-bs-toggle="modal" data-bs-target="#konfirmasiModal" style="width: 150px;">konfirmasi</button>
                        <br>
                        <button class="btn btn-sm btn-outline-primary border-blue my-text-blue border-1 my-1 <?= $disabledEC ?>" data-bs-toggle="modal" data-bs-target="#editInvoiceModal" style="width: 150px;">edit</button>
                        <br>
                        <button class="btn btn-sm btn-outline-danger border-red my-text-red border-1 my-1 <?= $disabledEC ?>" data-bs-toggle="modal" data-bs-target="#cancelModal" style=" width: 150px;">batalkan</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Konfirmasi Pembayaran -->
<div class="modal fade" id="konfirmasiModal" tabindex="-1" aria-labelledby="konfirmasiModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Konfirmasi Pembayaran</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                apakah anda yakin melakukan konfirmasi pada pembayaran ini?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary border-0 my-bg-blue" data-bs-dismiss="modal">tidak</button>
                <form action="<?= base_url('/po-admin/payment/' . $invoice['id_invoice'] . '/confirm?p=' . $invoice['id_invoice']) ?>" method="POST">
                    <button type="submit" class="btn btn-danger border-0 my-bg-red" name="submitPayment">ya</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal Cancel Payment -->
<div class="modal fade" id="cancelModal" tabindex="-1" aria-labelledby="cancelModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Batalkan Pembayaran</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                apakah anda yakin ingin membatalkan pembayaran ini?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary border-0 my-bg-blue" data-bs-dismiss="modal">tidak</button>
                <form action="<?= base_url('/po-admin/payment/' . $invoice['id_invoice'] . '/cancel') ?>" method="POST">
                    <button type="submit" class="btn btn-danger border-0 my-bg-red" name="submitCancel">ya</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal Edit Invoice -->
<div class="modal fade" id="editInvoiceModal" tabindex="-1" aria-labelledby="editInvoiceModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="btn-close float-end" data-bs-dismiss="modal" aria-label="Close"></button>
                <div class="text-center">
                    <h5 class="fw-bold m-0 mt-4">edit invoice</h5>
                    <p class="m-0 fw-bold">Invoice <?= $project['judul'] ?></p>
                </div>
                <div class="m-3">
                    <form action="" method="POST" enctype="multipart/form-data">
                        <div class="mb-3">
                            <label for="id" class="form-label fw-normal">id</label>
                            <input type="text" class="form-control form-rounded-0" name="id" id="id" value="<?= $invoice['kode_invoice'] ?>" readonly required>
                            <small class="text-danger">
                                <?= form_error('id') ?>
                            </small>
                        </div>
                        <div class="mb-3">
                            <label for="tanggal" class="form-label fw-normal">tanggal</label>
                            <input type="date" class="form-control form-rounded-0" name="tanggal" id="tanggal" value="<?= date("Y-m-d") ?>" required>
                            <small class="text-danger">
                                <?= form_error('tanggal') ?>
                            </small>
                        </div>
                        <div class="row mb-3">
                            <div class="col-3">
                                <label for="term" class="form-label fw-normal">term</label>
                                <input type="number" min="1" class="form-control form-rounded-0" name="term" id="term" value="<?= set_value('term', $invoice['term']) ?>" required>
                                <small class="text-danger">
                                    <?= form_error('term') ?>
                                </small>
                            </div>
                            <div class="col-9">
                                <label for="biaya" class="form-label fw-normal">biaya (Rp.)</label>
                                <input type="number" min="1" class="form-control form-rounded-0" name="biaya" id="biaya" value="<?= set_value('biaya', $invoice['biaya']) ?>" required>
                                <small class="text-danger">
                                    <?= form_error('biaya') ?>
                                </small>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="lampiran" class="form-label fw-normal">unggah lampiran</label>
                            <input type="file" class="form-control form-rounded-0" name="lampiran" id="lampiran" required>
                            <small class="text-danger">max size : 500 KB, typefile : pdf/png/jpg</small>
                        </div>
                        <button type="submit" class="btn form-btn-rounded-0 mt-2" name="submitInvoice">simpan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>