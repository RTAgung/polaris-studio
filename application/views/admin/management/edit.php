<div class="container-fluid">
    <h4 class="text-center">Ganti Password</h4>

    <div class="row">
        <div class="col-md-2">
            <!-- Kosong -->
        </div>
        <div class="col-md">
            <div class="shadow bg-body rounded-15 p-4 mb-4">
                <form action="<?= base_url('po-admin/management/edit_password') ?>" method="POST" enctype="multipart/form-data">
                    <div class="mb-3">
                        <label for="pwlama" class="form-label fw-normal">Password Lama</label>
                        <input type="password" class="form-control form-rounded-0" name="pwlama" id="pwlama" required>
                        <small class="text-danger">
                            <?= form_error('pwlama') ?>
                        </small>
                        <?= $this->session->flashdata('pwlama') ?>
                    </div>

                    <div class="mb-3">
                        <label for="pwbaru" class="form-label fw-normal">Password Baru</label>
                        <input type="password" class="form-control form-rounded-0" name="pwbaru" id="pwbaru" required>
                        <small class="text-danger">
                            <?= form_error('pwbaru') ?>
                        </small>
                    </div>

                    <div class="mb-3">
                        <label for="konfirmasipw" class="form-label fw-normal">Konfirmasi Password Baru</label>
                        <input type="password" class="form-control form-rounded-0" name="konfirmasipw" id="konfirmasipw" required>
                        <small class="text-danger">
                            <?= form_error('konfirmasipw') ?>
                        </small>
                        <?= $this->session->flashdata('konfirmasipw') ?>
                    </div>
                    <button type="submit" class="btn form-btn-rounded-0 mt-2" name="submit">simpan</button>
                </form>
            </div>
        </div>
        <div class="col-md-2">
            <!-- Kosong -->
        </div>
    </div>
</div>

<script>
<?php if($this->session->flashdata('update_pw_berhasil')){
    ?>
    alert("Password berhasil diubah")
    <?php
} ?>
</script>