<div class="container-fluid">
    <h4>Admins</h4>

    <div class="shadow bg-body rounded-15 p-4 mb-4">

        <div class="mb-4">
            <a href="#" class="text-dark link-hover-underline" data-bs-toggle="modal" data-bs-target="#addAdminModal">
                <span class="material-icons align-middle text-green fs-2">
                    add_circle
                </span>
            </a>

            <div class="table-responsive">
                <table class="table table-sm table-hover">
                    <thead>
                        <tr>
                            <th>No. </th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>No Hp</th>
                            <th>Hire Date</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($admin as $index => $value) {
                        ?>
                            <tr>
                                <td class="align-middle"><?= $index + 1 ?></td>
                                <td class="align-middle"><?= $value['nama'] ?></td>
                                <td class="align-middle"><?= $value['email'] ?></td>
                                <td class="align-middle">
                                    <?= $value['no_hp'] ?>
                                    <a target="_blank" href="https://wa.me/<?= $value['no_hp'] ?>" class="btn btn-sm btn-outline-primary border-blue border-1 my-text-blue ms-2 rounded-pill my-text-smaller">
                                        <i class="fab fa-whatsapp my-text-small"></i>
                                        call
                                    </a>
                                </td>
                                <td class="align-middle"><?= $value['tgl_masuk'] ?></td>
                                <td class="align-middle">
                                    <div class="float-end">
                                        <a class="btn btn-primary btn-sm my-bg-blue" data-bs-toggle="modal" data-bs-target="#editAdminModal<?= $value['id_admin'] ?>">
                                            <span class="material-icons align-middle my-text-small">
                                                edit
                                            </span>
                                        </a>
                                        <?php
                                        if ($value['jabatan'] != "manajer") {
                                        ?>
                                            <a class="btn btn-danger btn-sm" data-bs-toggle="modal" data-bs-target="#deleteModal<?= $value['id_admin'] ?>">
                                                <span class="material-icons align-middle my-text-small">
                                                    delete
                                                </span>
                                            </a>
                                        <?php
                                        }
                                        ?>
                                        <a class="btn btn-warning btn-sm" data-bs-toggle="modal" data-bs-target="#detailAdminModal<?= $value['id_admin'] ?>">
                                            <span class="material-icons align-middle my-text-small">
                                                arrow_forward
                                            </span>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<?php
foreach ($admin as $index => $value) {
    $lahir = date("d M Y", strtotime($value['tgl_lahir']));
    $masuk = date("d M Y", strtotime($value['tgl_masuk']));
?>

    <!-- DETAIL -->
    <div class="modal fade" id="detailAdminModal<?= $value['id_admin'] ?>" tabindex="-1" aria-labelledby="detailAdminModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="btn-close float-end" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="text-center">
                        <h5 class="fw-bold m-0 mt-4">Detail</h5>
                        <p class="m-0 fw-bold"><?= $value['nama'] ?></p>
                    </div>

                    <div class="row m-3">
                        <div class="col-md-5">email</div>
                        <div class="col-md"><?= $value['email'] ?></div>
                    </div>

                    <div class="row m-3">
                        <div class="col-md-5">nomor HP</div>
                        <div class="col-md"><?= $value['no_hp'] ?></div>
                    </div>

                    <div class="row m-3">
                        <div class="col-md-5">alamat</div>
                        <div class="col-md"><?= $value['alamat'] ?></div>
                    </div>

                    <div class="row m-3">
                        <div class="col-md-5">Tanggal lahir</div>
                        <div class="col-md"><?= $lahir ?></div>
                    </div>

                    <div class="row m-3">
                        <div class="col-md-5">Tanggal masuk</div>
                        <div class="col-md"><?= $masuk ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Delete admin -->
    <div class="modal fade" id="deleteModal<?= $value['id_admin'] ?>" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete Admin</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    apakah anda yakin ingin menghapus akun <span class="fw-bold"><?= $value['nama'] ?></span> selamanya?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary border-0 my-bg-blue" data-bs-dismiss="modal">tidak</button>
                    <form action="<?= base_url('/po-admin/management/delete/' . $value['id_admin']) ?>" method="POST">
                        <button type="submit" class="btn btn-danger border-0 my-bg-red" name="submit">ya</button>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal Update Admin -->
    <div class="modal fade" id="editAdminModal<?= $value['id_admin'] ?>" tabindex="-1" aria-labelledby="editAdminModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="btn-close float-end" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="text-center">
                        <h5 class="fw-bold m-0 mt-4">Edit Admin</h5>
                        <p class="m-0 fw-bold"><?= $value['nama'] ?></p>
                    </div>
                    <div class="m-3">
                        <form action="<?= base_url('/po-admin/management/edit/' . $value['id_admin']) ?>" method="POST">
                            <div class="mb-3">
                                <label for="nama" class="form-label fw-normal">Nama</label>
                                <input type="text" class="form-control form-rounded-0" name="nama" id="nama" value="<?= $value['nama'] ?>" required>
                                <small class="text-danger">
                                    <?= form_error('nama') ?>
                                </small>
                            </div>

                            <div class="mb-3">
                                <label for="email" class="form-label fw-normal">Email</label>
                                <input type="email" class="form-control form-rounded-0" name="email" id="email" value="<?= $value['email'] ?>" required>
                                <small class="text-danger">
                                    <?= form_error('email') ?>
                                </small>
                            </div>

                            <?php
                            if ($value['jabatan'] != "manajer") {
                            ?>
                                <div class="mb-3">
                                    <label for="password" class="form-label fw-normal">Password</label>
                                    <div class="row">
                                        <div class="col-8">
                                            <input type="text" class="form-control form-rounded-0" name="password" id="form-password">
                                        </div>
                                        <div class="col-4 d-grid">
                                            <p class="btn form-btn-rounded-0 m-0" id="buttonGenerate" data-bs-target="form-password">generate</p>
                                        </div>
                                    </div>
                                    <small class="text-secondary">
                                        *kosongkan jika tidak ingin diubah
                                    </small>
                                    <small class="text-danger">
                                        <?= form_error('password') ?>
                                    </small>
                                </div>
                            <?php
                            }
                            ?>

                            <div class="mb-3">
                                <label for="no_hp" class="form-label fw-normal">Nomor Handphone</label>
                                <input type="text" class="form-control form-rounded-0" name="no_hp" id="no_hp" value="<?= $value['no_hp'] ?>" required>
                                <small class="text-danger">
                                    <?= form_error('no_hp') ?>
                                </small>
                            </div>

                            <div class="mb-3">
                                <label for="alamat" class="form-label fw-normal">Alamat</label>
                                <textarea type="text" class="form-control form-rounded-0" rows="3" name="alamat" id="alamat" required><?= $value['alamat'] ?></textarea>
                                <small class="text-danger">
                                    <?= form_error('alamat') ?>
                                </small>
                            </div>

                            <div class="mb-3">
                                <label for="lahir" class="form-label fw-normal">Tanggal Lahir</label>
                                <input type="date" class="form-control form-rounded-0" name="lahir" id="lahir" value="<?= $value['tgl_lahir'] ?>" required>
                                <small class="text-danger">
                                    <?= form_error('lahir') ?>
                                </small>
                            </div>

                            <div class="mb-3">
                                <label for="masuk" class="form-label fw-normal">Tanggal Diterima</label>
                                <input type="date" class="form-control form-rounded-0" name="masuk" id="masuk" value="<?= $value['tgl_masuk'] ?>" required>
                                <small class="text-danger">
                                    <?= form_error('masuk') ?>
                                </small>
                            </div>

                            <button type="submit" class="btn form-btn-rounded-0 mt-2" name="submit">simpan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
}
?>

<!-- Modal Add Admin -->
<div class="modal fade" id="addAdminModal" tabindex="-1" aria-labelledby="addAdminModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="btn-close float-end" data-bs-dismiss="modal" aria-label="Close"></button>
                <div class="text-center">
                    <h5 class="fw-bold m-0 mt-4">Tambah Admin</h5>
                    <p class="m-0 fw-bold"></p>
                </div>
                <div class="m-3">
                    <form action="<?= base_url('/po-admin/management/new') ?>" method="POST">
                        <div class="mb-3">
                            <label for="nama" class="form-label fw-normal">Nama</label>
                            <input type="text" class="form-control form-rounded-0" name="nama" id="nama" value="" required>
                            <small class="text-danger">
                                <?= form_error('nama') ?>
                            </small>
                        </div>

                        <div class="mb-3">
                            <label for="email" class="form-label fw-normal">Email</label>
                            <input type="email" class="form-control form-rounded-0" name="email" id="email" value="" required>
                            <small class="text-danger">
                                <?= form_error('email') ?>
                            </small>
                        </div>

                        <div class="mb-3">
                            <label for="password" class="form-label fw-normal">Password</label>
                            <div class="row">
                                <div class="col-8">
                                    <input type="text" class="form-control form-rounded-0" name="password" id="form-password" required>
                                </div>
                                <div class="col-4 d-grid">
                                    <p class="btn form-btn-rounded-0 m-0" id="buttonGenerate" data-bs-target="form-password">generate</p>
                                </div>
                            </div>
                            <small class="text-danger">
                                <?= form_error('password') ?>
                            </small>
                        </div>

                        <div class="mb-3">
                            <label for="no_hp" class="form-label fw-normal">Nomor Handphone</label>
                            <input type="text" class="form-control form-rounded-0" name="no_hp" id="no_hp" value="" required>
                            <small class="text-danger">
                                <?= form_error('no_hp') ?>
                            </small>
                        </div>

                        <div class="mb-3">
                            <label for="alamat" class="form-label fw-normal">Alamat</label>
                            <textarea type="text" class="form-control form-rounded-0" rows="3" name="alamat" id="alamat" required></textarea>
                            <small class="text-danger">
                                <?= form_error('alamat') ?>
                            </small>
                        </div>

                        <div class="mb-3">
                            <label for="lahir" class="form-label fw-normal">Tanggal Lahir</label>
                            <input type="date" class="form-control form-rounded-0" name="lahir" id="lahir" value="<?= date("Y-m-d") ?>" required>
                            <small class="text-danger">
                                <?= form_error('lahir') ?>
                            </small>
                        </div>

                        <div class="mb-3">
                            <label for="masuk" class="form-label fw-normal">Tanggal Diterima</label>
                            <input type="date" class="form-control form-rounded-0" name="masuk" id="masuk" value="<?= date("Y-m-d") ?>" required>
                            <small class="text-danger">
                                <?= form_error('masuk') ?>
                            </small>
                        </div>

                        <button type="submit" class="btn form-btn-rounded-0 mt-2" name="submit">simpan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>