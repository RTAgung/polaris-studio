<div class="container p-4 mb-5">
    <div class="row justify-content-center">
        <div class="col-lg-7 col-12">
            <div class="text-center mb-5">
                <h3 class="m-0"><?= $client['nama_instansi'] ?></h3>
                <p class="m-0 fs-5"><?= $client['nama_pemilik'] ?></p>
            </div>
            <form class="px-4" action="" method="POST">
                <div class="mb-3">
                    <label for="namaInstansi" class="form-label">nama instansi</label>
                    <input type="text" class="form-control form-rounded-0" name="namaInstansi" id="namaInstansi" value="<?= set_value('namaInstansi', $client['nama_instansi']) ?>" readonly disabled required>
                </div>
                <div class="mb-3">
                    <label for="namaPemilikInstansi" class="form-label">nama pemilik instansi</label>
                    <input type="text" class="form-control form-rounded-0" name="namaPemilikInstansi" id="namaPemilikInstansi" value="<?= set_value('namaPemilikInstansi', $client['nama_pemilik']) ?>" required>
                    <small class="text-danger">
                        <?= form_error('namaPemilikInstansi') ?>
                    </small>
                </div>
                <div class="mb-3">
                    <label for="email" class="form-label">email</label>
                    <input type="email" class="form-control form-rounded-0" name="email" id="email" value="<?= set_value('email', $client['email']) ?>" readonly disabled required>
                </div>
                <div class="mb-3">
                    <label for="alamat" class="form-label">alamat</label>
                    <textarea class="form-control form-rounded-0" name="alamat" id="alamat" rows="3" required><?= set_value('alamat', $client['alamat']) ?></textarea>
                    <small class="text-danger">
                        <?= form_error('alamat') ?>
                    </small>
                </div>
                <div class="mb-3">
                    <label for="instagram" class="form-label">instagram</label>
                    <input type="text" class="form-control form-rounded-0" name="instagram" id="instagram" value="<?= set_value('instagram', $client['instagram']) ?>" required>
                    <small class="text-danger">
                        <?= form_error('instagram') ?>
                    </small>
                </div>
                <div class="mb-3">
                    <label for="whatsapp" class="form-label">whatsapp</label>
                    <input type="text" class="form-control form-rounded-0" name="whatsapp" id="whatsapp" value="<?= set_value('whatsapp', $client['whatsapp']) ?>" required>
                    <small class="text-danger">
                        <?= form_error('whatsapp') ?>
                    </small>
                </div>
                <button type="submit" class="btn form-btn-rounded-0 mt-3" name="submit">simpan</button>
            </form>
        </div>
    </div>
</div>