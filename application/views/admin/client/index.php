<div class="container-fluid">
    <div class="shadow bg-body rounded-15 p-4 mb-4">
        <div class="row justify-content-between">
            <div class="col-lg-5">
                <h4>Client</h4>
            </div>
            <div class="col-lg-5">
                <form method="GET" action="">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control form-control-sm rounded-15 border-1 border-blue" name="search" placeholder="cari disini" aria-label="Recipient's username" aria-describedby="button-addon2" style="border-color: #4D9DCF;">
                        <button class="btn btn-sm btn-primary rounded-15 border-0 my-bg-blue" type="button" id="button-addon2">
                            <i class="material-icons align-middle text-small">search</i>
                            search
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-sm table-hover">
                <thead>
                    <tr>
                        <th>No. </th>
                        <th>Instansi</th>
                        <th>Email</th>
                        <th>Telepon</th>
                        <th class="text-center">Project</th>
                        <th class="text-center">Terdaftar</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $count = $countClient;
                    foreach ($clients as $index => $client) :
                        $count--;
                    ?>
                        <tr>
                            <td class="align-middle"><?= $index + 1 ?>. </td>
                            <td class="align-middle"><?= $client['nama_instansi'] ?></td>
                            <td class="align-middle"><?= $client['email'] ?></td>
                            <td class="align-middle">
                                <?= $client['whatsapp'] ?>
                                <a target="_blank" href="https://wa.me/<?= $client['whatsapp'] ?>" class="btn btn-sm btn-outline-primary border-blue border-1 my-text-blue ms-2 rounded-pill my-text-smaller">
                                    <i class="fab fa-whatsapp my-text-small"></i>
                                    call
                                </a>
                            </td>
                            <td class="align-middle text-center"><?= $client['project'] ?></td>
                            <td class="align-middle text-center">
                                <?php if ($client['is_registered']) : ?>
                                    <span class="material-icons align-middle text-yellow">
                                        check_circle
                                    </span>
                                <?php else : ?>
                                    <span class="material-icons align-middle my-text-red">
                                        error
                                    </span>
                                <?php endif ?>
                            </td>
                            <td class="align-middle">
                                <div class="float-end">
                                    <?php if ($client['is_registered']) : ?>
                                        <a href="<?= base_url('/po-admin/client/' . $client['id_client']) ?>" class="btn btn-primary btn-sm my-bg-blue border-0">
                                            <span class="material-icons align-middle my-text-small">
                                                arrow_forward
                                            </span>
                                        </a>
                                    <?php else : ?>
                                        <button class="btn btn-danger btn-sm my-bg-red border-0" data-bs-toggle="modal" data-bs-target="#passwordModal<?= $client['id_client'] ?>">
                                            <span class="material-icons align-middle my-text-small">
                                                add
                                            </span>
                                            password
                                        </button>
                                        <button class="btn btn-danger btn-sm my-bg-red border-0" data-bs-toggle="modal" data-bs-target="#deleteModal<?= $client['id_client'] ?>">
                                            <span class="material-icons align-middle text-small">
                                                delete_forever
                                            </span>
                                        </button>
                                    <?php endif ?>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
        <?php if ($count > 0) : ?>
            <p class="text-end mb-0"><a class="text-underline text-dark link-hover-underline" href="<?= base_url('/po-admin/client?more=all') ?>">show all</a></p>
        <?php endif ?>
    </div>
</div>

<!-- Modal Generate Password -->
<?php foreach ($clients as $client) :  ?>
    <div class="modal fade" id="passwordModal<?= $client['id_client'] ?>" tabindex="-1" aria-labelledby="passwordModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="btn-close float-end" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="text-center">
                        <h5 class="fw-bold m-0 mt-4">Generate Password</h5>
                        <p class="m-0"><?= $client['nama_instansi'] ?></p>
                    </div>
                    <div class="mt-4 mx-3">
                        <form class="mt-4" action="" method="POST">
                            <input type="text" name="id" value="<?= $client['id_client'] ?>" hidden readonly>
                            <div class="mb-3">
                                <label for="password" class="form-label text-small">Password</label>
                                <div class="row">
                                    <div class="col-8">
                                        <input type="text" class="form-control form-rounded-0" name="password" id="form-password<?= $client['id_client'] ?>" required>
                                    </div>
                                    <div class="col-4 d-grid">
                                        <p class="btn form-btn-rounded-0 m-0" id="buttonGenerate" data-bs-target="form-password<?= $client['id_client'] ?>">generate</p>
                                    </div>
                                </div>
                                <small class="text-danger">
                                    <?= form_error('password') ?>
                                </small>
                            </div>
                            <div class="d-grid gap-2 col-2 mx-auto">
                                <button type="submit" class="btn form-btn-rounded-0 mt-2 mb-4" name="submit">simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Delete Client -->
    <div class="modal fade" id="deleteModal<?= $client['id_client'] ?>" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete Client</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    apakah anda yakin ingin menghapus akun <span class="fw-bold"><?= $client['nama_instansi'] ?></span> selamanya?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary border-0 my-bg-blue" data-bs-dismiss="modal">tidak</button>
                    <form action="<?= base_url('/po-admin/client/' . $client['id_client'] . '/delete') ?>" method="POST">
                        <button type="submit" class="btn btn-danger border-0 my-bg-red" name="submitDelete">ya</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php endforeach ?>