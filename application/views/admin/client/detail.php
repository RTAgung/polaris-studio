<div class="container-fluid">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6 text-center">
            <h3 class="mb-1"><?= $client['nama_instansi'] ?></h3>
            <p class="fs-5 mb-0"><?= $client['nama_pemilik'] ?></p>
        </div>
        <div class="col-md-3 text-end">
            <a href="<?= base_url('/po-admin/client/' . $client['id_client'] . '/edit') ?>" class="btn btn-sm btn-warning my-bg-yellow rounded-pill fw-bold border-0">edit</a>
            <br>
            <button class="btn btn-sm btn-danger my-bg-red rounded-pill fw-bold my-1 border-0" data-bs-toggle="modal" data-bs-target="#deleteModal">
                delete
            </button>
        </div>
    </div>
    <div class="shadow bg-body rounded-15 px-4 pt-4 pb-2 my-4">
        <div class="row">
            <div class="col-md-5">
                <div class="row">
                    <div class="col-1">
                        <i class="material-icons">mail</i>
                    </div>
                    <div class="col-11">
                        <p><?= $client['email'] ?></p>
                    </div>
                    <div class="col-1">
                        <i class="material-icons">business</i>
                    </div>
                    <div class="col-11">
                        <p>
                            <?php
                            if (empty($client['alamat'])) echo 'belum dilengkapi';
                            else echo $client['alamat'];
                            ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="row">
                    <div class="col-1">
                        <i class="fab fa-instagram" style="font-size: 24px;"></i>
                    </div>
                    <div class="col-11">
                        <p>
                            <?= $client['instagram'] ?>
                            <a target="_blank" href="https://www.instagram.com/<?= $client['instagram'] ?>" class="btn btn-sm btn-outline-primary border-blue border-1 my-text-blue ms-2 rounded-pill my-text-smaller">
                                see
                            </a>
                        </p>
                    </div>
                    <div class="col-1">
                        <i class="fab fa-whatsapp" style="font-size: 24px;"></i>
                    </div>
                    <div class="col-11">
                        <p>
                            <?= $client['whatsapp'] ?>
                            <a target="_blank" href="https://wa.me/<?= $client['whatsapp'] ?>" class="btn btn-sm btn-outline-primary border-blue border-1 my-text-blue ms-2 rounded-pill my-text-smaller">
                                call
                            </a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-2 text-end">
                <button class="btn btn-sm btn-danger border-0 my-bg-red mb-3" data-bs-toggle="modal" data-bs-target="#passwordModal">
                    <span class="material-icons align-middle text-small">
                        restart_alt
                    </span>
                    password
                </button>
            </div>
        </div>
    </div>
    <div class="shadow bg-body rounded-15 p-4 mb-4">
        <div class="row justify-content-between">
            <div class="col-lg-5">
                <h4>Project</h4>
            </div>
            <div class="col-lg-5">
                <form class="" action="">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control form-control-sm rounded-15 border-1 border-blue" name="search" placeholder="cari disini" aria-label="Recipient's username" aria-describedby="button-addon2" style="border-color: #4D9DCF;">
                        <button class="btn btn-sm btn-primary rounded-15 border-0 my-bg-blue" type="button" id="button-addon2">
                            <i class="material-icons align-middle text-small">search</i>
                            search
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-sm table-hover">
                <thead>
                    <tr>
                        <th>No. </th>
                        <th>Judul</th>
                        <th>Paket</th>
                        <th class="text-center">Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $countP = $countProjects;
                    foreach ($projects as $index => $project) :
                        $countP--;
                    ?>
                        <tr>
                            <td class="align-middle"><?= $index + 1 ?>. </td>
                            <td class="align-middle">
                                <?php if (empty($project['judul'])) echo "Judul Project";
                                else echo $project['judul'] ?>
                            </td>
                            <td class="align-middle"><?= $project['nama'] ?></td>
                            <td class="align-middle text-center"><?= $project['status'] ?></td>
                            <td class="align-middle">
                                <div class="float-end">
                                    <a target="_blank" href="<?= $project['asset'] ?>" class="btn btn-warning btn-sm my-bg-yellow border-0">
                                        <span class="material-icons align-middle my-text-small">
                                            cloud_download
                                        </span>
                                    </a>
                                    <a href="<?= base_url('/po-admin/project/' . $project['jenis'] . '/' . $project['id_order']) ?>" class="btn btn-primary btn-sm my-bg-blue border-0">
                                        <span class="material-icons align-middle my-text-small">
                                            arrow_forward
                                        </span>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
        <?php if ($countP > 0) : ?>
            <p class="text-end mb-0"><a class="text-underline text-dark link-hover-underline" href="<?= base_url('/po-admin/client/' . $client['id_client'] . '?pr_m=all') ?>">show all</a></p>
        <?php endif ?>
    </div>
    <div class="shadow bg-body rounded-15 p-4 mb-4">
        <div class="row justify-content-between">
            <div class="col-lg-5">
                <h4>Payment</h4>
            </div>
            <div class="col-lg-5">
                <form class="" action="">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control form-control-sm rounded-15 border-1 border-blue" name="search" placeholder="cari disini" aria-label="Recipient's username" aria-describedby="button-addon2" style="border-color: #4D9DCF;">
                        <button class="btn btn-sm btn-primary rounded-15 border-0 my-bg-blue" type="button" id="button-addon2">
                            <i class="material-icons align-middle text-small">search</i>
                            search
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-sm table-hover">
                <thead>
                    <tr>
                        <th>No. </th>
                        <th>Invoice</th>
                        <th>Paket</th>
                        <th class="text-center">Term</th>
                        <th>Harga</th>
                        <th class="text-center">Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $countI = $countInvoices;
                    foreach ($invoices as $index => $invoice) :
                        $countI--;
                    ?>
                        <tr>
                            <td class="align-middle"><?= $index + 1 ?>. </td>
                            <td class="align-middle">Invoice <?= $invoice['judul'] ?></td>
                            <td class="align-middle">Paket <?= $invoice['nama_paket'] ?></td>
                            <td class="align-middle text-center"><?= $invoice['term'] ?></td>
                            <td class="align-middle">Rp. <?= number_format($invoice['harga_paket'], 0, ".", ".") ?></td>
                            <td class="align-middle text-center"><?= $invoice['status'] ?></td>
                            <td class="align-middle">
                                <div class="float-end">
                                    <?php
                                    if ($invoice['status'] == "paid" || $invoice['status'] == "waiting")
                                        $disabled = "";
                                    else
                                        $disabled = "disabled";
                                    ?>
                                    <button class="btn btn-warning btn-sm my-bg-yellow border-0 <?= $disabled ?>" data-bs-toggle="modal" data-bs-target="#paymentModal<?= $invoice['id_invoice'] ?>">
                                        <span class="material-icons align-middle my-text-small">
                                            receipt_long
                                        </span>
                                    </button>
                                    <a href="<?= base_url('/po-admin/payment/' . $invoice['id_invoice']) ?>" class="btn btn-primary btn-sm my-bg-blue border-0">
                                        <span class="material-icons align-middle my-text-small">
                                            arrow_forward
                                        </span>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
        <?php if ($countI > 0) : ?>
            <p class="text-end mb-0"><a class="text-underline text-dark link-hover-underline" href="<?= base_url('/po-admin/client/' . $client['id_client'] . '?py_m=all') ?>">show all</a></p>
        <?php endif ?>
    </div>
</div>

<!-- Modal Generate Password -->
<div class="modal fade" id="passwordModal" tabindex="-1" aria-labelledby="passwordModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="btn-close float-end" data-bs-dismiss="modal" aria-label="Close"></button>
                <div class="text-center">
                    <h5 class="fw-bold m-0 mt-4">Generate Password</h5>
                    <p class="m-0"><?= $client['nama_instansi'] ?></p>
                </div>
                <div class="mt-4 mx-3">
                    <form class="mt-4" action="" method="POST">
                        <input type="text" name="id" value="<?= $client['id_client'] ?>" hidden readonly>
                        <div class="mb-3">
                            <label for="password" class="form-label text-small">Password</label>
                            <div class="row">
                                <div class="col-8">
                                    <input type="text" class="form-control form-rounded-0" name="password" id="form-password" required>
                                </div>
                                <div class="col-4 d-grid">
                                    <p class="btn form-btn-rounded-0 m-0" id="buttonGenerate" data-bs-target="form-password">generate</p>
                                </div>
                            </div>
                            <small class="text-danger">
                                <?= form_error('password') ?>
                            </small>
                        </div>
                        <div class="d-grid gap-2 col-2 mx-auto">
                            <button type="submit" class="btn form-btn-rounded-0 mt-2 mb-4" name="submit">simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Delete Client -->
<div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Delete Client</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                apakah anda yakin ingin menghapus akun ini selamanya?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary border-0 my-bg-blue" data-bs-dismiss="modal">tidak</button>
                <form action="<?= base_url('/po-admin/client/' . $client['id_client'] . '/delete') ?>" method="POST">
                    <button type="submit" class="btn btn-danger border-0 my-bg-red" name="submitDelete">ya</button>
                </form>
            </div>
        </div>
    </div>
</div>

<?php foreach ($invoices as $invoice) : ?>
    <!-- Modal Payment -->
    <div class="modal fade" id="paymentModal<?= $invoice['id_invoice'] ?>" tabindex="-1" aria-labelledby="paymentModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="btn-close float-end" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="text-center">
                        <h5 class="fw-bold m-0 mt-4">Invoice <?= $invoice['judul'] ?></h5>
                        <p class="m-0 fw-bold">Paket <?= $invoice['nama_paket'] ?></p>
                        <p class="m-0 my-text-small">Term <?= $invoice['term'] ?></p>
                    </div>
                    <div class="m-3">
                        <p>harga : <span class="fw-bold">Rp. <?= number_format($invoice['harga_paket'], 0, ".", ".") ?></span></p>
                        <div class="row">
                            <div class="col-md-6 mb-2 text-center">
                                <a target="_blank" href="<?= base_url('/asset/file/payment/' . $invoice['bukti']) ?>" class="btn btn-primary border-0 my-bg-blue fw-bold" style="width: 210px;">
                                    <span class="material-icons align-middle me-1">
                                        paid
                                    </span>
                                    bukti pembayaran
                                </a>
                            </div>
                            <div class="col-md-6 mb-2 text-center">
                                <?php if ($invoice['status'] == 'waiting')
                                    $disabled = "";
                                else
                                    $disabled = "disabled";
                                ?>
                                <button class="btn btn-warning border-0 my-bg-yellow fw-bold <?= $disabled ?>" data-bs-toggle="modal" data-bs-target="#konfirmasiModal<?= $invoice['id_invoice'] ?>" style="width: 210px;">
                                    <span class="material-icons align-middle me-1">
                                        check_circle
                                    </span>
                                    konfirmasi
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Konfirmasi Pembayaran -->
    <div class="modal fade" id="konfirmasiModal<?= $invoice['id_invoice'] ?>" tabindex="-1" aria-labelledby="konfirmasiModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Konfirmasi Pembayaran</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    apakah anda yakin melakukan konfirmasi pada pembayaran ini?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary border-0 my-bg-blue" data-bs-dismiss="modal">tidak</button>
                    <form action="<?= base_url('/po-admin/payment/' . $invoice['id_invoice'] . '/confirm?c=' . $client['id_client']) ?>" method="POST">
                        <button type="submit" class="btn btn-danger border-0 my-bg-red" name="submitPayment">ya</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php endforeach ?>