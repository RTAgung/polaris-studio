<div class="container-fluid">
    <h4>Tags</h4>

    <div class="row">
        <div class="col-md-7">
            <form action="#">
                <div class="input-group mb-3">
                    <input type="text" class="form-control form-control-sm rounded-15 border-1 border-blue" name="search" placeholder="cari disini" aria-label="Recipient's username" aria-describedby="button-addon2" style="border-color: #4D9DCF;">
                    <button class="btn btn-sm btn-primary rounded-15 border-0 my-bg-blue" type="button" id="button-addon2">
                        <i class="material-icons align-middle text-small">search</i>
                        search
                    </button>
                </div>
            </form>
            <div class="shadow bg-body rounded-15 p-4 mb-4">
                <div class="table-responsive">
                    <table class="table table-sm table-hover">
                        <thead>
                            <tr>
                                <th>No. </th>
                                <th>Nama Tag</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($tags as $index => $value) {
                            ?>
                                <tr>
                                    <td class="align-middle"><?= $index + 1 ?></td>
                                    <td class="align-middle"><?= $value['nama'] ?></td>
                                    <td class="align-middle">
                                        <div class="float-end">
                                            <a class="btn btn-primary btn-sm my-bg-blue" data-bs-toggle="modal" data-bs-target="#editTagModal<?= $value['id_tag'] ?>">
                                                <span class="material-icons align-middle my-text-small">
                                                    edit
                                                </span>
                                            </a>
                                            <a class="btn btn-danger btn-sm" data-bs-toggle="modal" data-bs-target="#deleteModal<?= $value['id_tag'] ?>">
                                                <span class="material-icons align-middle my-text-small">
                                                    delete
                                                </span>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-1">
            <!-- Kosong -->
        </div>
        <div class="col-md-4">
            <h5>Tambah Tag</h5>
            <div class="shadow bg-body rounded-15 p-4 mb-4">
                <form action="<?= base_url('/po-admin/article/tag/new') ?>" method="POST">
                    <div class="form-group">
                        <p class="mb-0">Nama Tag</p>
                        <input type="text" class="form-control" id="tag" name="tag" required>
                    </div>

                    <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                </form>

            </div>
        </div>
    </div>
</div>


<!-- Modal Edit Tag -->
<?php
foreach ($tags as $index => $value) {
?>
    <div class="modal fade" id="editTagModal<?= $value['id_tag'] ?>" tabindex="-1" aria-labelledby="editTagModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="btn-close float-end" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="text-center">
                        <h5 class="fw-bold m-0 mt-4">Edit Tag</h5>
                        <p class="m-0 fw-bold"><?= $value['nama'] ?></p>
                    </div>

                    <div class="m-3">
                        <form action="<?= base_url('/po-admin/article/tag/edit/' . $value['id_tag']) ?>" method="POST">
                            <div class="mb-3">
                                <label for="lampiran" class="form-label fw-normal">Nama Tag</label>
                                <input type="text" class="form-control form-rounded-0" value="<?= $value['nama'] ?>" name="namaTag" id="namaTag" required>
                                <small class="text-danger">
                                    <?= form_error('namaTag') ?>
                                </small>
                            </div>
                            <button type="submit" class="btn form-btn-rounded-0 mt-2" name="submitTag">simpan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Delete tag -->
    <div class="modal fade" id="deleteModal<?= $value['id_tag'] ?>" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete Tag</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    apakah anda yakin ingin menghapus tag <span class="fw-bold"><?= $value['nama'] ?></span> selamanya?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary border-0 my-bg-blue" data-bs-dismiss="modal">tidak</button>
                    <form action="<?= base_url('/po-admin/article/tag/delete/' . $value['id_tag']) ?>" method="POST">
                        <button type="submit" class="btn btn-danger border-0 my-bg-red" name="submit">ya</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php
}
?>