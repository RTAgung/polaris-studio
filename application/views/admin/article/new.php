<?= $this->session->flashdata('upload_invoice') ?>
<?php
if (isset($form_filled)) {
} else {
?>
    <div class="shadow bg-body rounded-15 p-4 mb-4">
        <form action="<?= base_url('po-admin/article/new') ?>" method="POST" enctype="multipart/form-data">
            <div class="mb-3">
                <label for="id" class="form-label fw-normal">judul</label>
                <input type="text" class="form-control form-rounded-0" name="judul" id="judul" required>
                <small class="text-danger">
                    <?= form_error('judul') ?>
                </small>
            </div>
            <div class="mb-3">
                <label for="gambar" class="form-label fw-normal">gambar</label>
                <input type="file" class="form-control form-rounded-0" name="gambar" id="gambar">
                <small class="text-danger">max size : 500 KB, typefile : png/jpg/jpeg</small>
                <small class="text-danger">
                    <?= form_error('gambar') ?>
                </small>
            </div>
            <div class="mb-3">
                <label for="teks" class="form-label fw-normal">teks</label>
                <textarea type="text" class="form-control form-rounded-0" rows="15" name="teks" id="teks" required></textarea>
                <small class="text-danger">
                    <?= form_error('teks') ?>
                </small>
            </div>
            <div class="row mb-3">
                <?php
                for ($i = 1; $i <= 4; $i++) {
                ?>
                    <div class="col-md">
                        <select class="form-select" name="tag<?= $i ?>" id="tag<?= $i ?>" aria-label="Default select example">
                            <option value="0" selected>Pilih tag</option>
                            <?php
                            foreach ($tags as $index => $value) {
                            ?>
                                <option value="<?= $value['id_tag'] ?>"><?= $value['nama'] ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>

                <?php
                }
                ?>
            </div>
            <button type="submit" class="btn form-btn-rounded-0 mt-2" name="submit">simpan</button>
        </form>
    </div>
<?php
}
?>