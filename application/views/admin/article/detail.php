<div class="px-5 pt-0">
    <div class="p-5">
        <div class="row">
            <div class="col-md-1">
                <!-- kosong -->
            </div>
            <div class="col-md-10 text-justify px-3 pb-5 pt-3">
                <div class="shadow bg-body rounded-15 p-4 mb-4">

                    <div class="d-flex justify-content-between">
                        <h1 class="navbar-hover fs-4"><?= $detail_artikel['judul'] ?></h1>
                        <div>
                            <a href="<?= base_url('po-admin/article/edit/' . $detail_artikel['id_article']) ?>" type="submit" class="btn btn-primary btn-sm my-bg-blue" name="submit">
                                <span class="material-icons align-middle my-text-small">
                                    edit
                                </span>
                            </a>
                            <a type="submit" class="btn btn-danger btn-sm" name="submit" data-bs-toggle="modal" data-bs-target="#deleteModal<?= $detail_artikel['id_article'] ?>">
                                <span class="material-icons align-middle my-text-small">
                                    delete
                                </span>
                            </a>
                        </div>
                    </div>
                    <img class="mx-auto d-block" width="100%" style="object-fit: cover;" src="<?= base_url('/asset/file/article/' . $detail_artikel['gambar']) ?>" alt="">
                    <p class="pt-5"><?= nl2br($detail_artikel['teks']) ?><br><br></p>
                    <div class="d-flex flex-row bd-highlight mb-3">
                        <?php
                        foreach ($tags as $index => $value) {
                        ?>
                            <span class="badge bg-success m-1 p-2 bd-highlight">#<?= $value['nama'] ?></span>

                        <?php
                        }
                        ?>
                    </div>

                </div>
            </div>

            <div class="col-md-1 pt-5 mt-3">

            </div>
        </div>

        <hr width="100%">
    </div>


    <!-- Delete tag -->
    <div class="modal fade" id="deleteModal<?= $detail_artikel['id_article'] ?>" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete Article</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    apakah anda yakin ingin menghapus artikel <span class="fw-bold"><?= $detail_artikel['judul'] ?></span> selamanya?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary border-0 my-bg-blue" data-bs-dismiss="modal">tidak</button>
                    <form action="<?= base_url('/po-admin/article/delete/' . $detail_artikel['id_article']) ?>" method="POST">
                        <button type="submit" class="btn btn-danger border-0 my-bg-red" name="submit">ya</button>
                    </form>
                </div>
            </div>
        </div>
    </div>