<div class="container-fluid">
    <h4>Articles</h4>
    <div class="mb-4">
        <a href="<?= base_url('po-admin/article/new') ?>" class="text-dark link-hover-underline">
            <span class="material-icons align-middle text-green fs-2">
                add_circle
            </span>new
        </a>
        <a href="<?= base_url('po-admin/article/tag') ?>" class="text-dark link-hover-underline">
            <span class="material-icons align-middle text-green fs-2">
                add_circle
            </span>tag
        </a>
    </div>

    <?php
        foreach ($article as $index => $value) {
            $valueDate = date("d M Y", strtotime($value['tgl_buat'])); 
            ?>
            <div class="shadow bg-body rounded-15 p-4 mb-4">
                <div class="row">
                    <div class="col-md-3">
                        <img width="100%" style="object-fit: cover;" src="<?= base_url('/asset/file/article/' . $value['gambar']) ?>" alt="">
                    </div>
                    <div class="col-md">
                        <h5 class="pb-0 mb-0 pt-2"><?= $value['judul'] ?></h5>
                        <small class="text-muted"><?= $valueDate ?></small>
                        <p class="mt-3"><?= nl2br($value['teks']) ?></p>
                        <div class="row">
                            <div class="col-md-11">
                                <!-- Kosong -->
                            </div>
                            <div class="col-md-1">
                                <a href="<?= base_url('/po-admin/article/detail/' . $value['id_article']) ?>">
                                    <span class="material-icons align-middle">
                                        arrow_forward
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
    ?>
</div>