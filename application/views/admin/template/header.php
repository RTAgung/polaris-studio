<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Polaris Admin - <?= $header_title ?></title>

    <link rel="icon" href="<?= base_url('/asset/image/general/polaris_logo_mini.png') ?>" type="image/x-icon">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= base_url('asset/template-admin/plugins/fontawesome-free/css/all.min.css') ?>">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Material Icon -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url('asset/template-admin/dist/css/adminlte.min.css') ?>">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="<?= base_url('asset/template-admin/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') ?>">
    <!-- Internal CSS -->
    <link rel="stylesheet" href="<?= base_url('asset/css/style.css') ?>">
    <link rel="stylesheet" href="<?= base_url('asset/css/admin-style.css') ?>">
    <link rel="stylesheet" href="<?= base_url('asset/css/text.css') ?>">
    <style>
        #my-bg-black {
            background-color: #303030;
        }
    </style>
</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <li class="nav-item d-none d-sm-inline-block">
                    <p class="nav-link m-0 fw-bold"><?= $header_admin ?></p>
                </li>
                <li class="nav-item">
                    <a href="<?= base_url('/po-admin/management/edit_password') ?>" class="nav-link btn btn-outline-danger border-red border-1 rounded-pill text-dark">Ubah Password</a>
                </li>
                <li class="nav-item ">
                    <a href="<?= base_url('/po-admin/logout') ?>" class="nav-link btn btn-danger rounded-pill my-bg-red text-white ms-2 border-0">Logout</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                        <i class="fas fa-expand-arrows-alt"></i>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary bg-darker elevation-4">
            <!-- Brand Logo -->
            <a href="<?= base_url('/po-admin') ?>" class="brand-link">
                <span class="brand-text font-weight-light">
                    <img src="<?= base_url('asset/image/general/logo_putih.png') ?>" class="ms-4" width="150px">
                </span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class
                        with font-awesome or any other icon font library -->
                        <li class="nav-item ">
                            <a href="<?= base_url('/po-admin/dashboard') ?>" class="nav-link <?php if (!empty($nav_dashboard)) echo 'text-white' ?>" id="<?= $nav_dashboard ?>">
                                <i class="nav-icon fas fa-tachometer-alt"></i>
                                <p>
                                    Dashboard
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?= base_url('/po-admin/client') ?>" class="nav-link <?php if (!empty($nav_client)) echo 'text-white' ?>" id="<?= $nav_client ?>">
                                <span class=" nav-icon fas fa-users"></span>
                                <p>
                                    Client
                                </p>
                            </a>
                        </li>
                        <li class="nav-item <?= $nav_menu_open ?>">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-copy"></i>
                                <p>
                                    Project
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="<?= base_url('/po-admin/project/video') ?>" class="nav-link <?php if (!empty($nav_video)) echo 'text-white' ?>" id="<?= $nav_video ?>">
                                        <i class=" fas fa-video nav-icon"></i>
                                        <p>Video</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?= base_url('/po-admin/project/web') ?>" class="nav-link <?php if (!empty($nav_web)) echo 'text-white' ?>" id="<?= $nav_web ?>">
                                        <i class=" fas fa-pager nav-icon"></i>
                                        <p>Web</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="<?= base_url('/po-admin/payment') ?>" class="nav-link <?php if (!empty($nav_payment)) echo 'text-white' ?>" id="<?= $nav_payment ?>">
                                <i class=" nav-icon fas fa-money-check-alt"></i>
                                <p>
                                    Payment
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?= base_url('/po-admin/packet') ?>" class="nav-link <?php if (!empty($nav_packet)) echo 'text-white' ?>" id="<?= $nav_packet ?>">
                                <i class=" nav-icon fas fa-box"></i>
                                <p>
                                    Packet
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?= base_url('/po-admin/article') ?>" class="nav-link <?php if (!empty($nav_article)) echo 'text-white' ?>" id="<?= $nav_article ?>">
                                <i class=" nav-icon fas fa-newspaper"></i>
                                <p>
                                    Article
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?= base_url('/po-admin/management') ?>" class="nav-link <?php if (!empty($nav_admin)) echo 'text-white' ?>" id="<?= $nav_admin ?>">
                                <i class=" nav-icon fas fa-user-cog"></i>
                                <p>
                                    Admin <?= $this->session->flashdata('access_denied') ?>
                                </p>
                            </a>
                        </li>
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper p-4">