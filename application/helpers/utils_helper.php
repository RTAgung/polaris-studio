<?php

function dateFormat($date, $format = "d-m-Y")
{
    return date($format, strtotime($date));
}

function konversiNomorHP($nomor)
{
    $nomor = str_replace(" ", "", $nomor);
    $nomor = str_replace("(", "", $nomor);
    $nomor = str_replace(")", "", $nomor);
    $nomor = str_replace(".", "", $nomor);
    $hasil = $nomor;

    if (!preg_match('/[^+0-9]/', trim($nomor))) {
        if (substr(trim($nomor), 0, 3) == '+62') {
            $hasil = trim($nomor);
        } elseif (substr(trim($nomor), 0, 1) == '0') {
            $hasil = '+62' . substr(trim($nomor), 1);
        }
    }
    $hasil = str_replace("+", "", $hasil);
    return $hasil;
}
