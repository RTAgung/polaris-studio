<?php

function logged_in()
{
    $instance = get_instance();
    if ($instance->session->userdata('auth_e')) {
        if ($instance->session->userdata('auth_k')) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function logged_in_admin()
{
    $instance = get_instance();
    if ($instance->session->userdata('auth_adme')) {
        if ($instance->session->userdata('auth_admk')) {
            if ($instance->session->userdata('auth_adms') == 1) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function encryptEmail($email, $key)
{
    $instance = get_instance();
    $instance->encryption->initialize(
        array(
            'cipher' => 'aes-256',
            'mode' => 'ctr',
            'key' => $key
        )
    );

    return $instance->encryption->encrypt($email);
}

function decryptEmail($email, $key)
{
    $instance = get_instance();
    $instance->encryption->initialize(
        array(
            'cipher' => 'aes-256',
            'mode' => 'ctr',
            'key' => $key
        )
    );
    return $instance->encryption->decrypt($email);
}

function getClientEmailSession()
{
    $instance = get_instance();
    $email = $instance->session->userdata('auth_e');
    $key = $instance->session->userdata('auth_k');
    return decryptEmail($email, $key);
}

function getAdminEmailSession()
{
    $instance = get_instance();
    $email = $instance->session->userdata('auth_adme');
    $key = $instance->session->userdata('auth_admk');
    return decryptEmail($email, $key);
}
